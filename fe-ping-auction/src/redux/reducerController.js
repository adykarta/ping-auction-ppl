import {register, getProv, getEmail, getPassword} from './reducers/RegisterReducer';

import {login, logout} from './reducers/LoginReducer';
import {sessionReducer} from 'redux-react-session'

export default {register, getProv, login, logout, getEmail, getPassword,
session:sessionReducer}
