import axios from 'axios';
// import pingAuction from '../../config';
import {pingAuction} from '../../config';
import {headers} from '../../config';
const head = {
    'Content-Type':'application/json',
    'Accept': 'application/json',
}

export function login(username, password){
    return{
      type: 'LOGIN',
      payload: axios.post(`${pingAuction}login`,{
        username,
        password
      

      },{
        head
      })
    }
}

export function logout(){
    return{
        type:'LOGOUT',
        payload:axios.get(`${pingAuction}logout`,
            headers()
        
        )
    }
}
