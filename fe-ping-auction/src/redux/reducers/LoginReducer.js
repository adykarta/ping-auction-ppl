const initialState = {
    fetching: false,
    fetched: false,
    error: null,
    data: [],
 
}

const logoutState ={
    fetching: false,
    fetched: false,
    error: null,
    data: [],
 
}

  export const login=(state = initialState, action)=>{
    switch (action.type) {
      case 'LOGIN_PENDING':
        return{...state, fetching: true, error:null}
      case 'LOGIN_FULFILLED':
        return{...state, fetching: false, fetched: true, data: action.payload.data, status:action.payload.data}
      case 'LOGIN_REJECTED':
        return{...state, fetching: false, error: action.payload.data}
      default:
        return state
    }
  }

  export const logout=(state = logoutState, action)=>{
    switch (action.type) {
      case 'LOGIN_PENDING':
        return{...state, fetching: true, error:null}
      case 'LOGIN_FULFILLED':
        return{...state, fetching: false, fetched: true, data: action.payload.data, status:action.payload.data}
      case 'LOGIN_REJECTED':
        return{...state, fetching: false, error: action.payload.data}
      default:
        return state
    }
  }
