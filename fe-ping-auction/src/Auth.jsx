
const Auth = {
	
	isAuthenticated: false,
	token: '',

	setAuth() {
		this.isAuthenticated = true
	},

	signout() {
		this.isAuthenticated = false
		this.setAuthToken('')
	
	},

	getAuth() {
		return this.isAuthenticated
	},

	setAuthToken(tokenStr) {
		this.token = tokenStr
	},

	getAuthToken() {
		return this.token
	},


}

export default Auth;

// import {sessionService} from 'redux-react-session';
// import React from 'react'
// import { useSelector } from 'react-redux'
// import store from './store'

// const Auth = ()=>{
// 	const state = useSelector(state=>state.session.user)
	
	
// 	return(
// 		state
		
// 	)
// }
// export default Auth