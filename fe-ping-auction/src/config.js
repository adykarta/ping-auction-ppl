
const head = {   'Content-Type':'application/json',   'Accept': 'application/json', }

const pingAuction = 'http://127.0.0.1:8080/api/'


export function headers(){
    
    const token = localStorage.getItem("token");
    return{
      headers :{
        'Content-Type':'application/json',
        'Authorization': `Bearer ${token}`
      }
    }
  }

  export {pingAuction, head}
  
