import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import DefaultLayout from './components/DefaultLayout';
import Homepage from './pages/Homepage';
import Transaction from './pages/Transaction'

import Login from './pages/Login/Login';
import Register from './pages/Login/Register';
import NewPassword from './pages/Login/NewPassword';
import Forgetpass from './pages/Login/Forgetpass';
import PostEmailKonf from './pages/Login/PostEmailKonf';
import PostPasswordKonf from './pages/Login/PostPasswordKonf';
import LandingPage from './pages/LandingPage';
import DetailProduct from './pages/DetailProduct';
import Profile from './pages/Profile'
import Marketplace from './pages/Marketplace';
 
import HasilSearch from './pages/HasilSearch';
import FormTambahB from './pages/Listing/FormTambahB';
import BiddingPage from './pages/Explore/BiddingPage';


const Router = (childProps) => {
	return (
		<Fragment>
			<Switch>	
				<PublicRoute exact path='/' component={LandingPage} props={childProps} />	

				<PrivateRoute exact path='/detail-product' component={DetailProduct} props={childProps} />
				<PrivateRoute exact path='/explore' component={HasilSearch} props={childProps} />
				<PrivateRoute exact path='/explore/bidding' component={BiddingPage} props={childProps} />
				<PublicRoute exact path='/login' component={Login} props={childProps} />
				<PublicRoute exact path='/post-email-konfirmasi' component={PostEmailKonf} props={childProps} />
				<PublicRoute exact path='/post-password-konfirmasi' component={PostPasswordKonf} props={childProps} />
				<PublicRoute exact path='/register' component={Register} props={childProps} />
				<PublicRoute exact path='/Forgetpass' component={Forgetpass} props={childProps} />
				<PrivateRoute exact path='/home' component={Homepage} props={childProps} />
				<PublicRoute exact path='/market' component={Marketplace} props={childProps} />
				<PrivateRoute exact path='/transaksi' component={Transaction} props={childProps} />
				<PublicRoute exact path='/setNewPassword' component={NewPassword} props={childProps} />
				<PrivateRoute exact path='/profile' component={Profile} props={childProps} />

				<PrivateRoute exact path='/listing/create-barang' component={FormTambahB} props={childProps} />

				{/* <PublicRoute exact path='/profile' component={Profile} props={childProps} /> */}

            </Switch>
        </Fragment>	
    )
}

const PublicRoute = ({ component: Component, props:cProps,  ...rest }) => (
	<Route {...rest} render={
		props=>(<Component  {...props} {...cProps}  />)
	}
	/>
)

const PrivateRoute = ({ component: Component, props:cProps, ...rest }) => (
	<Route {...rest} render={
		props=>(<DefaultLayout>
			<Component  {...props} {...cProps}  />
			</DefaultLayout>)
	} />
)

export default Router;
