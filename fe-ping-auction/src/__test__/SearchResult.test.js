import React from 'react';
import ReactDOM from 'react-dom'
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import HasilSearch from '../pages/HasilSearch';
import {Grid, FormControl} from '@material-ui/core';
import { Provider } from 'react-redux';
import store from '../store'


describe("Search Result testing", () => {
    test('First test', () => {
        expect(true).toBeTruthy()
    })
    
    test('there is a Search Result component', () => {
        const searchPage = render(<Provider store={store}><HasilSearch /></Provider>);
        expect(searchPage).toBeDefined();
    });
    
    // test('there is div tag', () => {
    //     const div = document.createElement("div")
    //     ReactDOM.render(<div></div>, div)
    // })
    
    // test("there is h1 tag", () => {
    //     const header = document.createElement("header")
    //     ReactDOM.render(<h1></h1>, header)
    // })
    
    // test("there is h3 tag ", () => {
    //     const cardTitle = document.createElement("cardTitle")
    //     ReactDOM.render(<h3></h3>, cardTitle)
    // })
    
    // test("exists a Grid components", () => {
    //     const grid = document.createElement("grid")
    //     ReactDOM.render(<Grid></Grid>, grid)
    // })
    
    // test("exists a TextField components", () => {
    //     const formControl = document.createElement("textfield")
    //     ReactDOM.render(<FormControl></FormControl>, formControl)
    // })
}) 

