import React from 'react';
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Provider } from 'react-redux';
import store from '../store'
import Login from '../pages/Login/Login';
import { HashRouter } from 'react-router-dom';
afterEach(cleanup);

describe("Login testing", () => {
    console.log("in")
    test('checking text', () => {
        const { getByText } = render(<Provider store={store}><HashRouter><Login /></HashRouter></Provider>);
        const linkElement = getByText("Belum punya akun?");
        expect(linkElement).toBeInTheDocument();
      });
    
    test('renders Login page', () => {
        const { queryAllByText } = render(<Provider store={store}><HashRouter><Login /></HashRouter></Provider>);
        const linkElement = queryAllByText("Masuk");
        expect(linkElement[0]).toBeInTheDocument();
    });

    const checkUsername = () => {
        const utils = render(<Provider store={store}><HashRouter><Login /></HashRouter></Provider>);
        const user = utils.getByLabelText("Username or Email");
        return { user, ...utils }
    }
    
    test('it should allow numbers', () => {
        const { user } = checkUsername();
        expect(user.value).toBe('');
        fireEvent.change(user, { target: {value: 'haha123'}});
        expect(user.value).toBe('haha123');
    });

    test('it should allow emails', () => {
        const { user } = checkUsername();
        expect(user.value).toBe('');
        fireEvent.change(user, { target: {value: 'haha123@yahoo.com'}});
        expect(user.value).toBe('haha123@yahoo.com');
    });

    const checkPassword = () => {
        const utils = render(<Provider store={store}><HashRouter><Login /></HashRouter></Provider>);
        const pass = utils.getByLabelText("Password");
        return { pass, ...utils }
    }

    test('it should allow numbers', () => {
        const { pass } = checkPassword();
        expect(pass.value).toBe('');
        fireEvent.change(pass, { target: {value: 'tes123'}});
        expect(pass.value).toBe('tes123');
    });

    test('it should allow uppercase', () => {
        const { pass } = checkPassword();
        expect(pass.value).toBe('');
        fireEvent.change(pass, { target: {value: 'Tes123'}});
        expect(pass.value).toBe('Tes123');
    });   
})
