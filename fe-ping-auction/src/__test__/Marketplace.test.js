import React from 'react';
import ReactDOM from 'react-dom'
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import Marketplace from '../pages/Marketplace';

test('First test', () => {
    expect(true).toBeTruthy()
})

// test('there is a Marketplace component', () => {
//     expect(Marketplace()).toBeDefined();
// });

test('there is div tag', () => {
    const div = document.createElement("div")
    ReactDOM.render(<div></div>, div)
})

test("there is h2 tag", () => {
    const header = document.createElement("header")
    ReactDOM.render(<h2></h2>, header)
})

// test("there is grid for maintain row and column", () => {
//     const grid = document.createElement("grid")
//     ReactDOM.render(<Grid></Grid>, grid)
// })
