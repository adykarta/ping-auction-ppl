import React from 'react';
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Provider } from 'react-redux';
import store from '../store'
import {HashRouter} from 'react-router-dom';

import Forgetpass from '../pages/Login/Forgetpass';

afterEach(cleanup)

describe("Forgetpass testing" ,() => {
  const checkUserEmail = () => {
    const utils = render(<Provider store={store}><HashRouter><Forgetpass /></HashRouter></Provider>);
    const user = utils.getByDisplayValue("");
    console.log("in")
    return { user, ...utils }
}
  test ('it should allow number',() => {
    const { user } = checkUserEmail();
    expect(user.value).toBe('');
    fireEvent.change(user,{ target:{value:'haha123@yahoo.com'}});
    expect(user.value).toBe('haha123@yahoo.com');
  })
});

