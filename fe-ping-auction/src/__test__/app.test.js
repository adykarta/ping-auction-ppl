import React from 'react';
import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import App from '../../src/App';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../store'

afterEach(cleanup);

it("matches snapshot", () => {
  const { asFragment } = render(<Provider store={store}><HashRouter><App /></HashRouter></Provider>);
  expect(asFragment()).toMatchSnapshot();

});
 