import React, { Fragment } from 'react';
import ReactDOM from 'react-dom'
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import Homepage from '../pages/Homepage';
import DefaultLayout from '../components/DefaultLayout';
import AppBarFixed from '../components/AppBar';
import {Grid, Paper} from '@material-ui/core';
import { Provider } from 'react-redux';
import store from '../store'

describe("Homepage Testing", () => {
    test('First test', () => {
        expect(true).toBeTruthy()
    })
    
    test('there is a Homepage component', () => {
        const homepage = render(<Provider store={store}><Homepage /></Provider>);
        expect(true).toBeTruthy()
        expect(homepage).toBeDefined();
    });
    
    
    test('there is div tag', () => {
        const div = document.createElement("div")
        ReactDOM.render(<div></div>, div)
    })
    
    // test("there is h2 tag", () => {
    //     const header = document.createElement("header")
    //     ReactDOM.render(<h2></h2>, header)
    // })
    
    // test("there is grid for maintain row and column", () => {
    //     const grid = document.createElement("grid")
    //     ReactDOM.render(<Grid></Grid>, grid)
    // })

    // test("there is paper for display item", () => {
    //     const paper = document.createElement("grid")
    //     ReactDOM.render(<Paper></Paper>, paper)
    // }) 
    
    // test('check for category section', () => {
    //     const { getByTestId } = render(<Homepage />)
    //     expect(getByTestId("category")).toHaveTextContent("Kategori")
    // })
    
    // test("check for today's auction  section", () => {
    //     const { getByTestId } = render(<Homepage />)
    //     expect(getByTestId("today's auction")).toHaveTextContent("Lelang Hari Ini")
    // })
    
    
})
