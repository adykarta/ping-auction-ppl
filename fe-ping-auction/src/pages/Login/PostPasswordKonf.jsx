import React, {Fragment} from 'react';
import Notification from '../../components/Notification'

const PostPasswordKonf = (props)=>{
    return(
        <Fragment>
            <Notification firstSentence="Your password has been updated!" secondSentence='Please login again.' buttonText='BACK TO LOGIN' link = '/login' />
        </Fragment>
    )
}

export default PostPasswordKonf;