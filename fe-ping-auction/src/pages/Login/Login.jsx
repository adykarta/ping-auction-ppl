import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button, TextField, Typography, InputAdornment, IconButton } from '@material-ui/core';
import LogoBlue from '../../images/pingAuctionLogoBlue.svg';
import { useDispatch} from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import {login} from '../../redux/actions/LoginAction';
import { sessionService } from 'redux-react-session';

import { Redirect, Link } from 'react-router-dom';
import {Visibility, VisibilityOff} from '@material-ui/icons';

const styles = makeStyles(theme=>({
  container:{
      backgroundColor:theme.palette.primary.main,
      width:'100vw',
      height:'100vh',
      display:'flex',
      justifyContent:'center',
      alignItems:'center',
  },
 
  loginBox:{
      backgroundColor:'white',
      border: '0px solid white',
      borderRadius:'50px',
      width:'1000px',
      height:'550px',
      position:'relative',
      display:'flex',
      justifyContent:'center',
      [theme.breakpoints.down('sm')]: {
        height:"500px",
        width:'90vw'
      },
  },
  leftSide:{
    position:'absolute',
    backgroundColor:'#F1F1F5', 
    height:"100%", 
    [theme.breakpoints.down('sm')]: {
        display:'none'
      },

    width:'50%',
    left:'0',
    border:'0px solid #F1F1F5', 
    borderTopLeftRadius:"50px", 
    borderBottomLeftRadius:"50px",
    display:"flex",
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column'
  },
  rightSide:{
    backgroundColor:'white', 
    height:"100%", 
    width:'40%',
    right:'0',
    position:'absolute',
    border:'0px solid white', 
    borderTopRightRadius:"50px", 
    borderBottomRightRadius:"50px",
    display:"flex",
    [theme.breakpoints.down('sm')]: {
        position:'static',
        alignItems:'center',
     
        border:'none',
        width:'auto',
        backgroundColor:"transparent",
        borderTopRightRadius:"0px", 
        borderBottomRightRadius:"0px",
    },
    // alignItems:'center',
    padding:'0px 50px 0px 50px',
    flexDirection:'column',
    justifyContent:'center'
    
  },
  username:{
      marginTop: '50px',
      width: '100%'
  },
  password:{
      marginTop: '20px',
      width: '100%'
  },
  inButton:{
      marginTop: '30px',
      borderRadius: '10px',
      height: '40px'
    //   widht: '20%'
  },
  forgot:{
    textAlign: 'right',
    marginTop: '10px'
  }
}))
const Login =(props)=>{

    const classes = styles()
    const dispatch = useDispatch()
    const token = localStorage.getItem("token");
    // const sessionState = useSelector(state=>state.session)
    // const preventDefault = event => event.preventDefault();
    // function myFunction() {
    //     alert("Successfully Login!");
    // }
    const [values, setValues] = React.useState({
        username:'',
        password:'',
        isError:false,
    })
  
 
  
    
    const [isShowed,setIsShowed] = React.useState(false)
    const [isLoading, setIsLoading] = React.useState(false)

    const handleInput=(form)=>(event)=>{
        const {value} = event.target;
        if(form==='username'){
            setValues(prev=>({...prev,username:value}))
        }
        if(form==='password'){
            setValues(prev=>({...prev, password:value}))
        }
    }
   
    const handlePost = ()=>{
        setIsLoading(true);
        dispatch(login(values.username,values.password)).then(
            (res)=>{
             
               
                if(res.value.data.auth===false){
            
                    setValues({username:'',
                    password:'',
                    isError:true,})
                    setIsLoading(false)
                }
                else{

                    const { token } = res.value.data;
                    localStorage.setItem('token', token);
                    sessionService.saveSession({ token })
                    .then(async() => {
                    
                    await sessionService.saveUser(res.value.data).then(
                        res=>{
                            setValues(prev=>({...prev, isError:false}))
                            setIsLoading(false)
                          
                            
                        }
                    )
                    
                    
                    })
                    }

                }
                
        
        )
    }

    const handleOpenPass = ()=>{
        setIsShowed(!isShowed)
    }
   
    return(
        <Fragment>
            {
               token!==null ?
                <Redirect to ="/home" />
                :
            
            <div className={classes.container}>
           
              
                <Box className={classes.loginBox}>
                   {
                       isLoading===true ?
                       <div style={{alignItems:"center", display:'flex', justifyContent:'center', width:'100%', height:'100%'}}>
                            <CircularProgress/>
                       </div>

                        :
                        <div>
                 
                      
                        <Box className={classes.leftSide}>
                            <img src={LogoBlue} style={{width:"inherit"}} alt="ping-logo"></img>
                        </Box>
                    
                    
                    
                        <Box className={classes.rightSide}>
                            <Typography  variant='h5'>Masuk</Typography>
                            <TextField className={classes.username} id="standard-basic" label="Username or Email" values={values.username} onChange={handleInput('username')}/>
                            <TextField className={classes.password} 
                            values={values.password} onChange={handleInput('password')}
                            id="standard-basic" 
                            label="Password"
                            type={isShowed===true ? "text":"password"}
                            InputProps={{
                                endAdornment:(
                                    <InputAdornment position="end">
                                        <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleOpenPass}
                                        
                                        // onMouseDown={handleMouseDownUlangiPassword}
                                       
                                        >
                                        {isShowed===false ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                )
                              }}
                           
                            />
                            {
                                values.isError===true?
                                <Typography style={{color:"red", marginTop:'4px'}}>*Username atau password salah</Typography>
                                :
                                null
                            }
                            

                            <Link className={classes.forgot} to="/Forgetpass" style={{color: "#50B5FF", alignItems: 'left'}}>
                                Lupa Password
                            </Link>
                            <Button className={classes.inButton} variant="contained" color="primary" onClick={()=>handlePost()}>
                                Masuk
                            </Button>
                            <Typography style={{textAlign: 'center', marginTop: '15px', }}>Belum punya akun? <Link to="/register" style={{color:'#284089', fontWeight:'bold'}}>Daftar</Link></Typography>
                        </Box>
                        
                        </div>
                    }
                </Box>
                
                    
            </div>
            }
        </Fragment>
                
    )
} 
export default Login
