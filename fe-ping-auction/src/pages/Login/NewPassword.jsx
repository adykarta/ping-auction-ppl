import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import { CardContent } from '@material-ui/core';
import LogoWhite from '../../images/pingAuctionLogoWhite.svg'
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import {getPassword} from '../../redux/actions/RegisterAction';
import { useDispatch } from 'react-redux';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { useEffect } from 'react';
import queryString from 'query-string'




const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  container:{
    backgroundColor:theme.palette.primary.main,
    width:'100vw',
    height:'100vh',  
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'column'
  },
  root: {
    Width: 275,
    borderRadius: '25px',
    paddingTop:'28px'
    
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(1, 0, 1),
    borderRadius: '10px'
  },
}));

export default function NewPass(props) {

  const dispatch = useDispatch();
  const classes = useStyles();
  const [values, setValues] = React.useState({
    password: '',
    password1: '',
    passwordError: '',
    password1Error: '',
    showPassword: false,
    showPassword2:false,
    token: '',
  });

  useEffect(() => { 
    
    const tkn = queryString.parse(props.location.search);

    setValues(prev=>({...prev,token:tkn.token}))
   
  },[props])

  const handleClickShowPassword = (param) => {
    if(param===1){
      setValues({ ...values, showPassword: !values.showPassword });

    }
    if(param===2){
      setValues({ ...values, showPassword2: !values.showPassword2 });
    }
   
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };
  const handleInput = (form) => event => {
    const {value} = event.target

    if(form === 'password'){
      var pass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
      if (pass.test(value) || value ==='') {
        setValues(prev=>({...prev, password:value}))
        values.passwordError = false;
    } else {
        setValues(prev=>({...prev, password:value}))
        values.passwordError = true;
      }
    }
   else if (form === 'password1'){
      var passw = values.password;
      if (passw === value || value ==='') {
         setValues(prev=>({...prev, password1:value}))
         values.password1Error = false;
    } else {
         setValues(prev=>({...prev, password1:value}))
         values.password1Error = true;
         }
      }
  };
  


  return (
<Fragment>
  <div className={classes.container}>
    <Container style={{textAlign:"center"}}> 
     <img src={LogoWhite} alt="ping-logo" ></img>
    </Container>
    <Container component="main" maxWidth="xs" style={{backgroundColour:'blue'}}>
      <CssBaseline />
      <div className={classes.paper}    >
        <Card className={classes.root} variant="outlined">
            <CardContent>
        <Typography component="h1" variant="h5" style={{textAlign:'center',fontFamily:'Poppins',marginBottom:'30px', fontWeight:"bold"}}>
          Buat Password
        </Typography>
        <FormControl className={clsx(classes.form, classes.textField)} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">New Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleInput('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={()=>handleClickShowPassword(1)}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={108}
            error={values.passwordError}
          />

        <FormHelperText style={{color:'red'}}id="my-helper-text">{values.passwordError ? "*Minimum 8 character with alphabetical and numerical" : ""}</FormHelperText>
        </FormControl>
          <FormControl className={clsx(classes.form, classes.textField)} variant="outlined" >
          <InputLabel htmlFor="outlined-adornment-password">Confirm Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword2 ? 'text' : 'password'}
            value={values.password1}
            onChange={handleInput('password1')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={()=>handleClickShowPassword(2)}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={128}
            error={values.password1Error}
            helperText={values.password1Error ? "*Password didn't match" : ""}

          />
          <FormHelperText style={{color:'red'}} id="my-helper-text">{values.password1Error ? "*Password didn't match" : ""}</FormHelperText>
        </FormControl>

          <Button style={{marginTop:"40px"}}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            fontFamily="Poppins"
            className={classes.submit}
            onClick={()=>dispatch(getPassword(values.password1,values.token)).then(
              props.history.push('/post-password-konfirmasi')
            )}

          >
            Buat Password
          </Button>
        </CardContent>
        </Card>
      </div>

    </Container>
    </div>
    </Fragment>
  );
}