import React, { Fragment, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import { Grid, Box, Button, TextField, Typography, Paper, Checkbox, FormControl, OutlinedInput, InputAdornment, IconButton, Select } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import LogoWhite from '../../images/pingAuctionLogoWhite.svg'
import { useDispatch, useSelector } from 'react-redux';
import {register, getProv} from '../../redux/actions/RegisterAction';
import {login} from '../../redux/actions/LoginAction';
import CircularProgress from '@material-ui/core/CircularProgress';
import  { Redirect, Link } from 'react-router-dom'
import {sessionService} from 'redux-react-session';

const styles = makeStyles(theme=>({
    root:{
      backgroundColor:theme.palette.primary.main,
      width:'100vw',
      height:'auto',
      minHeight:'100vh',
      display:'flex',
      justifyContent:'center',
      alignItems:'center' ,
      [theme.breakpoints.down('sm')]: {
       
        height: 'auto',
      },   
    },
    paper:{
      [theme.breakpoints.down('sm')]: {
        margin: '4% 50px',
        width: '80vw',
        height: 'auto',
        padding:'10% 0%'
      },
     
      margin: '4% 50px',
      width: '30vw',
      height: 'auto',
      paddingTop: '1px',
      paddingBottom: '20px',
      borderRadius: '25px',
      textAlign: 'center',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
    container2:{
      backgroundColor:theme.palette.primary.main,
      width:'100%',
      display:'flex',
      justifyContent:'center',
      alignItems:'flex-end'    
    },
    topGrid: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      [theme.breakpoints.up('xl')]: {
        paddingTop: '3vh'
      },
      [theme.breakpoints.down('xl')]: {
        paddingTop: '1vh'
      },
    },
    paperGrid:{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: 'auto',
      marginTop: '30px'
    },
    username:{
      marginTop: '40px',
      width: '20px'
    },
    email:{
      marginTop: '15px'
    },
    password:{
      marginTop: '15px',
      width: '90%',
    },
    password2:{
      marginTop: '15px',
      width: '90%',
    },
    handphone:{
        marginTop: '15px',
        width: '90%',
    },
    registerButton:{
        marginTop: '10px',
        borderRadius: '10px',
        width: '80%',
        height: '40px'
    },
    margin: {
        margin: theme.spacing(0.5),
    },
    withoutLabel: {
        marginTop: theme.spacing(3),
    },
    textField: {
        width: 200,
    },
    colorPrimary: {
        color: theme.palette.primary.main
    },
    loading: {
        width: '20vw',
        minHeight:'50vh',
        display:'flex',
        alignItems:'center',
        justifyContent:'center'
    }

}))
const Register =()=>{
    
    const token = localStorage.getItem("token");
    const dispatch = useDispatch()
    const classes = styles()
    const sessionState = useSelector(state=>state.session)
    // const provs = useSelector(state => state.getProv.data);

    // const postReg = useSelector(state => state.register.data);
    // const preventDefault = event => event.preventDefault();
    // const [checked, setChecked] = React.useState(true);
    const [values, setValues] = React.useState({
        password: '',
        ulangiPassword: '',
        phone: '',
        province: '',
        username:'',
        email: '',
        passwordError: false,
        ulangiPasswordError: false,
        showUlangiPassword: false,
        showPassword: false,
        agree: false,
        ready: false,
        emailError: false,
        userExist: false,
        phoneExist: false,
        emailExist: false,
        provs:[],
        checked:false,
        disabled:true,
        isLoading:true,
        redirect: false
    });
    useEffect(()=>{
        dispatch(getProv()).then(
            res=>setValues({...values, provs:res.value.data.data, isLoading:false})
        )
    },[])
    useEffect(()=>{
        
        if(values.password !=='' && values.email !== '' && values.phone !=='' && values.province !== '' && values.username !== '' && values.checked !== false){
            setValues(prev=>({...prev, disabled:false}))
        } else {
            setValues(prev=>({...prev, disabled:true}))
        }

    },[values.password,values.email,values.phone,values.province,values.username,values.checked])

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleClickShowUlangiPassword = () => {
        setValues({ ...values, showUlangiPassword: !values.showUlangiPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };

    const handleMouseDownUlangiPassword = event => {
        event.preventDefault();
    };

    const checkEmail = () => {
        var err = values.emailError;
        var exs = values.emailExist;
        if (err === true) {
            return "*example@mail.com"
        }
        else if (exs === true) {
            return "*Email telah terdaftar"
        }
    };

    const handleInput = (form) => event => {
        const {value} = event.target

        if(form ==='phone'){
            var phoneno = /^[0-9\b]+$/;
            if(phoneno.test(value) || value ==='') {
                setValues(prev=>({...prev, phone:value}))
                values.phoneExist = false;
            }  
            else {
                values.phoneExist = false;
                return false;
            }
        }
        else if(form ==='username'){
            setValues(prev=>({...prev,username:value}))
            values.userExist = false;
        }
        else if (form ==='email') {
            // eslint-disable-next-line
            var mailreg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (mailreg.test(value) || value ==='') {
                setValues(prev=>({...prev, email:value}))
                values.emailError = false;
                values.emailExist = false;
                if (values.ulangiPasswordError === false && values.passwordError === false && values.agree === true && values.emailError === false && values.username !== '' && values.email !== '' && values.password !== '' && values.ulangiPassword !== '' && values.province !== '' && values.phone !== '') {
                    values.ready = true;
                }
            } else {
                setValues(prev=>({...prev, email:value}))
                values.emailError = true;
                values.emailExist = false;
                values.ready = false;
            }
        }
        else if (form ==='password') {
            var pass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
            if (pass.test(value) || value ==='') {
                setValues(prev=>({...prev, password:value}))
                values.passwordError = false;
                if (values.ulangiPasswordError === false && values.passwordError === false && values.agree === true && values.emailError === false && values.username !== '' && values.email !== '' && values.password !== '' && values.ulangiPassword !== '' && values.province !== '' && values.phone !== '') {
                    values.ready = true;
                }
            } else {
                setValues(prev=>({...prev, password:value}))
                values.passwordError = true;
                values.ready = false;
            }
        }
        else if (form ==='ulangiPassword') {
            var passw = values.password;
            if (passw === value || value ==='') {
                setValues(prev=>({...prev, ulangiPassword:value}))
                values.ulangiPasswordError = false;
                if (values.ulangiPasswordError === false && values.passwordError === false && values.agree === true && values.emailError === false && values.username !== '' && values.email !== '' && values.password !== '' && values.ulangiPassword !== '' && values.province !== '' && values.phone !== '') {
                    values.ready = true;
                }
            } else {
                setValues(prev=>({...prev, ulangiPassword:value}))
                values.ulangiPasswordError = true;
                values.ready = false;
            }
        }
        else if (form ==='terms') {
            setValues(prev=>({...prev, checked:!values.checked}))
        }
    }
    const validate = (data)=>{
      if(data.status===200){
        setValues(prev=>({...prev,redirect:true}))
      }
      if(data.isUsernameAvailable===false){
        setValues(prev=>({...prev,userExist:true}))
      }
      if(data.isEmailAvailable===false){
        setValues(prev=>({...prev,emailExist:true}))
      }
      if(data.isHandphoneAvailable===false){
        setValues(prev=>({...prev,phoneExist:true}))
      }
      setValues(prev=>({...prev, isLoading:false}))
       
    }

    const renderRedirect = () => {
      if (values.redirect === true) {
        dispatch(login(values.username,values.password)).then(
            (res)=>{
                const { token } = res.value.data;
                localStorage.setItem('token', token);
                sessionService.saveSession({ token })
                 .then(async() => {
                   await sessionService.saveUser(res.value.data)    
                   window.location.reload();        
                 })
                }
        )
      
      }
    }
    const postData = () =>{
        setValues(prev=>({...prev, isLoading:true, disabled:true, checked:false}))
        dispatch(register(values.username,values.password,values.province,values.email,values.phone)).then(
            res=>validate(res.value.data)
        )
    }
    return(
        <Fragment>
          {renderRedirect()}
          {
                token!==null ?
                <Redirect to ="/home" />
                :
            <div className={classes.root}>
            <Grid direction='column' xs={12} md={8} className={classes.topGrid}>
                <Grid direction='row' xs={12} md={8} className={classes.logoGrid}>
                    <div className={classes.container2}>
                        <img src={LogoWhite} alt='logo-ping'></img>
                    </div>
                </Grid>
                <Grid direction='row' xs={12} md={8}>
                    <div className={classes.paperGrid}>
                        <Paper className={classes.paper}>
                            {
                                values.isLoading=== true ? 
                            <div className={classes.loading}>
                                <CircularProgress disableShrink />
                            </div>
                            :
                            <div>
                                <Typography variant='h6' style={{marginTop: '30px'}}>Daftar</Typography>
                            <div>
                            <TextField 
                                data-testid="username"
                                className={classes.username}
                                id="outlined-basic"
                                label="Username"
                                variant="outlined"
                                size="small"
                                style={{width: '80%'}}
                                value={values.username}
                                onChange={handleInput('username')}
                                error={values.userExist}
                                helperText={values.userExist ? "*Username sudah terdaftar" : ""}
                            >
                            </TextField>
                            <TextField
                                data-testid="email"
                                className={classes.email}
                                id="outlined-basic"
                                label="Email"
                                value={values.email}
                                onChange={handleInput('email')}
                                variant="outlined"
                                size="small"
                                style={{width: '80%'}}
                                error={values.emailError || values.emailExist}
                                helperText={checkEmail()}
                            >
                            </TextField>
                            <FormControl margin='dense' className={clsx(classes.textField)} variant="outlined" style={{width: '80%', height: '10%', marginTop: '15px', marginBottom: '0px'}}>
                                <InputLabel data-testid="pass1">Password</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={values.showPassword ? 'text' : 'password'}
                                    value={values.password}
                                    onChange={handleInput('password')}
                                    endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                        >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                    }
                                    labelWidth={75}
                                    error={values.passwordError}
                                    helperText={values.passwordError ? "*Minimal 8 karakter dan mengandung huruf dan angka" : ""}
                                />
                            </FormControl>
                            <FormControl margin='dense' className={clsx(classes.textField)} variant="outlined" style={{width: '80%', height: '20%', marginTop: '15px', marginBottom: '0px'}}>
                                <InputLabel>Ulangi Password</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={values.showUlangiPassword ? 'text' : 'password'}
                                    value={values.ulangiPassword}
                                    onChange={handleInput('ulangiPassword')}
                                    endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowUlangiPassword}
                                        onMouseDown={handleMouseDownUlangiPassword}
                                        edge="end"
                                        >
                                        {values.showUlangiPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                    }
                                    labelWidth={125}
                                    error={values.ulangiPasswordError}
                                    helperText={values.ulangiPasswordError ? "*Password anda tidak sesuai" : ""}
                                />
                            </FormControl>
                            <FormControl variant="outlined" className={classes.formControl} style={{width: '80%' ,marginTop: '15px',}}margin='dense'>
                                <InputLabel>
                                  Provinsi
                                </InputLabel>
                                <Select
                                native
                                value={values.province}
                                onChange={handleChange('province')}
                                labelWidth={60}
                                inputProps={{
                                    name: 'province',
                                    id: 'outlined-age-native-simple',
                                }}
                                >
                                <option value=""/>
                                {
                                    values.provs.map((item,idx)=>{
                                        return(
                                            <option key={`list-prov-${item.id_prov}`} value={item.id_prov}>{item.nama_prov}</option>
                                        )
                                    })
                                }
                                </Select>
                            </FormControl>
                            <TextField
                                className={classes.handphone}
                                label="No.Handphone"
                                id="outlined-start-adornment"
                                // eslint-disable-next-line
                                className={clsx(classes.textField)}
                                onChange={handleInput('phone')}
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">+62</InputAdornment>,
                                }}
                                variant="outlined"
                                size="small"
                                style={{width: '80%', marginTop: '15px'}}
                                value={values.phone}
                                error={values.phoneExist}
                                helperText={values.phoneExist ? "*Nomor telah terdaftar" : ""}
                            />
                            </div>
                        
                            <Box style={{display:'flex', flexDirection:'row', alignItems:'center', textAlign:'left', justifyContent:'center', padding:'0px 30px'}}>
                                    <Checkbox 
                                        value={values.checked}
                                        color="primary" 
                                        onChange={handleInput('terms')}
                                        inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} 
                                        style={{marginTop: '7px'}}
                                    />
                                
                                        <p style={{fontSize: '12px'}}>Dengan mendaftar, saya menyetujui <b className={classes.colorPrimary}>syarat dan ketentuan</b> yang berlaku</p>
                            </Box>
                    
                            
                            <Button disabled={values.disabled} variant="contained" color="primary" className={classes.registerButton} onClick={()=>postData()}>
                                Daftar
                            </Button>
                            <Typography style={{marginTop: '15px'}}>Sudah punya akun? <Link to="/login">Masuk</Link></Typography>
                            </div>

                            }
                        
                        </Paper>
                        </div>
                    </Grid>
                </Grid>
            </div>
          }
        </Fragment>
    )
} 
export default Register
