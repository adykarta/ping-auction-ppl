import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import { CardContent } from '@material-ui/core';
import LogoWhite from '../../images/pingAuctionLogoWhite.svg';
import { useDispatch } from 'react-redux';
import {getEmail} from '../../redux/actions/RegisterAction';
import {Link} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  container:{
    backgroundColor:theme.palette.primary.main,
    width:'100vw',
    height:'100vh',    
     display:'flex',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'column'
  },
  root: {
    Width: 275,
    borderRadius: '25px',
    paddingTop:"28px"
    
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(1, 0, 1),
    borderRadius: '10px'
  },
}));

export default function Forgetpass() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [values, setValues] = React.useState({
    email:'',
    emailError: false,
  });

  const handleInput = (form) => event => {
    const {value} = event.target 

    if(form === 'email'){
      var mailreg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (mailreg.test(value) || value ==='') {
          setValues(prev=>({...prev, email:value}))
          values.emailError = false;
      } else {
          setValues(prev=>({...prev, email:value}))
          values.emailError = true;
      }
  }
}
  return (
<Fragment>
  <div className={classes.container}>
   
    <Container style={{textAlign:"center"}}> 
     <img src={LogoWhite} alt="logo-ping" ></img>
    </Container>
    <Container component="main" maxWidth="xs" style={{backgroundColour:'blue'}}>
      <CssBaseline />
      <div className={classes.paper}    >
        <Card className={classes.root} variant="outlined">
            <CardContent>
        <Typography component="h1" variant="h5" style={{textAlign:'center',fontFamily:'Poppins', fontWeight:'bold'}}>
          Ubah Password
        </Typography>
        <Typography component="h5" variant="h5" style={{fontSize: '12px', marginTop:"13px", textAlign: 'center',fontFamily:'Poppins',color:'#92929D'}}>
        Link untuk mengubah password akan dikirim ke email 
yang diisi jika ada akun yang terdaftar pada email tersebut
        </Typography>
        <form className={classes.form} Validate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={values.email}
            onChange={handleInput('email')}
            error={values.emailError}
            helperText={values.emailError ? "*example@mail.com" : "" }
          />
          <Link to="/post-email-konfirmasi"   style={{textDecoration: "none"}}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            fontFamily="Poppins"
            className={classes.submit}
            onClick={()=>dispatch(getEmail(values.email))}
          >
            Kirim
          </Button>
          </Link>
          <Link to="/login"   style={{textDecoration: "none"}}>
          <Button
          type="submit"
          fullWidth
          variant="contained"
          style={{backgroundColor:'#FAFAFB', boxShadow:"none", color:"#284089"}}
          className={classes.submit}
          >Cancel
          </Button>
          </Link>
        </form>
        </CardContent>
        </Card>
      </div>

    </Container>
    </div>
    </Fragment>
  );
}