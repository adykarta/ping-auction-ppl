import React, {Fragment} from 'react';
import Notification from '../../components/Notification'

const PostEmailKonf = (props)=>{
    return(
        <Fragment>
            <Notification firstSentence="Your email has been sent!" secondSentence='Please check your email.' buttonText='BACK TO LOGIN' link = '/login' />
        </Fragment>
    )
}

export default PostEmailKonf;