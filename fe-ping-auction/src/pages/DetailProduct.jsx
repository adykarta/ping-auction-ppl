import React, { useEffect, useState, Fragment } from 'react';
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import Carousel from 'react-material-ui-carousel'
import CarouselItem from 'react-material-ui-carousel'
import {Grid, Paper, ButtonBase, Button, GridList, GridListTile} from '@material-ui/core';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import {pingAuction,headers} from '../config'
import queryString from 'query-string'
import {useSelector} from 'react-redux';
import CountDown from '../components/CountDown';
import{Link} from 'react-router-dom'
import RiwayatBid from './Explore/RiwayatBid';


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`vertical-tabpanel-${index}`}
        aria-labelledby={`vertical-tab-${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </Typography>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  }

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      display: 'flex',
      height: 200,
      
    },
    wrapperAll:{
      marginLeft:'3em',
      marginRight:'3em',
      [theme.breakpoints.down('md')]: {
        marginLeft:'0',
        marginRight:'0'
      
    },

    },
    form: {
      width: '100%',
      marginTop: theme.spacing(1),
    },
    profil:{
      borderRadius: '10px',
    },
    paper: {
      height:'60%',
      marginBottom: '2%',
      paddingLeft:'1.5em',
      paddingRight:'1.5em',
      width:'100%'
      
    },
  
    margin: {
      margin: theme.spacing(1),
    },
    imageCategory: {
        padding: theme.spacing(2),
        alignItems: 'center',
        width:"100%",
        height:'auto',
        [theme.breakpoints.down('sm')]: {
          width:"80%",
        
      },
    },
    imageContainer:{
      minHeight:'40em',
      display:'flex',
      justifyContent:'center',
      alignItems:'center'
    },
    paper2: {
      height: '150px',
      width: '100%', 
      marginTop: '10px',
      borderRadius: '15px'
  },
  paper3: {
      height: 'auto',
      width: '100%', 
      borderRadius: '15px',
      marginTop: '10px'
  },
  paper4: {
      height: 'auto',
      width: '80%', 
      borderRadius: '20px',

      marginLeft: '10%'
      
  },
  paper5: {
    height: 'auto',
    width: '50%', 
    borderRadius: '20px',

    marginLeft: '10%'
    
},
wrapper:{
  textAlign:'left'
},
justifyRight:{
  paddingLeft:'10%'

},
timer:{
  fontFamily: 'roboto',
  fontSize: '1.5rem',
  color: '#284089',
},
detailText:{
  fontSize:'0.875rem',
  [theme.breakpoints.down('sm')]:{
    fontSize:'0.7rem'
  }
},
headerText:{
  fontSize:'1rem',
  [theme.breakpoints.down('sm')]:{
    fontSize:'0.5rem'
  }
},
detailTextHeader:{
  fontSize:'1rem',
  [theme.breakpoints.down('sm')]:{
    fontSize:'0.7rem'
  }

},
data:{
  marginLeft:'3em',
  justifyContent:'center',
  alignItems:'center',
  [theme.breakpoints.down('sm')]: {
    marginLeft:'0'
  }
},
  tabox:{
    paddingLeft:"24px",
    paddingRight:"24px",
    paddingBottom:'24px',
    paddingTop:'0px',
    [theme.breakpoints.down('sm')]: {
      paddingTop:'24px',
      paddingLeft:"0",
      paddingRight:"0",
      paddingBottom:'24px',
    }

  },
  boxReplyContainer:{
    display:"flex",width:'100%',flexDirection:'row',marginBottom:'1em', alignItems:'center',paddingLeft:'10%',
    [theme.breakpoints.down('sm')]: {
   
      paddingLeft:"0",
    
    }

  },
  boxReply:{
    width:'80%', paddingLeft:'1.5em', paddingRight:'1.5em',
    [theme.breakpoints.down('sm')]: {
     width:'100%',
      paddingLeft:"0.5em",
      paddingRight:"0.5em",
     
    }


  },
  gridList: {
    width: '100%',
    height:'auto'
   
  },


  }));

const DetailProduct =(props)=>{
    const sessionState = useSelector(state=>state.session.user)
  
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const [id, setId] = React.useState(null);

    const [data, setData] = React.useState([])

    const handleChange = (event, newValue) => {
    setValue(newValue);
    };

    const [inputVal, updateInput] = useState('')

    const [inputValue, setInputValue] = useState({})


    // const handleSubmit = event => {
    //     event.preventDefault();

    // }

    


    useEffect(() => {
      
      const param = queryString.parse(props.location.search);
      setId(param.id_barang)
      const url = [{id: 0, url: `${pingAuction}explore/forum/get-forum/${param.id_barang}`},
      {id: 1, url: `${pingAuction}explore/update-jumlah-view`}, 
      {id: 2, url: `${pingAuction}item-detail/${param.id_barang}`},
      {id: 3, url: `${pingAuction}check-role`},
      
      ]
      Promise.all(url.map(y => {
        if(y.id === 0){
          return axios.get(y.url, headers())
        }
        else if(y.id === 1){
          return axios.post(y.url, {id_barang: param.id_barang}, headers())
        }
        else if(y.id === 2){
          return axios.get(y.url, headers())
        } 
        else if(y.id === 3){
          return axios.post(y.url, {id_barang: param.id_barang}, headers())
        } 
        else{
          return axios.get(y.url, headers())
        }
      })).then(responses => {
          setData(responses)
        
        
        })
      

    },[props])

  

    const getData = () => {
      var prevData = [...data]
      axios.get(`${pingAuction}explore/forum/get-forum/${id}`, headers())
        .then(res => {
          var array = [res].concat(prevData)
          array.splice(1, 1)
          setData(array)
        
          
          })
    }

    const handleClick = async (event) => {
      await axios.post(`${pingAuction}explore/forum/post-forum`,{id_barang: id, isi: inputVal}, headers())
      updateInput('')
      getData()
      
  }  


  const handleReplyClick = async (id_for,isi) => {
    await axios.post(`${pingAuction}explore/forum/thread/post-forum/`,{id_forum: id_for, isi: isi}, headers())
    setInputValue(prev=>({...prev,[`forum-${id_for}`]:''}))
    getData()
    
}  
console.log(data)
const handleChangeReply = (id_forum)=>(event)=>{
  const {value} = event.target;

  setInputValue(prev=>({...prev,[`forum-${id_forum}`]:value}))
}
      
    const parseDate = (date) => {
      var parsedDate = new Date(date)
      var result = parsedDate.toString().split(" ")
      return result.slice(0, 5).join(" ")
    }
  
  




    return(
        <Fragment>
        {data.length === 0 ? <p>Loading</p> 
        : 
      
         <div className={classes.wrapperAll}>
       
        <div style={{display:'flex', justifyContent:'center'}}>
       
            <Paper className={classes.paper}>
                <GridList style={classes.gridlist} cols={3} cellHeight={'auto'} >
                    <GridListTile cols={1.5}>
                        <Box className={classes.imageContainer}>
                          <Carousel animation="fade" interval="4000" autoPlay="true">
                            {data[2].data.data.img.map((elm,idx)=>{
                              return(
                               <CarouselItem key={idx}>
                                  <img src={elm.url} className={classes.imageCategory} alt={elm.url} ></img>
                               </CarouselItem>
                              )
                           

                            })}
                              </Carousel>
                           
                        </Box>
                      
                    </GridListTile>
                
                    
                    <GridListTile cols={1.5}>
                        <Box className={classes.data}>
                          <h1 style={{marginBottom:"2em",paddingTop:"2em"}}>{data[2].data.data.judul}</h1> 
                          <GridList style={classes.gridlist} cols={2} cellHeight={'auto'}>
                          <GridListTile cols={1} >
                            
                              <Grid item xs={12}  direction="row">
                              <h3 style={{color: '#92929D'}} className={classes.detailTextHeader}>{data[2].data.data.lama_berlangsung} hari yang lalu</h3>
                              <h3 style={{color: '#92929D'}} className={classes.detailTextHeader}>Peserta Lelang: {data[2].data.data.jumlah_peserta} Bidder</h3>
                              </Grid>
                              <Grid item xs={12} direction="row">
                                  <h3 style={{color: '#92929D'}} className={classes.detailTextHeader}>Katergori: <b style={{color:'#000000'}}>{data[2].data.data.nama_kategori}</b></h3>
                                  <h3 style={{color: '#92929D'}} className={classes.detailTextHeader}>Merk: <b style={{color:'#000000'}}>{data[2].data.data.merk}</b></h3>
                              </Grid>
                              <Grid item xs={12} direction="row">
                                  <h3 style={{color: '#92929D'}} className={classes.detailTextHeader}>Harga Awal: <b>Rp. {data[2].data.data.harga_awal.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</b></h3>
                                  <h3 style={{color: '#92929D'}} className={classes.detailTextHeader}>Bid Terakhir: <b style={{color:'#284089'}}>Rp. {data[2].data.data.bid_terakhir===null ? 0 :data[2].data.data.bid_terakhir.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</b></h3>
                              </Grid>
                          </GridListTile>
                          <GridListTile cols={1}>
                           <CountDown masa_lelang= {data[2].data.data.masa_lelang} id={id} />
                          </GridListTile>
                          </GridList>
                        </Box>
                           
                    </GridListTile>
                   
                 
                </GridList>
            </Paper> 
      
        </div>
        
        <div style={{marginTop:"2em"}}className={classes.root}>
          <Grid container>
            <Grid item xs={12} md={3}>
              <div style={{display:"flex", flexDirection:'column'}}>
                <Box style={{backgroundColor:'white', border:"0px solid white", marginBottom:"1em",textTransform:'capitalize', textAlign:'left'}}>
                <Tabs
                  orientation="vertical"
                  variant='scrollable'
                  value={value}
                  onChange={handleChange}
                  aria-label="Vertical tabs example"
                  className={classes.tabs}
                > 
                
                    <Tab style={value===0 ?{borderLeft:"2px solid #0062FF", color:'#0062FF', textTransform:'capitalize'}:{textTransform:'capitalize'}} classes={{wrapper: classes.wrapper}} label="Deskripsi dan Diskusi Product" {...a11yProps(0)} />
                    <Tab style={value===1 ?{borderLeft:"2px solid #0062FF", color:'#0062FF',textTransform:'capitalize'}:{textTransform:'capitalize'}}classes={{wrapper: classes.wrapper}} label="Daftar Bidding" {...a11yProps(1)} />

                  
                </Tabs>
                </Box> 
                <Link to={`/explore/bidding?id_barang=${id}`} style={{textDecoration:'none'}}>
                  <ButtonBase style={{borderRadius:'10px',backgroundColor:'#284089',color:'white', width:'100%'}}><p><b>Bid Product</b></p></ButtonBase>
               
                </Link>
              </div>
              </Grid>
              <Grid item xs={12} md={9}>
              <Typography
                  component="div"
                  role="tabpanel"
                  hidden={value !== 0}
                  id={`vertical-tabpanel-${0}`}
                  aria-labelledby={`vertical-tab-${0}`}
               
                >
                  {value === 0 && <Box className={classes.tabox}>
                    <Paper  elevation={0} className={classes.profil}>
                  <Grid container>
                    <div style={{ paddingLeft: '1em', }}>
                      <Typography>
                        <h3>Deskripsi Barang</h3>
                        <p className={classes.detailText}>
                        {data[2].data.data.deskripsi}
                        </p>
                        </Typography>
                  
                    </div>
                  </Grid>
                </Paper>
                <Grid container className={classes.root}>
                              
                    <Grid item xs={12} md={12}>
                        
                        <Paper elevation={0} className={classes.paper2}>
                            <Grid container>
                                <Grid item xs={12} md={12} sm={12}>
                                    <h3 style={{marginLeft: '20px'}}>Ajukan Petanyaan</h3>
                                    <hr style={{opacity: 0.5}}></hr>
                                </Grid>
                            
                                
                                <Grid item xs={8} md={10} >
                                  <Box style={{paddingLeft:'1em', paddingRight:'1em'}}>
                                  <TextField 
                                  id="standard-basic" 
                                  label="Tanyakan"
                                  
                                  value={inputVal} 
                                  onChange={e => updateInput(e.target.value)} 
                                  style={{width: '100%'}} 
                                  className={classes.fontContent}
                                  InputProps={{
                                    classes: 
                                      {input:classes.detailText},
                                   
                                  }}
                                  InputLabelProps={{
                                    classes: 
                                      {root:classes.detailText,focused:classes.detailText},
                                   
                                  }}
                                  />
                                  </Box>
                                    
                                </Grid>
                                <Grid item xs={4} md={2}  >
                                  <Box style={{paddingRight:"2em", marginTop:'1em'}}>
                                    <Button 
                                    onClick={() => handleClick()} 
                                    variant="contained" 
                                    color="primary" 
                                    style={{width:'50%'}}
                                    classes={
                                      {label:classes.headerText}
                                    }
                                    >
                                      Kirim
                                    </Button>
                                  </Box>
                                </Grid>
                            </Grid>
                        </Paper>
                        
                        
                        <Paper elevation={0} className={classes.paper3}>
                            <Grid container>
                                <Grid item xs={12} md={12} sm={12}>
                                    <h3 style={{marginLeft: '20px'}}>Diskusi Produk</h3>
                                    <hr style={{opacity: 0.5}}></hr>
                                </Grid>
                                {data[0].data.data.map((element, index) => {
                                
                                
                                  return(
                                    <Fragment>
                                    <Paper elevation={0} className={classes.paper4} style={{marginTop: '20px', backgroundColor: '#ECF3FF'}}>
                                      <Grid container>
                                        <Grid item xs={7} >
                                            <p className={classes.detailText} style={{marginLeft: '20px', color: '#0062FF'}}>{element.role==='penjual'?<span style={{fontWeight:'bolder'}}>Penjual</span>:element.username}</p>
                                          
                                        </Grid>
                                        <Grid item xs={5}  >
                                            <p className={classes.detailText} style={{color: '#696974', paddingRight:'1em'}}>{parseDate(element.created_date)}</p>
                                            
                                        </Grid>
                                        <Grid item xs={12}>
                                        <p className={classes.detailText} style={{marginLeft: '20px',color: '#92929D', wordBreak:'break-all',paddingRight:'1em'}}>{element.isi}</p>
                                        </Grid>
                                      </Grid>
                                      <hr style={{opacity: 0.5}}></hr>

                                        {
                                          element.thread.map((trd,idx)=>{
                                            
                                            return(
                                              
                                              <Fragment>
                                              <Box style={{paddingLeft:'1.5em'}}>
                                              <Grid container>
                                                <Grid item xs={7} >
                                                    <p className={classes.detailText} style={{marginLeft: '20px', color: '#0062FF'}}>{trd.role==='penjual'?<span style={{fontWeight:'bolder'}}>Penjual</span>:trd.username}</p>
                                                  
                                                </Grid>
                                                <Grid item xs={5}  >
                                                    <p className={classes.detailText} style={{color: '#696974',paddingRight:'1em'}}>{parseDate(trd.created_date)}</p>
                                                    
                                                </Grid>
                                                <Grid item xs={12}>
                                                <p className={classes.detailText} style={{marginLeft: '20px',color: '#92929D', wordBreak:'break-all',paddingRight:'1em'}}>{trd.isi}</p>
                                                </Grid>
                                              </Grid>
                                              </Box>
                                          
                                          
                                            </Fragment>
                                              
                                            )
                                          })
                                        }
                                        <Box className={classes.boxReplyContainer}>
                                          <Box className={classes.boxReply}>
                                            <TextField
                                                id="outlined-basic"
                                                label=""
                                                placeholder="balas..."
                                                multiline
                                                rowsMax="3"
                                                style={{width:'100%'}}
                                                value={inputValue[`forum-${element.id_forum}`]}
                                                onChange={handleChangeReply(element.id_forum)}
                                                InputProps={{
                                                  classes: 
                                                    {input:classes.detailText},
                                                 
                                                }}
                                                InputLabelProps={{
                                                  classes: 
                                                    {root:classes.detailText,focused:classes.detailText},
                                                 
                                                }}
                                              />
                                          

                                          </Box>
                                          <Button color="primary" style={{marginRight:'0.5em'}} classes={
                                      {label:classes.headerText}
                                    } variant="contained" onClick={()=>handleReplyClick(element.id_forum,inputValue[`forum-${element.id_forum}`] )} >Balas</Button>
                                        </Box>
                                    </Paper>
                                  
                                    </Fragment>
            
                                  )
                                  })}
              
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
                
                    
                    </Box>}
                </Typography>
                <Typography
                  component="div"
                  role="tabpanel"
                  hidden={value !== 1}
                  id={`vertical-tabpanel-${1}`}
                  aria-labelledby={`vertical-tab-${1}`}
               
                >
                    {value === 1 && <Box className={classes.tabox}>
                    <RiwayatBid id={id}/>
                    </Box>}
                </Typography>
           

              </Grid>
            </Grid>
          </div>
             
        </div>
        }
        
            
            </Fragment>
            
            
        )
} 
export default DetailProduct