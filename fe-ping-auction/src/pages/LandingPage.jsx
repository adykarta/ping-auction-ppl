import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import  { Redirect , Link} from 'react-router-dom'
import { Button, Container, Typography, Grid } from '@material-ui/core';
import AppBarTransparent from '../components/AppBarTransparent'
import Image from'../images/top.png'
import Image1 from'../images/1.png'
import Image2 from'../images/2.png'
import Image3 from'../images/3.png'
import Image4 from'../images/4.png'
import Image5 from'../images/5.png'
import Image6 from'../images/6.png'
import Image7 from'../images/7.png'
import Image8 from'../images/8.png'

const styles = makeStyles(theme=>({
    colorPrimary:{
        backgroundColor: theme.palette.primary.main,
        color: 'white'
    },
    root:{
        fontFamily: 'ProximaNova',
        backgroundImage: `url(${Image})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: "cover",
        width: '100vw',
        height: '100vh',
        textAlign: 'center'
    },
    container:{
        width:'100vw',
        height:'100vh',
    },
    container2:{
        backgroundColor: 'white',
        width:'100%',
        height:'auto',
        justifyContent:'center',
        alignItems:'center',
        marginBottom: '5%',
        marginTop: '5%'
    },
    container3:{
        backgroundColor: theme.palette.primary.main,
        width:'100vw',
        justifyContent:'center',
        alignItems:'center',
    },
    top:{
      color: 'white',
      textAlign: 'left',
      [theme.breakpoints.up('md')]: {
        fontSize: '3em',
        paddingTop: '20vh',
      },
      [theme.breakpoints.down('md')]: {
        fontSize: '2em',
        paddingTop: '30vh',
      },
      [theme.breakpoints.down('xs')]: {
        fontSize: '1.3em',
        paddingTop: '30vh',
      },
      marginLeft: '4vw',
    },
    filter:{
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        backgroundSize: "cover",
        width:'100vw',
        height:'100vh'
    },
    registerButton:{
        color: theme.palette.primary.main,
        backgroundColor: '#FFFFF',
        borderRadius: '10px',
        textTransform: 'capitalize',
        display:'flex',
        justifyContent:'center',
        height:'auto',
        alignItems:'center',
        [theme.breakpoints.up('md')]: {
          marginTop: '10px',
          minWidth: '12vw',
          fontSize: '17px',
        },
        [theme.breakpoints.down('md')]: {
          marginTop: '10px',
          minWidth: '25vw',
          fontSize: '18px',
        },
        [theme.breakpoints.down('xs')]: {
          marginTop: '10px',
          minWidth: '35vw',
          fontSize: '13px',
        },
    },
    findButton:{
      color: 'white',
      marginTop: '10px',
      borderRadius: '10px',
      display:'flex',
      alignItems:'center',
      minWidth:"25vw",
      justifyContent:'center',
      textTransform: 'capitalize',
      [theme.breakpoints.up('md')]: {
        fontSize: '17px',
        marginBottom:'50px',
      },
      [theme.breakpoints.down('md')]: {
        fontSize: '20px',
        marginBottom:'30px',
      },
      [theme.breakpoints.down('xs')]: {
        fontSize: '13px',
        marginBottom:'10px',
      },
    },
    storeButton:{
        color: theme.palette.primary.main,
        textTransform: 'capitalize',
        borderRadius: '10px',
        display:"flex",
        justifyContent:'center',
        alignItems:'center',
        [theme.breakpoints.up('md')]: {
          marginTop: '4vh',
          marginLeft: '8%',
          marginBottom: '8vh',
          minWidth: '12vw',
          fontSize: '17px',
        },
        [theme.breakpoints.down('md')]: {
          marginTop: '2vh',
          marginLeft: '8%',
          marginBottom: '10%',
          minWidth: '25vw',
          fontSize: '17px',
        },
        [theme.breakpoints.down('xs')]: {
          marginTop: '1vh',
          marginLeft: '8%',
          marginBottom: '10%',
          minWidth: '30vw',
          fontSize: '13px',
        },
    },
    bottomWords: {
      [theme.breakpoints.up('md')]: {
        marginTop: '10%',
        marginBottom: '1.5vh',
        marginLeft:'8%',
        fontSize: '45px',
        color: 'white'
      },
      [theme.breakpoints.down('md')]: {
        marginTop: '10%',
        marginBottom: '1.5vh',
        marginLeft:'8%',
        fontSize: '40px',
        color: 'white'
      },
      [theme.breakpoints.down('xs')]: {
        marginTop: '10%',
        marginBottom: '1.5vh',
        marginLeft:'8%',
        fontSize: '20px',
        color: 'white'
      },
    },
    media: {
        margin: '10px',
    },
    card:{
        width: '22vw',
        height: '11vw'
    },
    word:{
        marginBottom: '8vh',
        textAlign: 'center',
        color: '#494949',
        display:'flex',
        alignItems:'center',
        flexDirection:'column',
    },
    wordInside: {
      fontWeight:'bold',
      [theme.breakpoints.up('sm')]: {
        marginTop: '4px',
        fontSize: '35px',
      },
      [theme.breakpoints.down('xs')]: {
        marginTop: '1px',
        fontSize: '18px',
      },
    },
    space:{
        margin: '2vh'
    },
    halfSpace:{
        margin: '1vh'
    },
    content:{
        backgroundColor: theme.palette.primary.main,
        color: 'white',
        position: 'absolute',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        left: '0',
        bottom: '0',
        [theme.breakpoints.up('md')]: {
          width: '16vw',
          height: '5vh',
          fontSize: '1.3vw',
        },
        [theme.breakpoints.down('md')]: {
          width: '40vw',
          height: '5vh',
          fontSize: '3vw',
        },
    },
    footer:{
      backgroundColor: '#D2D2D2',
      width: '100%',
      height: '10%',
      display: 'flex',
      justifyContent:'center',
      alignItems:'center',
      bottom: '0',
    },
    insideFooter: {
      [theme.breakpoints.up('xs')]: {
        fontSize: '20px',
      },
      [theme.breakpoints.down('xs')]: {
        fontSize: '16px',
      },
    },
    barButton: {
        margin: '4vh',
        fontSize: '18px'
    },
    grow: {
        flexGrow: 1,
    },
    flexCenter: {
      display:'flex',
      justifyContent:'center',
      alignItems:'center'
    }
}))




const LandingPage =()=>{
  const classes = styles()
  // eslint-disable-next-line
  const [values, setValues] = React.useState({
    redirectRegister: false,
  })


  const renderRedirectRegister = () => {
    if (values.redirectRegister === true) {
      return <Redirect to='/register'  />
    }
  }
    return(
        <Fragment>
          {renderRedirectRegister()}
            <div className={classes.root}>
                <div className={classes.filter}>
                  <AppBarTransparent/>
                    <div className={classes.container}>
                      <div className={classes.top}>
                          <strong>
                              <h3 style={{marginBottom: '0.2vw'}}>Punya barang gak kepake?</h3>
                              <h1 style={{marginTop: '0.2vw', marginBottom: '0.5vw'}}>Lelangin aja!</h1>
                          </strong>
                          <Link to="/register"  style={{textDecoration:'none', color:'black'}}>
                          <Button variant="contained" className={classes.registerButton} >
                              <strong>Daftar Lelang</strong>
                          </Button>
                          </Link>
                      </div>
                    </div>
                  </div>
                  <Container>
                    <div className={classes.container2}>
                        <Grid container>
                          <Grid xs={12} md={8} className={classes.flexCenter}>
                            <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image1})`, height: '50vh', width: '100%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                                <div className={classes.content}>
                                  <strong>Mulai dari Rp.699.000</strong>
                                </div>
                            </div>
                          </Grid>
                          <Grid xs={12} md={4} className={classes.flexCenter}>
                            <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image2})`, height: '50vh', width: '100%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                              <div className={classes.content}>
                                <strong>Mulai dari Rp.699.000</strong>
                              </div>
                            </div>
                          </Grid>
                        </Grid>
                        <Grid container>
                          <Grid xs={12} md={6} className={classes.flexCenter}>
                            <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image3})`, height: '82vh', width: '100%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                              <div className={classes.content}>
                                <strong>Mulai dari Rp.699.000</strong>
                              </div>
                            </div>
                          </Grid>
                          <Grid direction='column' xs={12} md={6}>
                            <Grid direction='row' xs={12} md={12} className={classes.flexCenter}>
                              <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image4})`, height: '40vh', width: '94.5%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                                <div className={classes.content}>
                                  <strong>Mulai dari Rp.699.000</strong>
                                </div>
                              </div>
                            </Grid>
                            <Grid direction='row' xs={12} md={12} className={classes.flexCenter}>
                              <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image5})`, height: '40vh', width: '94.5%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                                <div className={classes.content}>
                                    <strong>Mulai dari Rp.699.000</strong>
                                </div>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>
                        <Grid container>
                          <Grid xs={12} md={8} className={classes.flexCenter}>
                            <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image6})`, height: '50vh', width: '100%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                              <div className={classes.content}>
                                <strong>Mulai dari Rp.699.000</strong>  
                              </div>
                            </div>
                          </Grid>
                          <Grid xs={12} md={4} className={classes.flexCenter}>
                            <div className={classes.space} style={{position: 'relative', backgroundImage: `url(${Image7})`, height: '50vh', width: '100%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center'}}>
                              <div className={classes.content}>
                                <strong>Mulai dari Rp.699.000</strong>
                              </div>
                            </div>
                          </Grid>
                        </Grid>
                    </div>
                    <div className={classes.word}>
                        <p className={classes.wordInside} style={{marginBottom: '4px'}}>Cari barang koleksimu sekarang juga.</p>
                        <p className={classes.wordInside}>Mulai dari harga terendah!</p>
                        <Button variant="contained" color="primary" className={classes.findButton}>
                            <strong>Cari Barang</strong>
                        </Button>
                </div>
                </Container>
                <div className={classes.container3}>
                    <Grid container style={{textAlign:'left', height:'100%', overflow: 'hidden'}}>
                        <Grid xs={12} md={5}>
                            <img src={Image8} width='100%' height='100%' alt="foto" />
                        </Grid>    
                        <Grid  xs={12} md={6}>
                            <p className={classes.bottomWords}>
                                “Cara mudah menjual barang dengan harga terbaik yang belum pernah terbayangkan sebelumnya”
                            </p>
                            <Button variant="contained" color="white" className={classes.storeButton}>
                                <strong>Buka Toko</strong>
                            </Button>
                        </Grid>
                    </Grid>
                </div>
                <div className={classes.footer}>
                    <Typography variant='h6' className={classes.insideFooter}><strong>Ping Auction, All rights reserved</strong></Typography>
                </div>
            </div>
        </Fragment>
    )
}
export default LandingPage