import React, { Fragment, useEffect, useState } from 'react';
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import Carousel from 'react-material-ui-carousel'
import CarouselItem from 'react-material-ui-carousel'
import Properti from '../images/propertiCategory.svg';
import MenFashion from '../images/menFashionCategory.svg';
import WomenFashion from '../images/womenFashionCategory.svg';
import Electronic from '../images/electronicCategory.svg';
import Antique from '../images/antiqueCategory.svg';
import {Link} from 'react-router-dom'
import {Grid, Paper, Button, ButtonGroup, Grow, Popper, ClickAwayListener, MenuList, MenuItem, Fab, Box} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import AddIcon from '@material-ui/icons/Add';
import {headers, pingAuction} from '../config';
import CountDown from '../components/CountDown';


const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      height: 'auto',
      width: '85%',
      borderRadius: '5px',
      marginTop: '15px',
      '&:hover': {
        background: "#fffcf3",
        transition: '1.5s'
        },

    },
    imageCategory: {
      padding: theme.spacing(2),
      alignItems: 'center',

    },
    itemsImage: {
        width: '80%',
        height: 'auto',
        padding: '20px',
        
    },

    timerBid: {
        fontFamily: 'roboto',
        fontSize: '30px',
        color: '#284089',
        // marginLeft: '15px',
        lineHeight: 0
    
    },
    bidButton: {
        width: '30px',
        marginTop: '15px',
        // marginLeft: '15px',

    },
    tr: {
        background: "#f1f1f1",
        '&:hover': {
           background: "yellow",
        },
    },
    floatBtn: {
        '& > *': {
            margin: theme.spacing(1),
        },
        position: 'fixed',
        bottom: 50,
        right: 50,
        zIndex:'10000'

    },
    detailText:{
        fontFamily: 'roboto',
        color: '#92929D',
        lineHeight: 1,
        fontSize:'0.875rem',
        [theme.breakpoints.down('sm')]:{
          fontSize:'0.7rem'
        }
      },
      headerText:{
        fontSize:'1rem',
        [theme.breakpoints.down('sm')]:{
          fontSize:'0.5rem'
        }
      },
    paper2: {
        height: '150px',
        width: '60%', 
        borderRadius: '15px'
    },
    paper3: {
        height: 'auto',
        width: '60%', 
        borderRadius: '15px',
        marginTop: '10px'
    },
    paper4: {
        height: 'auto',
        width: '80%', 
        borderRadius: '20px',
        marginLeft: '10%' 
        
    },
  }));

const Homepage =()=>{
    // const dispatch = useDispatch()
    const classes = useStyles();
    const options = ['Default', 'Date and time', 'The chepeaset', 'Most expensive'];
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleMenuItemClick = (event, index) => {
        setSelectedIndex(index);
        setOpen(false);
    };
    const handleToggle = () => {
        setOpen(prevOpen => !prevOpen);
    };
    const handleClose = event => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
          return;
        }
    
        setOpen(false);
    };

    const [items, setItems] = useState([])

 

    useEffect(() => {
        axios.get(`${pingAuction}todays-item`, headers())
        .then(res => {
            const items = res.data
            console.log(res)
            setItems(items) 

        })
    },[])
    console.log(items) 

    const [value, setValue] = useState([])
 
    useEffect(() => {
        axios.get(`${pingAuction}explore/get-item/jumlah-bidder`, headers())
        .then(res => {
            console.log(res)
            const value = res.data.data
            setValue(value)

        })
    },[])
    console.log(value)
    

    


    return(
        <Fragment>
            
            {value.length === 0 ? <p>Loading...</p> 
                : 
                <div style={{marginLeft: '10%'}}>
                    <Link to="/listing/create-barang"> 
                    <Fab className={classes.floatBtn} color="primary" aria-label="add">
                     <AddIcon />
                    </Fab>
                    </Link> 
                    
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={12}>
                            <h2>Promosi</h2>
                            <Carousel animation="fade" interval="4000" autoPlay="true">
                            {value.map(value => (
                            
                                <CarouselItem>
                                    <Link style={{textDecoration: 'none'}} to={`/detail-product?id_barang=${value.id_barang}`}>
                                    <Paper elevation={0} className={classes.paper} >
                                        <Grid container>
                                            <Grid item xs={12} md={4} sm={12} direction="column">
                                                <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                                    <img src={value.img[0].url} className={classes.itemsImage} alt={value.img[0].url}></img>
                                                </Box>   
                                            </Grid>
                                            <Grid item xs={12} md={5} sm={7} direction="column"> 
                                                <Box style={{marginLeft: '10px'}}>
                                                    <h3>{value.judul}</h3> 
                                                    <p className={classes.detailText}>{value.lama_berlangsung} hari yang lalu</p>
                                                    <p className={classes.detailText}>Peserta Lelang: {value.jumlah_peserta} Bidder</p>
                                                    <p className={classes.detailText}>Harga Awal: Rp {value.harga_awal.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</p>
                                                    <p className={classes.detailText}>Bid Terahir: Rp <b style={{color:'#284089'}}>{value.bid_terakhir===null ? 0: value.bid_terakhir.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</b></p> 
                                                </Box>
                                            </Grid>
                                            <Grid item xs={5} md={3} sm={5} direction="column">
                                                <Box style={{marginLeft: '30px'}}>
                                                    <p style={{color: '#92929D'}} className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                                    <CountDown id={value.id_barang} masa_lelang={value.masa_lelang} />
                                                    <Link to={`/explore/bidding?id_barang=${value.id_barang}`} style={{textDecoration:"none"}}>
                                                        <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%'}}
                                                        classes={
                                                        {label:classes.headerText}
                                                        }>Bid</Button>
                                                    </Link>
                                                </Box>
                                                
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                    </Link>
                                </CarouselItem>
                          
                                ))}
                            </Carousel>
                            
                        </Grid>
                        <Grid item xs={12} md={9} data-testid="custom-element">
                            <h2 data-testid = "category">Kategori</h2>
                        </Grid>
                        <Grid item xs={12} md={2}>
                            <Link TO="/explore" style={{color: "#50B5FF", alignItems: 'left', textDecoration:'none'}}>Lihat semua</Link>
                        </Grid>
                        <Grid item xs={12} md={12}>
                          <img src={Properti} className={classes.imageCategory} alt="properti" ></img>
                            <img src={MenFashion} className={classes.imageCategory} alt="menfasshion"></img>
                           <img src={WomenFashion} className={classes.imageCategory} alt="womenfashion"></img>
                           <img src={Electronic} className={classes.imageCategory} alt="electronic"></img>
                           <img src={Antique} className={classes.imageCategory} alt="antique"></img>
                        </Grid>
                        <Grid item xs={12} md={8}>
                            <h2 data-testid = "today's auction">Lelang Hari Ini</h2>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <ButtonGroup variant="contained" color="primary" ref={anchorRef} aria-label="split button">
                                <Button data-testid = "sortby">Sort By: <b>{options[selectedIndex]}</b></Button>
                                <Button
                                    color="primary"
                                    size="small"
                                    // aria-controls={open ? 'split-button-menu' : undefined}
                                    // aria-expanded={open ? 'true' : undefined}
                                    // aria-label="select merge strategy"
                                    // aria-haspopup="menu"
                                    onClick={handleToggle}
                                >
                                    <ArrowDropDownIcon />
                                </Button>
                            </ButtonGroup>
                            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                                {({ TransitionProps, placement }) => (
                                    <Grow
                                        {...TransitionProps}
                                        style={{
                                            transformOrigin: placement === 'center top' ? 'bottom' : 'center bottom',
                                        }}
                                    >
                                        <Paper>
                                            <ClickAwayListener onClickAway={handleClose}>
                                                <MenuList id="split-button-menu">
                                                    {options.map((option, index) => (
                                                        <MenuItem
                                                            // key={option}
                                                            selected={index === selectedIndex}
                                                            onClick={event => handleMenuItemClick(event, index)}
                                                        >
                                                            {option}
                                                        </MenuItem>
                                                    ))}
                                                </MenuList>
                                            </ClickAwayListener>
                                        </Paper>
                                    </Grow>
                                )}
                            </Popper>
                        </Grid>

                        <Grid item xs={12} md={12} sm={12}>
                            {items.map(items => (
                                <Link style={{textDecoration: 'none'}} to={`/detail-product?id_barang=${items.id_barang}`}>
                                    <Paper elevation={0} className={classes.paper}>
                                        <Grid container>
                                            <Grid item xs={12} md={4} sm={12} direction="column">
                                                <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                                    <img src={items.img[0].url} className={classes.itemsImage} alt={items.img[0].url} ></img>
                                                </Box>   
                                            </Grid>
                                            <Grid item xs={12} md={5} sm={7} direction="column"> 
                                                <Box style={{marginLeft: '10px'}}>
                                                    <h3>{items.judul}</h3> 
                                                    <p className={classes.detailText}>{items.lama_berlangsung} hari yang lalu</p>
                                                    <p className={classes.detailText}>Peserta Lelang: 257 Bidder</p>
                                                    <p className={classes.detailText}>Harga Awal: Rp {items.harga_awal.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</p>
                                                    <p className={classes.detailText}>Bid Terahir: Rp <b style={{color:'#284089'}}>{items.bid_terakhir ===null ? 0:items.bid_terakhir.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</b></p> 
                                                </Box>
                                            </Grid>
                                            <Grid item xs={5} md={3} sm={5} direction="column">
                                                <Box style={{marginLeft: '20px'}}>
                                                    <p style={{color: '#92929D'}}  className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                                    <CountDown id={items.id_barang} masa_lelang={items.masa_lelang} />
                                                    <Link to={`/explore/bidding?id_barang=${items.id_barang}`} style={{textDecoration:"none"}}>
                                                    <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%'}}
                                                        classes={
                                                        {label:classes.headerText}
                                                        }>Bid</Button>
                                                        </Link>
                                                </Box>
                                                
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Link>
                            ))}
                            

                        </Grid>
                        
                    </Grid>
                
                </div>
            }
        </Fragment>
    )
} 
export default Homepage
