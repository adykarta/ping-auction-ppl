import React,{useState,useEffect,Fragment} from 'react';
import axios from 'axios';
import {headers, pingAuction} from '../../config';
import {Grid, Paper, Box} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme=>({
  detailText:{
    fontSize:'0.875rem',
    [theme.breakpoints.down('sm')]:{
      fontSize:'0.7rem'
    }
  },

}))

const RiwayatBid =(props)=>{
  const classes = useStyles()
    const [data, setData] = useState([])
    const [isLoading ,setIsLoading] = useState(true)

    const parseDate = (date) => {
      var parsedDate = new Date(date)
      var result = parsedDate.toString().split(" ")
      return result.slice(0, 5).join(" ")
    }
    useEffect(()=>{
        axios.get(`${pingAuction}bidding/riwayat-bid/${props.id}/`, headers())
        .then(res=>{
            setData(res.data.data)
            setIsLoading(false)
        })

    },[])
    console.log(data)
    return(
        <Fragment>
           {
               isLoading===true ? <p>Mohon tunggu...</p>:
               data.map((elm,idx)=>{
                   return(
                    <Paper>
                        <Box style={{paddingLeft:'1.5em' ,marginBottom:'1.5em'}}>
                            <Grid container>
                                <Grid item xs={7} >
                                  <p className={classes.detailText} style={{marginLeft: '20px', color: '#0062FF'}}>+ Rp {elm.jumlah.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</p>
                                </Grid>
                                <Grid item xs={3}  >
                                  <p className={classes.detailText} style={{color: '#696974',paddingRight:'1em'}}>{parseDate(elm.created_date)}</p>
                                </Grid>
                                <Grid item xs={2}  >
                                  <p className={classes.detailText} style={{color: '#696974',paddingRight:'1em'}}>{elm.username}</p>
                                </Grid>
                            </Grid>
                        </Box>
                    </Paper>

                   )
               })

           }
        </Fragment>

    )
}
export default RiwayatBid