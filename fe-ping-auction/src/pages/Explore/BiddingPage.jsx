import axios from 'axios';
import queryString from 'query-string';
import { Redirect } from "react-router-dom";
import MuiAlert from '@material-ui/lab/Alert';
import { pingAuction, headers } from '../../config';
import React, { Fragment, useEffect } from 'react';
import CountDown from '../../components/CountDown'
import { makeStyles, Grid, Typography, Box, Button, Tooltip, Slider, Input, Snackbar } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  space: {
    [theme.breakpoints.up('415')]: {
      marginLeft: '10%',
      marginRight: '10%',
    },
    [theme.breakpoints.down('415')]: {
      marginLeft: '10%',
      marginRight: '10%',
    },
    [theme.breakpoints.down('376')]: {
      marginLeft: '8%',
      marginRight: '10%',
    },
    [theme.breakpoints.down('361')]: {
      marginLeft: '13%',
      marginRight: '10%',
    },
    [theme.breakpoints.down('321')]: {
      marginLeft: '7%',
      marginRight: '5%',
    },
  },
  space1:{
    margin: '20px'
  },
  paperTime:{
    width: '100%',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
  },
  paperProduct: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: '0 0 3px 3px',
    boxShadow: '3',
    marginBottom: '30px'
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  left: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  right: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  boxImage: {
    borderRadius: 16,
    margin: '13%',
    backgroundColor: '#BDBDBD',
    textAlign: 'center',
  },
  image:{
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(1),
    },
    [theme.breakpoints.down('md')]: {
      padding: '8px 8px 0 8px',
      maxWidth: '130px',
      height: '130px',
    },
    [theme.breakpoints.down('xs')]: {
      padding: '8px 8px 0 8px',
      width: '105px',
      height: '105px',
    },
    [theme.breakpoints.down('376')]: {
      padding: '8px 8px 0 8px',
      width: '95px',
      height: '95px',
    },
    [theme.breakpoints.down('361')]: {
      padding: '8px 8px 0 8px',
      width: '85px',
      height: '85px',
    },
    [theme.breakpoints.down('321')]: {
      padding: '8px 8px 0 8px',
      width: '85px',
      height: '85px',
    },
    maxWidth: '230px',
    height: '180px',
    alignItems: 'center'
  },
  paperSpace:{
    margin: '6%',
  },
  chooseYes: {
    width: '100%',
    padding: '10px',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    marginBottom: '20px',
    '&:hover': {
      backgroundColor: '#1b2c5e',
    },
  },
  chooseNo: {
    width: '100%',
    padding: '10px',
    backgroundColor: '#BDBDBD',
    color: 'white',
    marginBottom: '20px',
    '&:hover': {
      backgroundColor: '#999999',
    },
  },
  bidValue: {
    width: '50%',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    marginBottom: '20px',
    borderRadius: 16,
    '&:hover': {
      backgroundColor: '#1b2c5e',
    },
  },
  hargaAwal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'flex-start',
    },
  },
}));

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const BiddingPage = (props) => {
  const classes = useStyles();
  const [lastBid, setLastBid] = React.useState(0)
  const [, setId] = React.useState(null);
  const [details, setDetails] = React.useState([]);
  const [price, setPrice] = React.useState(0)
  const [values, setValues] = React.useState({
    selected: null,
    refresh: false,
    leave: false,
  });
  const [value, setValue] = React.useState(0);
  const [showValid, setShowValid] = React.useState(false);
  const [showError, setShowError] = React.useState(false);

  const handleSliderChange = (event, newValue) => {
    setValue(newValue);
    setPrice(lastBid + newValue);
  };

  const handleInputChange = (event) => {
    setValue(event.target.value === '' ? '' : Number(event.target.value));
  };

  const handleBlur = () => {
    if (value < 0) {
      setValue(0);
    } else if (value > 1000000) {
      setValue(1000000);
    }
  };

  const validate = (data) => {
    if (data.status === 200) {
      setShowValid(true);
    } else {
      setShowError(true);
    }
  }

  const handleValue = (param, idx) => {
    setPrice(lastBid + param)
    setValues({ selected: idx })
  }

  useEffect(() => {
    const param = queryString.parse(props.location.search);
    setId(param.id_barang)
    axios.get(`${pingAuction}item-detail/${param.id_barang}`, headers())
      .then(res => {
        res.data.data.kelipatan_bid = res.data.data.kelipatan_bid.sort((a, b) => a - b)
        const item = res.data.data
        if (item.bid_terakhir === null) {
          setLastBid(item.harga_awal)
          setPrice(item.harga_awal)
        } else {
          setLastBid(item.bid_terakhir)
          setPrice(item.bid_terakhir + item.kelipatan_bid[0])
          setValues({ selected: 0 })
        }
        setDetails(item)
        console.log(item)
      })
  }, [])

  const bidProduct = () => {
    axios.post(`${pingAuction}bidding/bid-product`, { id_barang: details.id_barang, harga_now: price, jumlah_bid: price - lastBid }, headers())
      .then(res => {
          validate(res.data)
      })
  }
  
  const handleShut = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setShowError(false);
    setValues({ refresh: true })
  };

  const handleValid = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setShowValid(false);
    setValues({ leave: true })
  };

  const renderRefresh = () => {
    if (values.refresh === true) {
      return window.location.reload();
    }
  }

  const renderRedirect = () => {
    if (values.leave === true) {
      return <Redirect to='/explore' />;
    }
  }

  

  return (
    <Fragment>
      <Snackbar open={showValid} autoHideDuration={4000} onClose={handleValid}>
        <Alert onClose={handleValid} severity="success">
          Bid anda telah berhasil!
        </Alert>
      </Snackbar>
      <Snackbar open={showError} autoHideDuration={6000} onClose={handleShut}>
        <Alert onClose={handleShut} severity="error">
          Gagal melakukan bid!
        </Alert>
      </Snackbar>
      {renderRefresh()}
      {renderRedirect()}
      {lastBid === 0 ? <p>Loading...</p> :
      details.length === 0 ? <p>Loading...</p>
        : 
      <div className={classes.space}>
        <Box boxShadow={3} className={classes.paperTime}>
          <Grid container>
            <Grid item xs={5} md={5} style={{display:'flex', alignItems:'center', flexDirection:'column', paddingTop:"2em"}}>
              <Typography variant='body2' style={{ textAlign: 'center' }}>Sisa waktu lelang</Typography>
            
              <CountDown id={details.id_barang} masa_lelang={details.masa_lelang} color="#FF974B"/>
          
            </Grid>
            <hr />
            <Grid item xs={6} md={6} style={{display:'flex', alignItems:'center', flexDirection:'column', paddingTop:"2em"}}>
              <Typography variant='body2' style={{ textAlign: 'center' }}>Peserta lelang</Typography>
              <Typography variant='body2' style={{ textAlign: 'center', marginTop: '10px' }}>{details.jumlah_peserta} bidder</Typography>
            </Grid>
          </Grid>
        </Box>
        <Box boxShadow={3} className={classes.paperProduct}>
          <Grid container wrap='nowrap'>
            <Grid item xs={12} sm={4} md={4}>
              <Box className={classes.boxImage}>
                <img src={details.img[0].url} className={classes.image} alt={details.img[0].url}/>
              </Box>
            </Grid>
            <Grid item xs={12} sm={8} md={8}>
              <div className={classes.paperSpace}>
                <Typography noWrap variant='subtitle2'>{details.judul}</Typography>
                <Typography variant='body2' style={{ marginBottom: '12%', color: '#BDBDBD' }}>{details.lama_berlangsung === 0 ? "Baru hari ini" : details.lama_berlangsung +" hari yang lalu"}</Typography>
                <Grid container>
                  <Grid item xs={12} sm={6} md={6}>
                    <Typography variant='subtitle2' style={{ color: '#BDBDBD' }}>Bid terakhir:</Typography>
                    <Typography variant='subtitle2'><b>Rp. {details.bid_terakhir === null ? 0 : details.bid_terakhir.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</b></Typography>
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Grid container>
                      <Grid item xs={12} md={12} className={classes.hargaAwal}>
                        <Typography variant='subtitle2' style={{ color: '#BDBDBD' }}>Harga awal:</Typography>
                      </Grid>
                      <Grid item xs={12} md={12} className={classes.hargaAwal}>
                        <Typography variant='subtitle2' style={{ color: '#BDBDBD' }}>Rp. {details.harga_awal.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </Box>
        <Grid container>
          <Grid item xs={12} md={12}>
            <Typography variant='subtitle2' style={{ marginBottom: '10px' }}>Kelipatan Bid</Typography>
          </Grid>
              {details.kelipatan_bid.map((elm, idx) => {
              return (
                <Grid item xs={12} md={12}>
                  <Button onClick={() => handleValue(elm,idx)} className={values.selected === idx ? classes.chooseYes : classes.chooseNo}>Rp. {elm.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</Button>
                </Grid>
              )
            })}
          <Grid item xs={12} md={12}>
            <Button onClick={() => handleValue(0, 999)} className={values.selected === 999 ? classes.chooseYes : classes.chooseNo}>Custom Nilai Bid</Button>
          </Grid>
          <Grid item xs={12} md={12}>
                <Box display={values.selected === 999 ? 'block' : 'none'}>
              <div className={classes.space1}>
                <Typography variant='subtitle2' style={{ margin: '10px 0' }}>Custom Nilai Bid</Typography>
                <Slider
                  value={typeof value === 'number' ? value : 0}
                  ValueLabelComponent={ValueLabelComponent}
                  onChange={handleSliderChange}
                  aria-labelledby="input-slider"
                  step={details.kelipatan_bid[0]}
                  min={0}
                  max={1000000}
                />
                <Input
                  className={classes.input}
                  value={value}
                  margin="dense"
                  onChange={handleInputChange}
                  onBlur={handleBlur}
                  inputProps={{
                    step: details.kelipatan_bid[0],
                    min: 0,
                    max: 1000000,
                    type: 'number',
                    'aria-labelledby': 'input-slider',
                  }}
                />
              </div>
            </Box>
          </Grid>
          <Grid item xs={12} md={12} className={classes.center}>
              <Button onClick={() => bidProduct()} className={classes.bidValue}>BID - Rp. {price.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</Button>
          </Grid>
        </Grid>
      </div>
        }
    </Fragment>
  )
}
export default BiddingPage