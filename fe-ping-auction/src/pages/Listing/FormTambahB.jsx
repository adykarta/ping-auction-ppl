import React, { Fragment,useMemo, useEffect, useState} from 'react';

import {KeyboardDateTimePicker,MuiPickersUtilsProvider } from "@material-ui/pickers";
import axios from 'axios'
import { useDropzone } from "react-dropzone";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import { makeStyles,useTheme } from '@material-ui/core/styles';
import {headers, pingAuction} from '../../config';
import FormControl from '@material-ui/core/FormControl';
import Chip from '@material-ui/core/Chip';
import {Redirect} from 'react-router-dom'



const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: '10px',
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#F2F2F2",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",

};
function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};



const activeStyle = {
  borderColor: "#2196f3"
};

const acceptStyle = {
  borderColor: "#00e676"
};

const rejectStyle = {
  borderColor: "#ff1744"
};

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16
};

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: "auto",
  height: 100,
  padding: 4,
  boxSizing: "border-box"
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden"
};

const img = {
  display: "block",
  width: "auto",
  height: "100%"
};
const useStyles = makeStyles((theme) => ({
  paper: {
    
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  root:{
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  photo:{
    width:"400px",
    [theme.breakpoints.down('xs')]: {
      width:"100%",
    }
  },
  formWrapper:{
    width:'50%',
    [theme.breakpoints.down('xs')]: {
      width:"100%",
  },

  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
}
}));


export default function FormTambahB() {
  const theme = useTheme();

  const [listCategory, setListCategory] = useState([])
  const [imgFull, setImgFull] = useState(false)
  const [disabled, setDisable] = useState(true)
  const [isLoading, SetIsLoading] = useState(false)
  const [redirect, setRedirect] = useState(false)
  const [values, setValues] = React.useState({
    file : [],
    judul : '',
    deskripsi : '',
    harga_awal : '',
    harga_akhir :'',
    masa_lelang: new Date(),
   id_kategori:'',
    kelipatan_bid : [],
    isNew : '',
    merk : '',
   
  });
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
    open
  } = useDropzone({
    accept: "image/*",
    noClick: true,
    noKeyboard: true,
    onDrop: acceptedFiles => {
      var val = acceptedFiles.map(file =>
            Object.assign(file, {
             preview: URL.createObjectURL(file)
            })
           ) 
      
      setValues(prev=>({...prev,file:[...prev['file'].concat(val)]}))
  
    }
  });
  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {})
    }),
    [isDragActive, isDragReject,isDragAccept]
  );

  const thumbs = values.file.map(file => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} alt={file.preview} />
      </div>
    </div>
  ));

  useEffect(()=>{
    axios.get(`${pingAuction}get-kategori`).
    then(res=>{
     
      setListCategory(res.data.data)
    })

  },[])
  const checkProperties=(obj)=> {
    for (var key in obj) {
        if (obj[key] === null || obj[key] === "")
            return true;
    }
    return false;
  }

  useEffect(()=>{
    if(values.file.length===6){
      setImgFull(true)
    }
    var cloneVal = Object.assign({}, values);
    delete cloneVal['file'];
    delete cloneVal['kelipatan_bid'];
    var isEmpty = checkProperties(cloneVal)
    // const isEmpty = !Object.values(cloneVal).some(x => (x !== null && x !== ''));
    const isEmptyKelipatanBid = values.kelipatan_bid.length===0?true:false
    const isEmptyFile = values.file.length===0 ?true :false
    if(isEmpty===false && isEmptyFile===false && isEmptyKelipatanBid===false){
      setDisable(false)
    }
    else{
      setDisable(true)
    }
    
  },[values])

  const classes = useStyles();
  
  
  
  const handleSubmit = () => {
    SetIsLoading(true)
    var formData = new FormData();
    values.file.map((el)=>{
      formData.append('file',el)
    })
    formData.append('judul', values.judul)
    formData.append('deskripsi', values.deskripsi)
    formData.append('harga_awal', values.harga_awal)
    formData.append('harga_akhir', values.harga_akhir)
    formData.append('masa_lelang', values.masa_lelang)
    formData.append('kelipatan_bid', values.kelipatan_bid)
    formData.append('isNew', values.isNew)
    formData.append('merk', values.merk)
    formData.append('id_kategori', values.id_kategori)

    axios.post(`${pingAuction}listing/create-item`, formData, headers()).then(
      res=>{
        alert('success')
        console.log(res)
        setRedirect(true)     
       }
    )

  };
 


  const handleInput = (form) => event => {
    const {value} = event.target 

    if(form === 'harga_awal'){
      var phoneno = /^[0-9\b]+$/;
            if(phoneno.test(value) || value ==='') {
                setValues(prev=>({...prev, harga_awal:value}))
                values.hargawalerror = false;
            }  
            else {
                values.hargawalerror = false;
                return false;
            }
       }
       else if(form === 'harga_akhir'){
        var phonenos = /^[0-9\b]+$/;
              if(phonenos.test(value) || value ==='') {
                  setValues(prev=>({...prev, harga_akhir:value}))
                  values.hargaakhirerror = false;
              }  
              else {
                  values.harga_akhir = false;
                  return false;
              }
        }
        else{
          setValues(prev=>({...prev,[form]:value}))
        }
    }
  const handleChange = (event) => {
      setValues({...values, kelipatan_bid:event.target.value});
  };

  const handleDateChange = (event,date) =>{
    setValues({...values, masa_lelang:date});
  }
  return (
    <Fragment>
    {
      redirect ===true ?
      <Redirect to="/explore"/>
      :
      listCategory.length===0 ?
      <p>loading</p>
      :
    <Fragment>
      {
        isLoading ===true ? <p>loading</p>:
      
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" style={{fontWeight:'bold', marginBottom:'1em'}}>
          Tambah Barang
        </Typography>
        <div className="container">
          {
            imgFull === true ? 
            <p>Sudah mencapai batas maksimum 6 foto</p>
            :
            <div {...getRootProps({ style })} className={classes.photo} >
              <input {...getInputProps()} />
              <button type="button"
              style={{backgroundColor:'#E0E0E0',borderRadius:'100%',height:'50px',width:'50px',fontSize:'30px'}}
              onClick={open}>
                +
              </button>
              <b style={{color:'#828282'}}>Foto Barang</b>
              
            </div>
          }
          <aside style={thumbsContainer}>{thumbs}</aside>
          </div>
        <div className={classes.formWrapper}>
          
          
  <TextField id="standard-basic" 
  label="Judul Listing"
  values={values.judul}
  placeholder="Judul Listing"
  onChange={handleInput('judul')}
  fullWidth
  />
  <TextField
    id="standard-textarea"
    label="Deksripsi"
    placeholder="Dekripsi"
    values={values.deskripsi}
    multiline
    onChange={handleInput('deskripsi')}
    fullWidth
  />
  <TextField
    id="standard-textarea"
    label="Merk"
    placeholder="Merk"
    values={values.merk}
    multiline
    fullWidth
    onChange={handleInput('merk')}
  />
  <FormControl fullWidth style={{marginTop:'10px'}}>
    <InputLabel htmlFor="standard-adornment-amount">Harga Awal</InputLabel>
    <Input
      id="standard-adornment-amount"
      value={values.harga_awal}
      onChange={handleInput('harga_awal')}
      startAdornment={<InputAdornment position="start">Rp</InputAdornment>}
    />
  </FormControl>
  <FormControl fullWidth style={{marginTop:'10px'}}>
    <InputLabel htmlFor="standard-adornment-amount">Harga Akhir</InputLabel>
    <Input
      id="standard-adornment-amount"
      value={values.harga_akhir}
      onChange={handleInput('harga_akhir')}
      startAdornment={<InputAdornment position="start">Rp</InputAdornment>}
    />
  </FormControl>
  <MuiPickersUtilsProvider utils={DateFnsUtils}>
    <KeyboardDateTimePicker
    fullWidth
    variant="inline"
    ampm={false}
    label="Akhir lelang"
    value={values.masa_lelang}
    onChange={handleDateChange}
    onError={console.log}
    disablePast
    format="yyyy/MM/dd HH:mm"
  />
  </MuiPickersUtilsProvider>
  <FormControl fullWidth style={{marginTop:'2px'}}>
  <InputLabel id="demo-controlled-kondisi-select-label">Kondisi</InputLabel>
  <Select
    labelId="demo-controlled-kondisi-select-label"
    id="demo-controlled-Bukain-select"
   
    value={values.kondisi}
    onChange={handleInput('isNew')}
  >
    <MenuItem value="">
      <em>None</em>
    </MenuItem>
    <MenuItem value={'false'}>Bekas</MenuItem>
    <MenuItem value={'true'}>Baru</MenuItem>

  </Select>
</FormControl>
<FormControl fullWidth style={{marginTop:'2px'}}>
  <InputLabel id="demo-controlled-kategori-select-label">Kategori</InputLabel>
  <Select
    labelId="demo-controlled-kategori-select-label"
    id="demo-controlled-kategori-select"

  
    value={values.id_kategori}
    onChange={handleInput('id_kategori')}
  >
    <MenuItem value="">
      <em>None</em>
    </MenuItem>
    {
      listCategory.map((elm, idx)=>{
        return(
          <MenuItem key={idx} value={elm.id_kategori} >{elm.nama_kategori}</MenuItem>
          
        )
      })
    }
   

  </Select>
</FormControl>
    {/* <FormControl fullWidth style={{marginTop:'2px'}}>
  <InputLabel id="demo-controlled-Bukain-select-label">Kelipatan Bid</InputLabel>
  <Select
    labelId="demo-controlled-Bukain-select-label"
    id="demo-controlled-Bukain-select"

  
  
    value={values.kelipatan_bid}
    onChange={handleInput('kelipatan_bid')}
  >
    <MenuItem value="">
      <em>None</em>
    </MenuItem>
    <MenuItem value={50000}>50.000</MenuItem>
    <MenuItem value={100000}>100.000</MenuItem>
    <MenuItem value={200000}>200.000</MenuItem>
  </Select>
</FormControl> */}
<FormControl fullWidth className={classes.formControl}>
        <InputLabel id="demo-mutiple-chip-label">Kelipatan Bid</InputLabel>
        <Select
          labelId="demo-mutiple-chip-label"
          id="demo-mutiple-chip"
          multiple
          value={values.kelipatan_bid}
          onChange={handleChange}
          input={<Input id="select-multiple-chip" />}
          renderValue={(selected) => (
            <div className={classes.chips}>
              {selected.map((value) => (
                <Chip key={value} label={value} className={classes.chip} />
              ))}
            </div>
          )}
          MenuProps={MenuProps}
        >
    <MenuItem value={10000} style={getStyles(10000, values.kelipatan_bid, theme)}>10.000</MenuItem>
    <MenuItem value={20000} style={getStyles(20000, values.kelipatan_bid, theme)}>20.000</MenuItem>
    <MenuItem value={50000} style={getStyles(50000, values.kelipatan_bid, theme)}>50.000</MenuItem>
    <MenuItem value={100000} style={getStyles(100000, values.kelipatan_bid, theme)}>100.000</MenuItem>
        
        </Select>
  </FormControl>
    <Button
      type="submit"
      fullWidth
      variant="contained"
      color="primary"
      className={classes.submit}
      onClick={()=>handleSubmit()}
      disabled = {disabled===true ? true:false}
    >
      Lelang Barang
    </Button>
    </div>
        </div>
     

  
    }
    </Fragment>
    }
    </Fragment>
  );
}