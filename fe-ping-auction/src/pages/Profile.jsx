import React, { Fragment, useEffect, useState } from 'react';
import { MemoryRouter, Route } from 'react-router';
import moment from 'moment';
import axios from 'axios';
import { makeStyles, lighten, withStyles } from '@material-ui/core/styles';
import { Grid, Paper, Button, AppBar, Tabs, Tab, Typography, Box, LinearProgress, Divider, fade, InputBase, Snackbar } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Rating, Pagination, PaginationItem } from '@material-ui/lab';
import MuiAlert from '@material-ui/lab/Alert';
import Display from  '../images/mad.jpeg';
import { Link } from 'react-router-dom';
import { pingAuction, headers } from '../config';
import EditProfile from '../components/EditProfile';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    borderRadius: 20,
    backgroundColor: lighten('#D3D3D3', 0.5),
  },
  bar: {
    borderRadius: 20,
    backgroundColor: '#F2994A',
  },
})(LinearProgress);

function weeksBetween(d1, d2) {
  return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  photo:{
    borderRadius: '50%',
    width: '12vw',
    height: '12vw',
  },
  top:{
    backgroundColor: '#F4F4F4',
    padding: '2vw 0',
    color: '#494949',
  },
  center:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  left: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  right: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  profil:{
    borderRadius: theme.shape.borderRadius,
    fontSize: '13px',
  },
  points: {
    backgroundColor: '#F4F4F4',
    color: '#494949',
    height: '100%',
    width: '100%',
    padding: '0 1vw',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper:{
    backgroundColor: '#EEEEEE',
    [theme.breakpoints.up('sm')]: {
      height: '10vw',
    },
    [theme.breakpoints.down('sm')]: {
      height: '15vw',
    },
    [theme.breakpoints.down('xs')]: {
      height: '27vw',
    },
    width: '100%',
    // margin: '2%',
    padding: '4%',
    borderRadius: '20px',
  },
  imageCategory: {
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(1),
    },
    [theme.breakpoints.down('md')]: {
      padding: '8px 8px 0 8px',
    },
    width: '80px',
    height:'80px',
    alignItems: 'center'
  },
  detailText: {
    fontFamily: 'roboto',
    fontSize: '14px',
    color: '#92929D',
    lineHeight: 1
  },
  timerBid: {
    fontFamily: 'roboto',
    fontSize: '30px',
    color: '#284089',
    marginLeft: '15px',
    lineHeight: 0
  },
  bidButton: {
    width: '30px',
    marginTop: '15px',
    marginLeft: '35px',
  },
  statusBid:{
    fontFamily: 'ProximaNova',
    width: '70%',
    padding: '5% 0',
    textAlign: 'center',
    borderRadius: '10px',
    backgroundColor: '#03AD00',
    color: 'white',
  },
  bidGreen:{
    fontFamily: 'ProximaNova',
    width: '70%',
    borderRadius: '10px',
    backgroundColor: '#03AD00',
    color: 'white',
    '&:hover': {
      backgroundColor: '#03AD00',
      color: 'white',
    }
  },
  bidAgain:{
    fontFamily: 'ProximaNova',
    width: '70%',
    borderRadius: '10px',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    '&:hover': {
      backgroundColor: fade(theme.palette.primary.main, 0.7),
      color: 'white',
    }
  },
  bidPending:{
    fontFamily: 'ProximaNova',
    width: '70%',
    borderRadius: '10px',
    backgroundColor: '#FF974B',
    color: 'white',
    '&:hover': {
      backgroundColor: fade('#FF974B', 0.7),
      color: 'white',
    }
  },
  bidRed:{
    fontFamily: 'ProximaNova',
    width: '70%',
    borderRadius: '10px',
    backgroundColor: '#BE2E2E',
    color: 'white',
    '&:hover':{
      backgroundColor: '#BE2E2E',
      color: 'white',
    }
  },
  margin: {
    margin: theme.spacing(1),
  },
  tab:{
    backgroundColor: 'white',
    color: 'black',
  },
  boxPicture:{
    backgroundColor: '#FFFFFF',
    textAlign: 'center',
    [theme.breakpoints.up('lg')]: {
      height: '120px',
      width: '120px',
    },
    [theme.breakpoints.only('md')]: {
      height: '120px',
      width: '120px',
    },
    [theme.breakpoints.down('md')]: {
      height: '85px',
      width: '85px',
    },
    [theme.breakpoints.down('sm')]: {
      height: '95px',
      width: '95px',
    },
    [theme.breakpoints.down('xs')]: {
      height: '70px',
      width: '70px',
    },
    [theme.breakpoints.down('415')]: {
      height: '95px',
      width: '95px',
    },
    [theme.breakpoints.down('376')]: {
      height: '85px',
      width: '85px',
    },
    [theme.breakpoints.down('361')]: {
      height: '83px',
      width: '83px',
    },
    [theme.breakpoints.down('321')]: {
      height: '70px',
      width: '70px',
    },
  },
  namaBarang:{
    transform: 'capitalize',
    marginBottom: '2%',
    fontSize: '11px',
  },
  howLong:{
    marginBottom: '4%',
    fontSize: '8px',
  },
  lastBid:{
    fontSize: '10px',
  },
  winnerName:{
    color: theme.palette.primary.main,
    fontSize: '10px',
  },
  bidValue:{
    fontSize: '16px'
  },
  lastBidOver: {
    [theme.breakpoints.up('md')]: {
      fontSize: '8px',
      marginTop: '5px',
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '8px',
      marginTop: '0',
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: '8px',
      marginTop: '5px',
    },
  },
  boxPictureOver: {
    position: 'relative',
    backgroundColor: '#FFFFFF',
    textAlign: 'center',
    [theme.breakpoints.up('lg')]: {
      height: '120px',
      width: '120px',
    },
    [theme.breakpoints.only('md')]: {
      height: '120px',
      width: '120px',
    },
    [theme.breakpoints.down('md')]: {
      height: '85px',
      width: '85px',
    },
    [theme.breakpoints.down('sm')]: {
      height: '95px',
      width: '95px',
    },
    [theme.breakpoints.down('xs')]: {
      height: '70px',
      width: '70px',
    },
    [theme.breakpoints.down('415')]: {
      height: '95px',
      width: '95px',
    },
    [theme.breakpoints.down('376')]: {
      height: '85px',
      width: '85px',
    },
    [theme.breakpoints.down('361')]: {
      height: '83px',
      width: '83px',
    },
    [theme.breakpoints.down('321')]: {
      height: '70px',
      width: '70px',
    },
  },
  overlay: {
    position: 'absolute',
    width:'100%',
    [theme.breakpoints.up('md')]: {
      height: '70px',
      paddingTop: '20px',
    },
    [theme.breakpoints.down('md')]: {
      height: '50px',
      paddingTop: '15px',
    },
    [theme.breakpoints.down('sm')]: {
      height: '55px',
      paddingTop: '15px',
    },
    top: 8,
    left: 0,
    color: 'green',
    backgroundColor: fade(`#FFFFFF`, 0.7),
  },
  userName: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start'
    },
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  },
  countDown: {
    [theme.breakpoints.down('md')]: {
      fontSize: '12px',
    },
  },
  space: {
    [theme.breakpoints.up('415')]: {
      marginLeft: '15%',
      marginRight: '15%',
    },
    [theme.breakpoints.down('415')]: {
      marginLeft: '20%',
      marginRight: '10%',
    },
    [theme.breakpoints.down('321')]: {
      marginLeft: '25%',
      marginRight: '5%',
    },
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Profile = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [details, setDetails] = useState([]);
  const [items, setItems] = useState([]);
  const [open, setOpen] = React.useState({
    state: false,
    data: {}
  });
  const [values, setValues] = React.useState({
    refresh: false
  });
  const [show, setShow] = React.useState(false);


  const handleSuccess = () => {
    setOpen({ state: false, data: {} });
    setShow(true);
  };

  const handleShut = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setShow(false);
    setValues(prev => ({ ...prev, refresh: true }))
  };

  const handleOpen = (param) => {
    setOpen({ state: true, data: param });
  };

  const handleClose = () => {
    setOpen({ state: false, data: {} });
  };

  useEffect(() => {
    axios.get(`${pingAuction}profile/get-user`, headers())
    .then(res => {
      const detail = res.data.data[0]
      setDetails(detail)
    })
  }, [])

  useEffect(() => {
    axios.get(`${pingAuction}profile/list-barang`, headers())
      .then(res => {
        const item = res.data.data
        setItems(item)
      })
  }, [])


  const [, setCompleted] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  }

  React.useEffect(() => {
    function progress() {
      setCompleted(oldCompleted => {
        if (oldCompleted === 100) {
          return 0;
        }
        const diff = Math.random() * 10;
        return Math.min(oldCompleted + diff, 100);
      });
    }
    const timer = setInterval(progress, 0);
    return () => {
      clearInterval(timer);
    };
  }, []);

  const renderRefresh = () => {
    if (values.refresh === true) {
      return window.location.reload();
    }
  }


  return (
    <Fragment>
      {renderRefresh()}
     
      <div className={classes.space}>
        <Snackbar open={show} autoHideDuration={6000} onClose={handleShut}>
          <Alert onClose={handleShut} severity="success">
            Your profile have been updated - close me to refresh!
          </Alert>
        </Snackbar>
        <Grid container className={classes.root} spacing={2}>
          <Grid item xs={12} md={12}>
            <div className={classes.top}>
              <Grid container>
                <Grid item xs={12} md={3} className={classes.center}>
                  <img src={Display} className={classes.photo} alt='profile-pic'/>
                </Grid>
                <Grid item xs={12} md={2} className={classes.left}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.userName}>
                      <h2 data-testid="profile">{details.username}</h2>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.userName}>
                      <Typography variant="caption">Bergabung sejak</Typography>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.userName}>
                      <Typography>{moment(details.created_date).utc().format('DD-MM-YYYY')}</Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12} md={7} className={classes.center}>
                  <Paper elevation={0} className={classes.points}>
                    <Grid container>
                      <Grid item xs={12} md={12} className={classes.userName}>
                        <Rating name="read-only" value={3} readOnly style={{marginBottom:'2%'}}/>
                      </Grid>
                      <Grid item xs={12} md={12}>
                        <BorderLinearProgress
                          variant="determinate"
                          color="secondary"
                          value={50}
                        />
                      </Grid>
                      <Grid item xs={12} md={12} className={classes.center}>
                        <Grid container>
                          <Grid item xs={6} md={6} className={classes.left}>
                            <p style={{ fontSize: '13px', marginTop: '5px' }}><b>1.000 poin</b></p>
                          </Grid>
                          <Grid item xs={6} md={6} className={classes.right}>
                            <p style={{fontSize:'13px', marginTop: '5px'}}>250 poin lagi</p>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} md={12}>
            <AppBar position="static" className={classes.tab}>
              <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                <Tab label="Informasi Akun" {...a11yProps(0)} />
                <Tab label="Toko Saya" {...a11yProps(1)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              <Grid container>
                <Grid item xs={12} md={6}>
                  <Paper elevation={1} className={classes.profil}>
                    <Grid container>
                      <Grid item xs={7} md={7} className={classes.center} style={{ justifyContent: 'flex-start', paddingLeft: '1vw' }}>
                        <p><b>Profil</b></p>
                      </Grid>
                      <Grid item xs={5} md={5} className={classes.center} style={{ justifyContent: 'flex-end', paddingRight: '1vw'}}>
                        <Box component="span" m={1} border={1} borderRadius={25} color='black'>
                          <Button onClick={() => handleOpen({details})} style={{fontSize: '11px', padding: '3px 8px'}}>Ubah Profil</Button>
                        </Box>
                      </Grid>
                      <Divider style={{ color: 'black', width: '100%' }} />
                      <div style={{ padding: '1vw' }}>
                        <Grid item xs={12} md={12} style={{ paddingTop: '2%' }}>
                          <Typography variant="caption">Nomor handphone</Typography>
                          <Typography variant="body1">{details.handphone}</Typography>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ paddingTop: '2%' }}>
                          <Typography variant="caption">Email</Typography>
                          <Typography variant="body1">{details.email}</Typography>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ paddingTop: '2%' }}>
                          <Typography variant="caption">Password</Typography>
                        </Grid>
                        <Grid item xs={12} md={12}>
                          <InputBase
                            className={classes.margin}
                            disabled
                            type='password'
                            defaultValue="abcdefgh"
                            inputProps={{ 'aria-label': 'naked' }}
                            style={{ margin: '0' }}
                          />
                        </Grid>
                      </div>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <Grid container>
                <Grid item xs={12} md={12} style={{ padding: '1%' }}>
                  <Grid container>
                    <Grid item xs={12} md={5} style={{ padding: '1%' }}>
                      <Grid item xs={12} md={12}>
                        <BorderLinearProgress
                          variant="determinate"
                          color="secondary"
                          value={85}
                        />
                      </Grid>
                      <Grid item xs={12} md={12} className={classes.center}>
                        <Grid container>
                          <Grid item xs={6} md={6} className={classes.left}>
                            <p style={{ fontSize: '13px', marginTop: '5px' }}><b>8 barang terjual</b></p>
                          </Grid>
                          <Grid item xs={6} md={6} className={classes.right}>
                            <p style={{ fontSize: '13px', marginTop: '5px' }}>12 barang lelang</p>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                {
                  items.map((item) => {
                    if (item.status === "tersedia") {
                      return (
                        <Grid item xs={12} sm={6} md={4} style={{ padding: '1%' }}>
                           <Link to={`/detail-product?id_barang=${item.id_barang}`} style={{textDecoration:'none'}} >
                          <Paper elevation={0} className={classes.paper}>
                            <Grid container>
                              <Grid item xs={7} md={7} direction="column">
                                <Typography className={classes.namaBarang}><b>{item.judul}</b></Typography>
                                <Typography className={classes.howLong}>{weeksBetween(new Date(), new Date(moment(item.created_date).utc().format('YYYY, MM, DD')))} minggu yang lalu</Typography>
                                <Typography className={classes.lastBid}>Bid Terakhir:</Typography>
                                <Typography><b className={classes.bidValue}>Rp {item.bid_terakhir===null?'0':item.bid_terakhir}</b></Typography>
                              </Grid>
                              <Grid item xs={5} md={5} direction="column" className={classes.center}>
                                <Box borderRadius={16} className={classes.boxPicture}>
                               
                                  <img src={item.img[0].url} className={classes.imageCategory} alt={item.img[0].url}></img>
                                 
                                  <Typography className={classes.countDown}><b>1:23:45:00</b></Typography>
                                </Box>
                              </Grid>
                            </Grid>
                          </Paper>
                          </Link>
                        </Grid>
                      )
                    } else {
                      return (
                        <Grid item xs={12} sm={6} md={4} style={{ padding: '1%' }}>
                          <Link to={`/detail-product?id_barang=${item.id_barang}`} style={{textDecoration:'none'}} >
                          <Paper elevation={0} className={classes.paper}>
                            <Grid container>
                              <Grid item xs={7} md={7}>
                                <Typography className={classes.namaBarang}><b>{item.judul}</b></Typography>
                                <Typography className={classes.howLong}>Pemenang <b className={classes.winnerName}> {item.id_pembeli}</b></Typography>
                                <Typography className={classes.lastBid}>Bid Terakhir:</Typography>
                                <Typography><b className={classes.bidValue}>Rp {item.bid_terakhir===null?'0':item.bid_terakhir}</b></Typography>
                              </Grid>
                              <Grid item xs={5} md={5} className={classes.center}>
                                <Box borderRadius={16} className={classes.boxPictureOver}>
                                  
                                  <img src={item.img[0].url} className={classes.imageCategory} alt={item.img[0].url}></img>
                                 
                                  <Typography className={classes.lastBidOver}>Masa lelang {weeksBetween(new Date(moment(item.masa_lelang).utc().format('YYYY, MM, DD')), new Date(moment(item.created_date).utc().format('YYYY, MM, DD')))} minggu</Typography>
                                  <Typography className={classes.overlay}><b>TERJUAL</b></Typography>
                                </Box>
                              </Grid>
                            </Grid>
                          </Paper>
                          </Link>
                        </Grid>
                      )
                    }
                  })
                }
                {/* <Grid item xs={12} sm={6} md={4} style={{ padding: '1%' }}>
                  <Paper elevation={0} className={classes.paper}>
                    <Grid container>
                      <Grid item xs={7} md={7} direction="column">
                        <Typography className={classes.namaBarang}><b>Nikon D5600 Kit AF-P 18-55mm VR Pak...</b></Typography>
                        <Typography className={classes.howLong}>11 minggu yang lalu</Typography>
                        <Typography className={classes.lastBid}>Bid Terakhir:</Typography>
                        <Typography><b className={classes.bidValue}   >Rp 2.900.000</b></Typography>
                      </Grid>
                      <Grid item xs={5} md={5} direction="column" className={classes.center}>
                        <Box borderRadius={16} className={classes.boxPicture}>
                          <img src={Camera} className={classes.imageCategory}></img>
                          <Typography className={classes.countDown}><b>1:23:45:00</b></Typography>
                        </Box>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item xs={12} sm={6} md={4} style={{ padding: '1%' }}>
                  <Paper elevation={0} className={classes.paper}>
                    <Grid container>
                      <Grid item xs={7} md={7}>
                        <Typography className={classes.namaBarang}><b>{cont.length <= 35 ? cont : cont.substring(0, 35)+"..."}</b></Typography>
                        <Typography className={classes.howLong}>Pemenang <b className={classes.winnerName}> Bagus Tenan</b></Typography>
                        <Typography className={classes.lastBid}>Bid Terakhir:</Typography>
                        <Typography><b className={classes.bidValue}>Rp 2.900.000</b></Typography>
                      </Grid>
                      <Grid item xs={5} md={5} className={classes.center}>
                        <Box borderRadius={16} className={classes.boxPictureOver}>
                          <img src={Camera} className={classes.imageCategory}></img>
                          <Typography className={classes.lastBidOver}>Masa lelang 12 minggu</Typography>
                          <Typography className={classes.overlay}><b>TERJUAL</b></Typography>
                        </Box>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid> */}
                <Grid item xs={12} md={12} style={{ padding: '10px' }} className={classes.center}>
                  <MemoryRouter initialEntries={['/inbox']} initialIndex={0}>
                    <Route>
                      {({ location }) => {
                        const query = new URLSearchParams(location.search);
                        const page = parseInt(query.get('page'), 10) || 1;

                        return (
                          <Pagination
                            page={page}
                            count={10}
                            renderItem={item => (
                              <PaginationItem
                                component={Link}
                                to={`/inbox${item.page === 1 ? '' : `?page=${item.page}`}`}
                                {...item}
                              />
                            )}
                          />
                        );
                      }}
                    </Route>
                  </MemoryRouter>
                </Grid>
              </Grid>
            </TabPanel>
          </Grid>
        </Grid>
      </div>
      <EditProfile open={open} handleClose={() => handleClose()} handleSuccess={() => handleSuccess()} />
    </Fragment>
  )
}
export default Profile