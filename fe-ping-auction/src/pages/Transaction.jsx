import React, {Fragment, useEffect, useState} from 'react'
import axios from 'axios'
import { makeStyles }  from '@material-ui/core/styles'
import {Grid, Paper, Button, ButtonGroup, Grow, Popper, ClickAwayListener, MenuList, MenuItem, Fab, Box} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Camera from '../images/camera.svg';
import {headers, pingAuction} from '../config'
import moment from 'moment'
import {Link} from 'react-router-dom'
import CountDown from '../components/CountDown'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 'auto',
        width: '85%',
        borderRadius: '5px',
        marginTop: '15px',
        '&:hover': {
          background: "#d8e6fc",
          transition: '1.5s'
        },
  
    },
    paper2: {
        height: 'auto',
        width: '85%',
        borderRadius: '5px',
        marginTop: '15px',
        opacity: 0.5
        
  
    },
    paper3: {
        height: 'auto',
        width: '85%',
        borderRadius: '5px',
        marginTop: '15px',
        backgroundColor: '#F1F1F5'
        
  
    },
    imageCategory: {
        padding: theme.spacing(2),
        alignItems: 'center',
  
    },
    timerBid: {
        fontFamily: 'roboto',
        fontSize: '30px',
        color: '#284089',
        // marginLeft: '15px',
        lineHeight: 0
    
    },
    bidButton: {
        width: '30px',
        marginTop: '15px',
        // marginLeft: '15px',

    },
    sortButton: {
        marginTop: '10px'
    },
    itemsImage: {
        width: '80%',
        height: 'auto',
        padding: '20px',
        
    },
    detailText:{
        fontFamily: 'roboto',
        color: '#92929D',
        lineHeight: 1,
        fontSize:'0.875rem',
        [theme.breakpoints.down('sm')]:{
          fontSize:'0.7rem'
        }
    },
    soldText: {
        color: '#03AD00',
        fontWeight: 'bold',
        position: 'absolute',
        fontSize: '45px',
        zIndex: -1
    },
    failedText: {
        color: '#FF0000',
        fontWeight: 'bold',
        position: 'absolute',
        fontSize: '45px',
        zIndex: -1
    },
    myBid: {
        color: '#03AD00',
        fontWeight: 'bold'
    },
    myBid2: {
        color: '#FF0000',
        fontWeight: 'bold'
        // fontWeight: 'bold'
    },
    waktu: {
        display: 'inline'
    }
    
}))

const Transaction = () => {

    const classes = useStyles()
    const options = ['Default', 'Date and time', 'The chepeaset', 'Most expensive'];
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleMenuItemClick = (event, index) => {
        setSelectedIndex(index);
        setOpen(false);
    };
    const handleToggle = () => {
        setOpen(prevOpen => !prevOpen);
    };
    const handleClose = event => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
          return;
        }
    
        setOpen(false);
    };

    const [items, setItems] = useState([])

    useEffect(() => {
        axios.get(`${pingAuction}transaction/detail-transaction`, headers())
        .then(res => {
            const items = res.data.data
            console.log(items)
            setItems(items)
        })
    },[])

    const formatter = new Intl.NumberFormat('de-De', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
    })

    // console.log(moment('2020-04-20').toString())
    
    var currentTime = new Date().getTime();
    // console.log(Date.parse('2020-04-21T17:00:00.000Z') - Date.parse(currentTime))
    // console.log(Date.parse('2020-04-21T17:00:00.000Z'))
    // console.log(Date.parse(currentTime))

    
    
    // var myfunc = (param) => {

    
    //     setInterval(function() {
    //     var countDownDate = new Date(param).getTime()
    //     var now = new Date().getTime();
    //     var timeleft = countDownDate - now;
    //     // console.log(timeleft)
            
    //     // Calculating the days, hours, minutes and seconds left
    //     var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
    //     var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //     var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
    //     var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
            
    //     if (timeleft < 0) {
    //         clearInterval(myfunc);
    //         return document.getElementById("days").innerHTML = "TIME UP!!";
    //     }
    //     return document.getElementById("days").innerHTML = hours + ":" + minutes + ":" + seconds
    //     }, 1000);

    // }
    

    return(
        <Fragment>

            <div style={{marginLeft: '10%'}}>
                <Grid container>
                    <Grid item xs={12} sm={12} md={8}>
                        <h2>Transaksi</h2>
                        {/* <p className={classes.timerBid}><b>{myfunc()}</b></p> */}
                        {/* <h3 id="days"></h3> */}
                        
                        {/* <h1>{console.log(moment('2020-04-20'))}</h1> */}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <ButtonGroup className={classes.sortButton} variant="contained" color="primary" ref={anchorRef} aria-label="split button">
                            <Button data-testid = "sortby">Sort By: <b>{options[selectedIndex]}</b></Button>
                                <Button
                                    color="primary"
                                    size="small"
                                    // aria-controls={open ? 'split-button-menu' : undefined}
                                    // aria-expanded={open ? 'true' : undefined}
                                    aria-label="hello"
                                    // aria-haspopup="menu"
                                    onClick={handleToggle}
                                >
                                    <ArrowDropDownIcon />
                                </Button>
                        </ButtonGroup>
                        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                            {({ TransitionProps, placement }) => (
                                <Grow
                                    {...TransitionProps}
                                    style={{
                                        transformOrigin: placement === 'center top' ? 'bottom' : 'center bottom',
                                    }}
                                >
                                    <Paper>
                                        <ClickAwayListener onClickAway={handleClose}>
                                            <MenuList id="split-button-menu">
                                                {options.map((option, index) => (
                                                    <MenuItem
                                                        // key={option}
                                                        selected={index === selectedIndex}
                                                        onClick={event => handleMenuItemClick(event, index)}
                                                    >
                                                        {option}
                                                    </MenuItem>
                                                ))}
                                            </MenuList>
                                        </ClickAwayListener>
                                    </Paper>
                                </Grow>
                            )}
                        </Popper>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12}>
                    {items.map(items =>{ 
                        
                        return (
                        <Paper elevation={0} 
                            className=
                            {Date.parse(items.masa_lelang) - currentTime < 0 ? 
                            classes.paper2 
                            : 
                            classes.paper}>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}>
                                    <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                        <img src={items.url[0].url} className={classes.itemsImage} alt="foto kucing"></img>
                                    </Box>   
                                </Grid>
                                <Grid item xs={12} sm={7} md={5}> 
                                    <Box style={{marginLeft: '10px'}}>
                                            <h3>{items.judul}</h3>
                                        <Grid style={{display:'flex', alignItems:'baseline', justify:'center'}}>
                                            <Grid item xs={12} sm={12} md={4} direction="column">
                                                <p className={classes.detailText}>Bid saya:</p>
                                                <p className={classes.detailText} style={{marginTop: '20px'}}>Bid Terakhir: </p>
                                            </Grid> 
                                            <Grid item xs={12} sm={12} md={5} direction="column">
                            <p className={items.bid<items.bid_terakhir ? classes.failedText : classes.soldText} style={Date.parse(items.masa_lelang) - currentTime < 0 ?{display:'block'}:{display:'none'}}>{items.bid<items.bid_terakhir ?'Kalah':'Menang'  }</p>
                                                {items.bid < items.bid_terakhir ? 
                                                    <p className={classes.myBid2}>Rp {formatter.format(items.bid).slice(0, -5)}</p>
                                                    :
                                                    <p className={classes.myBid}>Rp {formatter.format(items.bid).slice(0, -5)}</p>
                                                }
                                                
                                                <p><b>Rp {formatter.format(items.bid_terakhir).slice(0, -5)}</b></p>
                                            </Grid> 
                                        </Grid>                                      
                                    </Box>
                                </Grid>
                                    
                                <Grid item xs={5} md={3} sm={4} direction="column">
                                    <Box style={{marginLeft: '20px'}}>
                                        <p style={{color: '#92929D'}}  className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                        {/* <p id="days"></p> */}
                                        <CountDown id={items.id_barang} masa_lelang={items.masa_lelang} />
                                        {/* <h3 className={classes.timerBid} id='days'><b>{myfunc(items.masa_lelang)}</b></h3> */}
                                        {/* {items.bid >= items.bid_terakhir && Date.parse(items.masa_lelang) - currentTime < 0 ? //belum ngecek kalo kalah
                                           <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%', backgroundColor: '#03AD00'}}
                                           classes={
                                               {label:classes.headerText}
                                               }
                                           >Menang
                                           </Button>
                                           : 
                                           <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%'}}
                                        classes={
                                            {label:classes.headerText}
                                            }
                                        >Bid
                                        </Button>
                                        } */}
                                        {
                                            Date.parse(items.masa_lelang) - currentTime < 0 ? null :
                                            <Link to={`/detail-product?id_barang=${items.id_barang}`} style={{textDecoration:'none'}}>
                                            <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%'}}
                                            classes={
                                                {label:classes.headerText}
                                                }
                                            >Lihat Item
                                            </Button>
                                        </Link>
                                        }
                                     
                                       
                                        
                                    </Box>
                                    
                                </Grid>
                            </Grid>
                        </Paper>
                    )})}

                        {/* <Paper elevation={0} className={classes.paper}>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}>
                                    <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                        <img src={Camera} className={classes.itemsImage} ></img>
                                    </Box>   
                                </Grid>
                                <Grid item xs={12} sm={7} md={5}> 
                                    <Box style={{marginLeft: '10px'}}>
                                            <h3>Nikon D5600 Kit AF-P 18-55mm VR Paket Bonus </h3>
                                        <Grid style={{display:'flex', alignItems:'baseline', justify:'center'}}>
                                            <Grid item xs={12} sm={12} md={4} direction="column">
                                                <p className={classes.detailText}>Bid saya:</p>
                                                <p className={classes.detailText} style={{marginTop: '20px'}}>Bid Terakhir: </p>
                                            </Grid> 
                                            <Grid item xs={12} sm={12} md={3} direction="column">
                                                <p className={classes.myBid2}>Rp 1.000.000 </p>
                                                <p><b>Rp 2.000.000</b></p>
                                            </Grid> 
                                        </Grid>                                      
                                    </Box>
                                </Grid>
                                    
                                <Grid item xs={5} md={3} sm={4} direction="column">
                                    <Box style={{marginLeft: '20px'}}>
                                        <p style={{color: '#92929D'}}  className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                        <p className={classes.timerBid} id="days"><b>00:15:25</b></p>
                                        <h1 id="days"></h1>
                                        <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%'}}
                                        classes={
                                            {label:classes.headerText}
                                            }
                                        >Bid
                                        </Button>
                                    </Box>   
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper elevation={0} className={classes.paper3}>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}>
                                    <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                        <img src={Camera} className={classes.itemsImage} ></img>
                                    </Box>   
                                </Grid>
                                <Grid item xs={12} sm={7} md={5}> 
                                    <Box style={{marginLeft: '10px'}}>
                                            <h3>Nikon D5600 Kit AF-P 18-55mm VR Paket Bonus </h3>
                                        <Grid style={{display:'flex', alignItems:'baseline', justify:'center'}}>
                                            <Grid item xs={12} sm={12} md={4} direction="column">
                                                <p className={classes.detailText}>Bid saya:</p>
                                                <p className={classes.detailText} style={{marginTop: '20px'}}>Bid Terakhir: </p>
                                            </Grid> 
                                            <Grid item xs={12} sm={12} md={3} direction="column">
                                                <p className={classes.myBid2}>Rp 1.000.000 </p>
                                                <p><b>Rp 2.000.000</b></p>
                                            </Grid> 
                                        </Grid>                                      
                                    </Box>
                                </Grid>
                                    
                                <Grid item xs={5} md={3} sm={4} direction="column">
                                    <Box style={{marginLeft: '20px'}}>
                                        <p style={{color: '#92929D'}}  className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                        <p className={classes.timerBid}><b>00:00:00</b></p>
                                        <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%', backgroundColor: '#BE2E2E'}}
                                        classes={
                                            {label:classes.headerText}
                                            }
                                        >Kalah
                                        </Button>
                                    </Box>   
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper elevation={0} className={classes.paper3}>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}>
                                    <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                        <img src={Camera} className={classes.itemsImage} ></img>
                                    </Box>   
                                </Grid>
                                <Grid item xs={12} sm={7} md={5}> 
                                    <Box style={{marginLeft: '10px'}}>
                                            <h3>Nikon D5600 Kit AF-P 18-55mm VR Paket Bonus </h3>
                                        <Grid style={{display:'flex', alignItems:'baseline', justify:'center'}}>
                                            <Grid item xs={12} sm={12} md={4} direction="column">
                                                <p className={classes.detailText}>Bid saya:</p>
                                                <p className={classes.detailText} style={{marginTop: '20px'}}>Bid Terakhir: </p>
                                            </Grid> 
                                            <Grid item xs={12} sm={12} md={3} direction="column">
                                                <p className={classes.myBid}>Rp 1.000.000 </p>
                                                <p><b>Rp 1.000.000</b></p>
                                            </Grid> 
                                        </Grid>                                      
                                    </Box>
                                </Grid>
                                    
                                <Grid item xs={5} md={3} sm={4} direction="column">
                                    <Box style={{marginLeft: '20px'}}>
                                        <p style={{color: '#92929D'}}  className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                        <p className={classes.timerBid}><b>00:00:00</b></p>
                                        <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%', backgroundColor: '#03AD00'}}
                                        classes={
                                            {label:classes.headerText}
                                            }
                                        >Menang
                                        </Button>
                                    </Box>   
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper elevation={0} className={classes.paper2}>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={4}>
                                    <Box style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                                        <img src={Camera} className={classes.itemsImage} ></img>
                                    </Box>   
                                </Grid>
                                <Grid item xs={12} sm={7} md={5}> 
                                    <Box style={{marginLeft: '10px'}}>
                                            <h3>Nikon D5600 Kit AF-P 18-55mm VR Paket Bonus </h3>
                                            
                                        <Grid style={{display:'flex', alignItems:'baseline', justify:'center'}}>
                                            <Grid item xs={12} sm={12} md={4} direction="column">
                                                <p className={classes.detailText}>Bid saya:</p>
                                                
                                                <p className={classes.detailText} style={{marginTop: '20px'}}>Bid Terakhir: </p>
                                            </Grid> 
                                            <Grid item xs={12} sm={12} md={3} direction="column">
                                            <p className={classes.soldText}>Menang</p>
                                                <p className={classes.myBid2}>Rp 1.000.000 </p>
                                                
                                                <p><b>Rp 1.000.000</b></p>
                                            </Grid> 
                                        </Grid>                                      
                                    </Box>
                                </Grid>
                                    
                                <Grid item xs={5} md={3} sm={4} direction="column">
                                    <Box style={{marginLeft: '20px'}}>
                                        <p style={{color: '#92929D'}}  className={classes.detailText}><b>SISA WAKTU LELANG:</b></p>
                                        <p className={classes.timerBid}><b>00:15:25</b></p>
                                        <Button variant="contained" color="primary" className={classes.bidButton}  style={{width:'50%'}} disabled
                                        classes={
                                            {label:classes.headerText}
                                            }
                                        >Bid
                                        </Button>
                                    </Box>   
                                </Grid>
                            </Grid>
                        </Paper> */}
                    </Grid>


                </Grid>
                
            </div>
            
        </Fragment>
    )

}
export default Transaction