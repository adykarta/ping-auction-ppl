import React, { Fragment } from 'react';
import { makeStyles, fade } from '@material-ui/core/styles';
import AppBarFixed from '../components/AppBar'
import DefaultLayout from '../components/DefaultLayout'
import Camera from '../images/camera.svg';
import { Grid, Paper, Button, InputBase, ButtonGroup, Popper, ClickAwayListener, MenuList, MenuItem, Grow } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import FilterListIcon from '@material-ui/icons/FilterList';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: '35vh',
    width: '100%',
  },
  find: {
    height: 80,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageCategory: {
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'center'
  },
  detailText: {
    fontFamily: 'roboto',
    fontSize: '14px',
    color: '#92929D',
    lineHeight: 1
  },
  timerBid: {
    fontFamily: 'roboto',
    fontSize: '30px',
    color: '#284089',
    marginLeft: '15px',
    lineHeight: 0
  },
  bidButton: {
    width: '70%',
    borderRadius: '25px',
  },
  search: {
    height: '5.5vh',
    position: 'relative',
    display: "flex",
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#FFFFF',
    '&:hover': {
      backgroundColor: fade('#92929D', 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    paddingRight: '1%',
    width: '100%',
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    color: '#92929D'
  },
  inputRoot: {
    color: 'black',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 250,
    },
  },
  sell:{
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '50%',
    borderRadius: theme.shape.borderRadius,
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  location: {
    height: '5.5vh',
    position: 'relative',
    display: "flex",
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#FAFAFB',
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  locationIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#92929D'
  },
  inputLocation: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 250,
    },
  },
  searchNow: {
    backgroundColor: '#FAFAFB',
    color: '#696974',
    width: '60%',
    height: '5.5vh',
    borderRadius: theme.shape.borderRadius,
  },
  category:{
    height: '5.5vh',
    width: '90%',
    borderRadius: theme.shape.borderRadius,
  },
  filter:{
    backgroundColor: 'white',
    marginRight: '3%',
    borderRadius: theme.shape.borderRadius,
  },
}));

const Marketplace = () => {
  const classes = useStyles();
  const options = ['All Category', 'Property', 'Men Fashion', 'Women Fashion', 'Electronic', 'Antique'];
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index);
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(prevOpen => !prevOpen);
  };
  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };


  return (
    <Fragment>
      <div data-testid="div">
        <AppBarFixed />
        <DefaultLayout />
      </div>
      <div style={{ marginLeft: '15%', marginRight: '15%' }}>
        <Grid container className={classes.root} spacing={2}>
          <Grid item xs={12} md={8}>
            <h2 data-testid="marketplace">Marketplace</h2>
          </Grid>
          <Grid item xs={12} md={4} className={classes.center} style={{justifyContent: 'flex-end'}}>
            <Button className={classes.filter}>
              <FilterListIcon />
            </Button>
            <Button className={classes.sell}>
              <AddIcon style={{marginRight: '3%'}}/>
              Sell Something
            </Button>
          </Grid>
          <Grid item xs={12} md={12}>
            <Paper elevation={1} className={classes.find}>
              <Grid container>
                <Grid item xs={12} md={4}>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <InputBase
                      placeholder="I wanna buy…"
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      inputProps={{ 'aria-label': 'search' }}
                    />
                  </div>
                </Grid>
                <Grid item xs={12} md={4}>
                  <div className={classes.location}>
                    <div className={classes.locationIcon}>
                      <LocationOnIcon />
                    </div>
                    <InputBase
                      placeholder="Yogyakarta,ID"
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputLocation,
                      }}
                      inputProps={{ 'aria-label': 'location' }}
                    />
                  </div>
                </Grid>
                <Grid item xs={12} md={2} className={classes.center}>
                  <ButtonGroup variant="contained" color="#FFFFFF" ref={anchorRef} aria-label="split button" className={classes.category}>
                    <Button data-testid="sortby" style={{width: '80%'}}><b>{options[selectedIndex]}</b></Button>
                    <Button
                      color="#FFFFFF"
                      size="small"
                      onClick={handleToggle}
                      style={{ width: '20%' }}
                    >
                      <ArrowDropDownIcon />
                    </Button>
                  </ButtonGroup>
                  <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                    {({ TransitionProps, placement }) => (
                      <Grow
                        {...TransitionProps}
                        style={{
                          transformOrigin: placement === 'center top' ? 'bottom' : 'center bottom',
                        }}
                      >
                        <Paper>
                          <ClickAwayListener onClickAway={handleClose}>
                            <MenuList id="split-button-menu">
                              {options.map((option, index) => (
                                <MenuItem
                                  // key={option}
                                  selected={index === selectedIndex}
                                  onClick={event => handleMenuItemClick(event, index)}
                                >
                                  {option}
                                </MenuItem>
                              ))}
                            </MenuList>
                          </ClickAwayListener>
                        </Paper>
                      </Grow>
                    )}
                  </Popper>
                </Grid>
                <Grid item xs={12} md={2} className={classes.center}>
                  <Button className={classes.searchNow}>
                    Search Now
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera"></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera"></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Paper elevation={0} className={classes.paper}>
              <Grid container>
                <Grid item xs={12} md={12} className={classes.center}>
                  <img src={Camera} className={classes.imageCategory} alt="Camera" ></img>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <h3>Nikon D5600...</h3>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>SISA WAKTU LELANG:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b>01:15:25</b>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} md={6} className={classes.center}>
                  <Grid container>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <p style={{ color: '#92929D' }}><b>BID TERAKHIR:</b></p>
                    </Grid>
                    <Grid item xs={12} md={12} className={classes.center}>
                      <b style={{ color: '#284089' }}>2.345.678</b>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </Fragment>
  )
}
export default Marketplace