import React, { Fragment, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Grid, Paper, Button, MenuItem, Box, Typography} from '@material-ui/core';
import axios from 'axios';

import {headers, pingAuction} from '../config'
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import {Link} from 'react-router-dom';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider, KeyboardDateTimePicker,} from '@material-ui/pickers';
import 'date-fns';
import CountDown from '../components/CountDown';


const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 0,
    },
    paper: {
      height: 'auto',
      width: '90%',
      marginLeft:'5%',
      marginBottom:'25px',
      display:'flex',
      '&:hover': {
        background: "#d8e6fc",
        transition: '1s'
        },
    },
    imageCategory: {
      alignItems: 'center',
      flex: 1,
      height: '150px',
      width: '150px',
      resizeMode: 'contain'
    },
    detailText: {
        fontFamily: 'roboto',
        fontSize: '14px',
        color: '#92929D',
        lineHeight: 1
    },
    timerBid: {
        fontFamily: 'roboto',
        fontSize: '22px',
        color: '#284089',
        lineHeight: 0,
        marginRight:'20%'
    
    },
    bidButton: {
        width: '30px',
        height: '40px',
        marginTop: '10px',
        zIndex:'1000'
        // marginLeft: '30%',

    },
    imageResize: {
        width: '80%',
        height:'auto',
        padding: '20px'
    },
    formControl: {
        margin: theme.spacing(2),
        width: '90%',
        justifyContent:'center'
    },
    formControlCheckBox: {
        minWidth: 200,
        margin: theme.spacing(2),
    },

}));


const HasilSearch =()=>{
    const classes = useStyles();
 
  

    // const [value, setValue] = React.useState(null);
    
 

   
    const [items, setItems] = useState([])

    useEffect(() => {
        axios.get(`${pingAuction}all-item`, headers())
        .then(res => {
            const items = res.data.data
            setItems(items)
            setFilter(items)
        })
    },[])
    
    // const [search, setSearch] = useState('')
    const [filterResults, setFilterResults] = useState({
        id_kategori: null,
        isnew: null,
        lokasi: null,
        bid_terakhir: null,
        masa_lelang:null,
        search:null
    })

    const handleChange4 = (event, name) => {
    
    if(name==='baru'){
        setFilterResults({...filterResults,isnew:true});  
    }
    else if(name==='bekas'){
        setFilterResults({...filterResults,isnew:false});
    }
    else{
        setFilterResults({...filterResults,[name]:event.target.value});
    }
   
    };
   
    const [category, setCategory] = useState([])

    const [filter, setFilter] = useState([])
    const [searchFilter, setSearchFilter] = useState([])

    useEffect(() => {
        axios.get(`${pingAuction}get-kategori`)
        .then(res => {
            const category = res.data.data
            setCategory(category)
        })
    },[])
   
    const[provinsi, setProvinsi] = useState([])

    useEffect(() => {
        axios.get(`${pingAuction}get-prov`)
        .then(res => {
            const provinsi = res.data.data
            setProvinsi(provinsi)
        })
    },[])
   



    const handleFilter = ()=>{
        let filterData = filterResults

        // let res = items.filter(function(elm){
        //     if(filterResults.id_kategori===null){
        //         return items
        //     }
        //     else{
        //         return elm.id_kategori === filterResults.id_kategori
        //     }
        // })
        // console.log(res)

        Object.keys(filterData).forEach(function(key) {
            if(filterData[key]=== null){
                delete filterData[key]
            }
           
        });

        console.log(filterData)

        let res= items.filter(function(item) {
            // console.log(item)
            let bool = true
            for (var key in filterData) {
                if(key==='bid_terakhir'){
                    if(filterData[key]===1){
                        if(item[key] < 1000000){
                            bool = true
                        }
                        else{
                            bool = false
                            return bool
                        }
                    }
                    else if(filterData[key]===2){
                        // console.log(item[key])
                       
                        if(item[key] >=1000000 && item[key] <5000000){
                            console.log("in")
                            bool = true
                        }
                        else{
                            bool = false
                            return bool
                        }  
                        console.log("diluar") 
                      
                    }
                    else if(filterData[key]===3){
                        if(item[key] >=5000000){
                            bool = true
                        }
                        else{
                            bool = false
                            return bool
                        }
                    }
                }
                if(key==='masa_lelang'){
                    if(new Date(item[key]) < new Date(filterData[key])){
                        bool = true
                    }
                    else{
                        bool = false
                        return bool
                    }
                }
                if(key==='id_kategori'){
                    if(item[key]===filterData[key]){
                        bool = true
                     }
                     else{
                        bool = false;
                        return bool
                     }

                }
                if(key==='isnew'){
                    if(item[key]===filterData[key]){
                        bool = true
                     }
                     else{
                        bool = false;
                        return bool
                     }
                }
                if(key==='lokasi'){
                    if(item[key]===filterData[key]){
                        bool = true
                     }
                     else{
                        bool = false;
                        return bool
                     }
                }

             
            }
     
            return bool
  
          });
        
          setFilter(res)
          setSearchFilter(res)
          
  
    }

  
    

    const [selectedDate, setSelectedDate] = React.useState();

    const handleDateChange = (date) => {
        setSelectedDate(date);
        setFilterResults({...filterResults,masa_lelang:date});
    };
   const handleReset = ()=>{
       setFilterResults({ id_kategori: null,
        isnew: null,
        lokasi: null,
        bid_terakhir: null,
        masa_lelang:null,
        search:''
        
        })
        setFilter(items)
        setSearchFilter([])
   }

    const handleSearch = (event)=>{
        const value = event.target.value
        
        
        setFilterResults({...filterResults,search:value})
        let filtered
        if(searchFilter.length===0){
            filtered= items.filter(function(item){
                return item.judul.toLowerCase().includes(value.toLowerCase())
            })

        }
        else{
            filtered= searchFilter.filter(function(item){
                return item.judul.toLowerCase().includes(value.toLowerCase())
            })
        }

        
        setFilter(filtered)
    }
    
  


    return(
        
        <Fragment>
        
            <Grid container className={classes.root} spacing={0}>
                <Grid item xs={12} md={3} sm={12} direction="column">
                    <Paper style={{backgroundColor:'#FFFFFF'}} elevation={3}>
                        <Box>
                            <Typography variant='h6' align='center'>Search Filter</Typography>
                            <TextField
                                className={classes.formControl} 
                                id="input-with-icon-textfield"
                                onChange={(e)=>handleSearch(e)}
                                value={filterResults.search}
                                label="Cari Barang"
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <SearchIcon />
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="age-native-label-placeholder">Category</InputLabel>
                                <Select id="demo-simple-select" value={filterResults.id_kategori} onChange={(event) => handleChange4(event,'id_kategori')}>
                                <MenuItem value={null}>
                                        <em>None</em>
                                    </MenuItem>
                                {category.map(category => (
                                    <MenuItem value={category.id_kategori} name="id_kategori">{category.nama_kategori}</MenuItem>
                                ))}
                                </Select>
                            </FormControl>
                            <FormControl component="fieldset" className={classes.formControlCheckBox}>
                                <FormLabel component="legend">Kondisi Barang</FormLabel>
                                <FormGroup>
                                    <FormControlLabel control={<Checkbox  onChange={(event) => handleChange4(event,'baru')} checked={filterResults.isnew===true ? true :false} color="primary"/>}label="Baru"/>
                                    <FormControlLabel control={<Checkbox onChange={(event) => handleChange4(event,'bekas')} checked={filterResults.isnew===false ? true :false}  color="primary" />}label="Bekas"/>
                                </FormGroup>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="age-native-label-placeholder">Lokasi</InputLabel>
                                <Select id="demo-simple-select" value={filterResults.lokasi} onChange={(event) => handleChange4(event,'lokasi')}>
                                    <MenuItem value={null}>
                                        <em>None</em>
                                    </MenuItem>
                                {provinsi.map(provinsi => (
                                    <MenuItem value={provinsi.nama_prov} name="lokasi">{provinsi.nama_prov}</MenuItem>
                                ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="age-native-label-placeholder">Harga</InputLabel>
                                <Select id="demo-simple-select" value={filterResults.bid_terakhir} onChange={(event) => handleChange4(event,'bid_terakhir')}>
                                    <MenuItem value={null}>
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value={1} name="bid_terakhir">0 - 999.999</MenuItem>
                                    <MenuItem value={2} name="bid_terakhir">1.000.000 - 4.999.999</MenuItem>
                                    <MenuItem value={3} name="bid_terakhir">> 5.000.000</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDateTimePicker
                                    margin="normal"
                                    // id="date-time-picker-dialog"
                                    label="Waktu Berakhir"
                                    format="yyyy/MM/dd hh:mm a"
                                    disablePast = {true}
                                    value={selectedDate}
                                    onChange={handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                    name="masa_lelang"
                                />
                                </MuiPickersUtilsProvider>
                            </FormControl>
                            <Button variant="contained" color="primary" className={classes.formControl} onClick={handleFilter}>Filter</Button>
                            <Button variant="contained" color="secondary" className={classes.formControl} onClick={handleReset} style={{color:'#d50000'}}>Reset</Button>
                        </Box>
                    </Paper>
                </Grid>
              
                <Grid item xs={12} md={9} sm={12} direction="column">
                {/* <h1 style={{textAlign:'center'}}>Lelang Hari Ini</h1> */}

                    {
                        filter.length === 0 ? <p>Items not found</p>
                        :
                        <Fragment>
                            {filter.map(items =>{
                                
                            
                            return(
                                    <Link style={{textDecoration: 'none'}} to={`/detail-product?id_barang=${items.id_barang}`}>
                                    <Paper elevation={1} className={classes.paper}>
                                        <Grid container>
                                            <Grid item xs={12} md={4} sm={12}>
                                                <Box style={{width:'100%', height:'100%', display:'flex', justifyContent:'center'}}>
                                                <img src={items.img[0].url} className={classes.imageResize} alt={items.img[0].url} ></img>
                                                </Box>
                                            </Grid>
                                            <Grid item xs={6} md={5} sm={7}>
                                                <Box style={{marginLeft:'10%'}}>
                                                    <h3>{items.judul}</h3> 
                                                    <p className={classes.detailText}>{items.lama_berlangsung} hari yang lalu</p>
                                                    <p className={classes.detailText}>Peserta Lelang: {items.jumlah_peserta} Bidder</p>
                                                    <p className={classes.detailText}>Harga Awal: Rp {items.harga_awal.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</p>
                                                    <p className={classes.detailText}>Bid Terakhir: <b style={{color:'#284089'}}> Rp {items.bid_terakhir === null ? 0:items.bid_terakhir.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</b></p>
                                                </Box>                       
                                            </Grid>                    
                                            <Grid item xs={6} md={3} sm={5}>
                                                <div style={{marginLeft:'10%'}}>
                                                    <p style={{color: '#92929D'}}><b>SISA WAKTU LELANG:</b></p>
                                                   
                                                    <CountDown id={items.id_barang} masa_lelang ={items.masa_lelang}/>
                                                    <Link to={`/explore/bidding?id_barang=${items.id_barang}`} style={{textDecoration:'none'}}>
                                                    <Button variant="contained" color="primary" className={classes.bidButton}>Bid</Button>
                                                    </Link>
                                                </div>
                                            </Grid>                     
                                        </Grid>
                                    </Paper>
                                    </Link>
                            )})}
                        </Fragment>
                    }
                   
                    
                </Grid> 
              
            </Grid>

            
        </Fragment>
    )
} 

export default HasilSearch