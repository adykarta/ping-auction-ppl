import React from 'react';
import './App.css';
import { createMuiTheme,  MuiThemeProvider} from '@material-ui/core/styles'
import Router from './Router';
import Auth from './Auth';


const theme = createMuiTheme({
  palette: {
    primary: {main:'#284089'},
    secondary:{main:'#FFFFFF'},
    error:{main:"#BE2E2E"},
  },
  typography: {
    "fontFamily": "\"Roboto\", \"Poppins\", sans-serif",
   }

});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
     	<Router childProps={Auth} /> 
   </MuiThemeProvider>
  );
}

export default App;