import React,{ Fragment, useEffect } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import {headers, pingAuction} from '../config';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import clsx from 'clsx';
import LogoBlue from '../images/pingAuctionLogoBlue.svg';
import { useDispatch } from 'react-redux';
import { sessionService } from 'redux-react-session';
import { logout } from '../redux/actions/LoginAction';
import { Redirect, Link } from 'react-router-dom';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    display: "flex",
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade('#92929D', 0.45),
    '&:hover': {
      backgroundColor: fade('#92929D', 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#92929D'
  },
  inputRoot: {
    color: 'black',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: theme.palette.secondary.main
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  // eslint-disable-next-line
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  navLogo: {
    width: '50px',
    height: 'auto'
  },
}));

const  AppBarFixed = (props)=> {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  // eslint-disable-next-line
  const dispatch = useDispatch()
  // const sessionState = useSelector(state=>state.session.user)
  const token = localStorage.getItem("token");
  const [auth, setAuth] = React.useState('')
  
  useEffect(()=>{
    axios.get(`${pingAuction}todays-item`, headers())
    .then(res => {
      
        const result = res.data.message
        setAuth(result)
        
    })
  })
  

  const handleLogout = ()=>{
    dispatch(logout(
      
    )).then(
        x=>{ 
        localStorage.removeItem("token");
        sessionService.deleteSession()
        sessionService.deleteUser()
        window.location.reload();
      
      }
    )
  
}

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };


  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <Link to="/profile" style={{textDecoration:'none',color:'black'}}>
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      </Link>
      <MenuItem onClick={()=>handleLogout()}>Logout</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <Link to="/profile" style={{textDecoration:'none', color:'black'}}>
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      </Link>
      <MenuItem onClick={()=>handleLogout()}>Logout</MenuItem>
    </Menu>
  );

  const checkAuth = ()=>{
    if(auth==='invalid token!'){
      localStorage.removeItem("token");
      sessionService.deleteSession()
      sessionService.deleteUser()
        return true
    }
    if(token===null){
      return true
    }
    else{
      return false
    }

  }
  
  return (
    <div className={classes.root}>
      
      {
       checkAuth() ===true ?
         <Redirect to ="/login" />:
         <Fragment>
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: props.open,
        })}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleClick}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: props.open,
              // eslint-disable-next-line
            })}
          >
            <img src={LogoBlue} className={classes.navLogo} alt="ping-logo"></img>
          </IconButton>


          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {/* <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div> */}

            <IconButton aria-label="show 17 new notifications" style={{ color: "#92929D" }}>
              <Badge badgeContent={17} >
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              style={{ color: "#92929D" }}
            >
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              style={{ color: "#92929D" }}
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
      </Fragment>
              }
    </div>
  );
}
export default AppBarFixed