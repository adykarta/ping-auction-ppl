import React from 'react';
import axios from 'axios';
import { pingAuction, headers } from '../config';
import { makeStyles, InputLabel,  Modal, Backdrop, Fade, Grid,  Button, TextField, Typography,  FormControl, OutlinedInput, InputAdornment, IconButton, } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    [theme.breakpoints.up('lg')]: {
      width: '30%',
    },
    [theme.breakpoints.only('md')]: {
      width: '30%',
    },
    [theme.breakpoints.down('md')]: {
      width: '30%',
    },
    [theme.breakpoints.down('sm')]: {
      width: '30%',
    },
    [theme.breakpoints.down('xs')]: {
      width: '90%',
    },
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 16,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  email: {
    marginTop: '15px'
  },
  handphone: {
    marginTop: '15px',
    width: '90%',
  },
}));

export default function EditProfile(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    handphone: '',
    email: '',
    // passwordLama: '',
    passwordBaru: '',
    ulangiPasswordBaru: '',
    passwordError: false,
    ulangiPasswordError: false,
    showPassword: false,
    showUlangiPassword: false,
    phoneExist: false,
    emailExist: false
  });



  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleClickShowUlangiPassword = () => {
    setValues({ ...values, showUlangiPassword: !values.showUlangiPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const handleMouseDownUlangiPassword = event => {
    event.preventDefault();
  };
  
  const checkEmail = () => {
    var err = values.emailError;
    var exs = values.emailExist;
    if (err === true) {
      return "*example@mail.com"
    }
    else if (exs === true) {
      return "*Email telah terdaftar"
    }
  };

  const handleInput = (form) => event => {
    const {value} = event.target

    if (form ==='phone') {
      var phoneno = /^[0-9\b]+$/;
      if(phoneno.test(value) || value ==='') {
        setValues(prev=>({...prev, handphone:value}))
        values.phoneExist = false;
      } else {
        values.phoneExist = false;
        return false;
      }
    } else if (form ==='email') {
      var mailreg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (mailreg.test(value) || value ==='') {
        setValues(prev=>({...prev, email:value}))
        values.emailError = false;
        values.emailExist = false;
        if (values.ulangiPasswordError === false && values.passwordError === false && values.agree === true && values.emailError === false && values.username !== '' && values.email !== '' && values.password !== '' && values.ulangiPassword !== '' && values.province !== '' && values.phone !== '') {
          values.ready = true;
        }
      } else {
        setValues(prev=>({...prev, email:value}))
        values.emailError = true;
        values.emailExist = false;
        values.ready = false;
      }
    } else if (form ==='password') {
      var pass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
      if (pass.test(value) || value ==='') {
        setValues(prev=>({...prev, passwordBaru:value}))
        values.passwordError = false;
        if (values.ulangiPasswordError === false && values.passwordError === false && values.agree === true && values.emailError === false && values.username !== '' && values.email !== '' && values.password !== '' && values.ulangiPassword !== '' && values.province !== '' && values.phone !== '') {
          values.ready = true;
        }
      } else {
        setValues(prev=>({...prev, passwordBaru:value}))
        values.passwordError = true;
        values.ready = false;
      }
    } else if (form ==='ulangiPassword') {
        var passw = values.password;
        if (passw === value || value ==='') {
          setValues(prev=>({...prev, ulangiPasswordBaru:value}))
          values.ulangiPasswordError = false;
          if (values.ulangiPasswordError === false && values.passwordError === false && values.agree === true && values.emailError === false && values.username !== '' && values.email !== '' && values.password !== '' && values.ulangiPassword !== '' && values.province !== '' && values.phone !== '') {
            values.ready = true;
          }
        } else {
          setValues(prev=>({...prev, ulangiPasswordBaru:value}))
          values.ulangiPasswordError = true;
          values.ready = false;
        }
    }
  }

  const validate = (data) => {
    if (data.status === 200) {
      props.handleSuccess()
    } else if (data.isHandphoneAvailable === false) {
      setValues(prev => ({ ...prev, phoneExist: true }))
    } else if (data.isEmailAvailable === false) {
      setValues(prev => ({ ...prev, emailExist: true }))
    } else {
      props.handleClose()
    }
  }

  const postData = () => {
    axios.post(`${pingAuction}profile/edit-profile`, {email: values.email, handphone: values.handphone, password: values.passwordBaru}, headers())
      .then(res => {
        const result = res.data
        validate(result)
        console.log(result)
      })
  }

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.open.state}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={props.open.state}>
          <div className={classes.paper}>
            <Grid container>
              <Grid item xs={12} md={12}>
                <Typography variant='h5'>Ubah Profile</Typography>
                <TextField
                  className={classes.handphone}
                  label="No.Handphone"
                  id="outlined-start-adornment"
               
                  onChange={handleInput('phone')}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">+62</InputAdornment>,
                  }}
                  variant="outlined"
                  size="small"
                  style={{ width: '80%', marginTop: '15px' }}
                  value={values.handphone}
                  error={values.phoneExist}
                  helperText={values.phoneExist ? "*Nomor telah terdaftar" : ""}
                />
                <TextField
                  data-testid="email"
                  className={classes.email}
                  id="outlined-basic"
                  label="Email"
                  value={values.email}
                  onChange={handleInput('email')}
                  variant="outlined"
                  size="small"
                  style={{ width: '80%' }}
                  error={values.emailError || values.emailExist}
                  helperText={checkEmail()}
                ></TextField>
                {/* <FormControl margin='dense' className={clsx(classes.textField)} variant="outlined" style={{ width: '80%', height: '10%', marginTop: '15px', marginBottom: '0px' }}>
                <InputLabel data-testid="pass1">Password Lama</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={values.showPasswordLama ? 'text' : 'password'}
                  value={values.passwordLama}
                  onChange={handleInput('password')}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {values.showPasswordLama ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                  labelWidth={75}
                  error={values.passwordLamaError}
                  helperText={values.passwordLamaError ? "*Password tidak sesuai" : ""}
                />
                </FormControl> */}
                <FormControl margin='dense' variant="outlined" style={{ width: '80%', height: '10%', marginTop: '15px', marginBottom: '15px' }}>
                  <InputLabel data-testid="pass1">Password Baru</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}
                    value={values.passwordBaru}
                    onChange={handleInput('password')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {values.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={120}
                    error={values.passwordError}
                    helperText={values.passwordError ? "*Minimal 8 karakter dan mengandung huruf dan angka" : ""}
                  />
                </FormControl>
                <FormControl margin='dense'  variant="outlined" style={{ width: '80%', height: '20%', marginTop: '15px', marginBottom: '10px' }}>
                  <InputLabel>Ulangi Password Baru</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={values.showUlangiPassword ? 'text' : 'password'}
                    value={values.ulangiPasswordBaru}
                    onChange={handleInput('ulangiPassword')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowUlangiPassword}
                          onMouseDown={handleMouseDownUlangiPassword}
                          edge="end"
                        >
                          {values.showUlangiPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={170}
                    error={values.ulangiPasswordError}
                    helperText={values.ulangiPasswordError ? "*Password anda tidak sesuai" : ""}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={12}>
                <Grid container>
                  <Grid item xs={6} md={6}>
                    <Button onClick={props.handleClose} style={{ backgroundColor: '#c23b22', color: 'white' }}>Cancel</Button>
                  </Grid>
                  <Grid item xs={6} md={6}>
                    <Button onClick={() => postData()} style={{ backgroundColor: '#284089', color: 'white' }}>Simpan</Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}