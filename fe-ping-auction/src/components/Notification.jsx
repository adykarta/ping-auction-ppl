import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button, Typography } from '@material-ui/core';
import Notif from '../images/notif.png'
import {Link} from 'react-router-dom'
const styles = makeStyles(theme=>({
  container:{
      backgroundColor:theme.palette.primary.main,
      width:'100vw',
      height:'100vh',
      display:'flex',
      justifyContent:'center',
      alignItems:'center'    
  },

  Text:{
      fontFamily:"ProximaNovaBold",
      fontSize:"1.5em",
      color:'white',
      textAlign:'center'
  },


  Button:{
      fontFamily:"ProximaNovaBold",
      fontSize:"1em",
      backgroundColor:"White",
      color: theme.palette.primary.main,
      display:'flex',
      margin:'auto',
      marginTop:'12px',
      '&:hover':{
        backgroundColor:'white'
        }

  },

  image:{
      [theme.breakpoints.up('sm')]: {
        width:'400px',
        height:'400px',
        display:'flex'
      },
      [theme.breakpoints.down('xs')]: {
        width:'100%',
        height:'100%',
        display:'flex'
      },

      marginBottom: '12px',
  }
    

}))

const Notification =(props)=>{
    const classes = styles()
    
    return(
        <Fragment>
            <div className={classes.container}>
                <Box>
                    <img src={Notif} className={classes.image} alt="img-notif"></img>
                    <Typography className={classes.Text}>{props.firstSentence}</Typography>
                    <Typography className={classes.Text}>{props.secondSentence}</Typography>
                    <Link to={props.link} style={{textDecoration:'none'}}>
                    <Button className={classes.Button} >{props.buttonText}</Button>
                    </Link>
                </Box>

            </div>

        </Fragment>
    )
}
export default Notification
