import React,{useState, Fragment,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import {headers, pingAuction} from '../config';
const useStyles = makeStyles(theme=>({
    timer:{
        fontFamily: 'roboto',
        fontSize: '1.5rem',
        color: '#284089',
      },
    timeExpired:{
        fontFamily: 'roboto',
        fontSize: '1.5rem',
        color: '#800000',

    }
}))

const Timer = (props)=>{
    const classes = useStyles()
    const[time, setTime] =useState(
        {
            days:0,
            hours:0,
            minutes:0,
            seconds:0,
            isExpired:false
        }
    )
    

    const setCountDown = () =>{
     
        var x = setInterval(()=>{
            var now = Date.now();
            
        
            var countDownDate = new Date(props.masa_lelang).getTime();
           
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
  
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (distance < 0) {
               
                clearInterval(x);
                setTime({
                    'days':0,
                    'hours':0,
                    'minutes':0,
                    'seconds':0,
                    isExpired:true
                })
                axios.post(`${pingAuction}bidding/akhir-lelang`,
                {id_barang:props.id},
                headers()
                ).then(res=>console.log(res))
                
               
                
            }
            else{
                setTime({
                    'days':days,
                    'hours':hours,
                    'minutes':minutes,
                    'seconds':seconds
                })

            }
                       
           
          
        },1000)
    
    }
    useEffect(()=>{
        setCountDown()
     },[])

    return(
        <Fragment>
             <h1 className={time.isExpired===true ? classes.timeExpired :classes.timer} style={props.color?{color:props.color} : null}>{time.isExpired===true ? "WAKTU HABIS" :time.days + ' hari ' +time.hours+':'+ time.minutes +':'+time.seconds  }</h1>
        </Fragment>

    )
}

export default Timer;