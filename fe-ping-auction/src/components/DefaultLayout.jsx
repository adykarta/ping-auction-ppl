import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Logo from '../images/pingAuctionLogo.svg';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import GavelIcon from '@material-ui/icons/Gavel';
import PersonIcon from '@material-ui/icons/Person';
import AppBarFixed from './AppBar';
import {Link} from 'react-router-dom'

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor:'white'
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor:theme.palette.primary.main
 
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
    
    backgroundColor:theme.palette.primary.main

  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing(3),
  },
  navLogo:{
    width:'50px',
    height:'auto',
    display: 'flex',
    alignItems: 'right'
  },
  parent: {
    backgroundColor: theme.palette.primary.main    ,
    '&:hover': {
      backgroundColor: '#0033cc'
    }

  },
}));

const DefaultLayout = ({children})=> {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBarFixed open ={open} handleClick={handleDrawerOpen}/>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
        <img src={Logo} className={classes.navLogo} alt="logo-ping" ></img>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          {['Home', 'Explore', 'Transaksi', 'Listing', 'Profile'].map((text, index) => 
            {
              const getIcon = (text)=>{  
                if (text==='Home'){
                    return(<Link to="/home" style={{textDecoration:'none'}}><HomeIcon color="secondary"/></Link>)
                }
                else if(text==='Explore'){
                    return(<Link to="/explore" style={{textDecoration:'none'}}><SearchIcon color="secondary"/></Link>)
                }
                else if(text==='Transaksi'){
                    return(<Link to="/transaksi" style={{textDecoration:'none'}}><QueryBuilderIcon color="secondary"/></Link>)
                }
                else if(text==='Listing'){
                    return(<Link to="/listing/create-barang" style={{textDecoration:'none'}}><GavelIcon color="secondary"/></Link>)
                }
                else if(text==='Profile'){
                    return(<Link to="/profile" style={{textDecoration:'none'}}><PersonIcon color="secondary"/></Link>)
                }       
              }


              return(
                <ListItem button key={text} 
                  className={
                    classes.parent
                  }
                >
                  <ListItemIcon>{getIcon(text)}</ListItemIcon>
                  <ListItemText primary={text} style={{color:theme.palette.secondary.main}} />
                </ListItem>
              )}
          )}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}  
      </main>
    </div>
  );
}
export default DefaultLayout;
