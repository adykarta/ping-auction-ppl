import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
import clsx from 'clsx';
import { Link } from 'react-router-dom'
import Logo from '../images/pingAuctionLogoWhite.svg';
import { Divider } from '@material-ui/core';
import Button from '@material-ui/core/Button';


const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },

  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    display: "flex",
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade('#92929D', 0.45),
    '&:hover': {
      backgroundColor: fade('#92929D', 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#92929D'
  },
  inputRoot: {
    color: 'black',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    background: 'transparent',
    boxShadow: 'none'
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 4,
  },
  hide: {
    display: 'none',
  },
  navLogo: {
    width: '128px',
    height: 'auto'
  },
  smallLogo: {
    width: '100px',
  }
}));

export default function AppBarFixed(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const token = localStorage.getItem("token");

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };


  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Lelang</MenuItem>
      <MenuItem onClick={handleMenuClose}>Tentang Kami</MenuItem>
      {
        token!==null || token !=="" ?
        <Link to="/home" style={{ textDecoration: 'none', color: 'white' }}>
        <MenuItem onClick={handleMenuClose}>Home</MenuItem>
      </Link>
      :
      <Link to="/login" style={{ textDecoration: 'none', color: 'white' }}>
        <MenuItem onClick={handleMenuClose}>Login</MenuItem>
      </Link>

      }
      
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <p>Lelang</p>
      </MenuItem>
      <Divider />
      <MenuItem>
        <p>Tentang Kami</p>
      </MenuItem>
      <Divider />
      {
        token!==null || token !=="" ?
        <Link to="/home" style={{ textDecoration: 'none', color: 'white' }}>
        <MenuItem onClick={handleMenuClose}><p>Home</p></MenuItem>
      </Link>
      :
      <Link to="/login" style={{ textDecoration: 'none', color: 'white' }}>
        <MenuItem onClick={handleMenuClose}><p>Login</p></MenuItem>
      </Link>

      }
    </Menu>
  );

  return (
    <div className={classes.root}>
      <AppBar
        position="static"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: props.open,
        })}
        style={{ paddingTop: '1vh' }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: props.open,
            })}
            style={{ marginLeft: '2vw' }}
          >
            <img src={Logo} className={classes.navLogo} alt="logo"></img>
          </IconButton>

          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {/* <img edge="start" src={LogoWhite} /> */}
            <div className={classes.grow} />
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <Button className={classes.barButton} color="inherit">Lelang</Button>
            </IconButton>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <Button className={classes.barButton} color="inherit">Tentang Kami</Button>
            </IconButton>
            <Link to="/login" style={{ textDecoration: 'none', color: 'white' }}>
              <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                <Button className={classes.barButton} color="inherit">Login</Button>
              </IconButton>
            </Link>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              style={{ color: "white" }}
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
}
