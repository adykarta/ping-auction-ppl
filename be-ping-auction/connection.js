var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
const Pool = require('pg').Pool
const pool = new Pool({
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    password: process.env.PG_PASSWORD,
    port: process.env.PG_PORT_DB,
})

pool.connect(function (err) {
    if (err) {
        throw err;
    }
    let createCategoryTable = `CREATE TABLE IF NOT EXISTS KATEGORI(id_kategori SERIAL not null primary key,
        
        nama_kategori varchar(50) not null);`

    let createBarangTable = `CREATE TABLE IF NOT EXISTS BARANG(id_barang SERIAL not null primary key,
        judul varchar(100)  not null,
        deskripsi varchar(250) not null,
        harga_awal integer  not null,
        harga_akhir integer not null,
        lokasi varchar(50) not null,
        masa_lelang timestamp not null,
        kelipatan_bid integer[] not null,
        isNew boolean not null,
        merk varchar(50) not null,
        created_date timestamp not null,
        status varchar(20),
        jumlah_view integer not null,
        bid_terakhir integer,
        id_penjual integer not null,
        id_pembeli integer,
        id_kategori integer not null,

        foreign key (id_kategori) references KATEGORI(id_kategori) on delete cascade on update cascade,
        foreign key (id_penjual) references USERS(id_user) on delete cascade on update cascade,
        foreign key (id_pembeli) references USERS(id_user) on delete cascade on update cascade
        );`

    let createProvTable = `CREATE TABLE IF NOT EXISTS PROVINSI(id_prov SERIAL not null primary key,
        
        nama_prov varchar(50) not null);`
    let createRiwayatBidTable = `CREATE TABLE IF NOT EXISTS RIWAYAT_BID(id_riwayat_bid SERIAL not null primary key,
        
            jumlah integer not null,
            id_user integer not null,
            id_barang integer not null,
            created_date timestamp not null,
            foreign key (id_user) references USERS(id_user) on delete cascade on update cascade,
            foreign key (id_barang) references BARANG(id_barang) on delete cascade on update cascade


            
            );`
    let createTransaksiTable = `CREATE TABLE IF NOT EXISTS TRANSAKSI(id_transaksi SERIAL not null primary key,
        id_barang integer not null,
        id_user integer not null,
        bid integer not null, 
        created_date timestamp not null,
        foreign key (id_user) references USERS(id_user) on delete cascade on update cascade,
        foreign key (id_barang) references BARANG(id_barang) on delete cascade on update cascade

        
        );`
    let createForumTable = `CREATE TABLE IF NOT EXISTS FORUM(id_forum SERIAL not null primary key,
        id_barang integer not null,
        isi varchar(250) not null,
        created_date timestamp not null,
        id_user integer not null,
        role varchar(20) not null,
        foreign key (id_user) references USERS(id_user) on delete cascade on update cascade,
        foreign key (id_barang) references BARANG(id_barang) on delete cascade on update cascade

        
        );`
    let createGambarTable = `CREATE TABLE IF NOT EXISTS IMG(id_img SERIAL not null primary key,
        id_barang integer not null,
        url varchar(250) not null,
        
        foreign key (id_barang) references BARANG(id_barang) on delete cascade on update cascade

        
        );`
    let createUserTable = `CREATE TABLE IF NOT EXISTS USERS(id_user SERIAL not null primary key,
        username varchar(50) unique not null,
        email varchar(50) unique not null,
        handphone varchar(50) unique not null,
        password varchar(200) not null,
        created_date timestamp not null,
        id_prov integer not null,
        token varchar(50),
        foreign key (id_prov) references PROVINSI(id_prov) on delete cascade on update cascade
        );`

    let createThreadForumTable = `CREATE TABLE IF NOT EXISTS THREAD_FORUM(id_thread SERIAL not null primary key, 
        id_forum integer not null,
        id_user integer not null, 
        created_date timestamp not null,
        isi varchar(250) not null,
        role varchar(20) not null,
        foreign key (id_forum) references FORUM(id_forum) on delete cascade on update cascade,
        foreign key (id_user) references USERS(id_user) on delete cascade on update cascade
        );`
    pool.query(createCategoryTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM KATEGORI;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var kategori = JSON.parse(string)

            if (kategori.rowCount === 0) {

                let createKategori = `INSERT INTO KATEGORI(nama_kategori) VALUES ('dummy');`
                pool.query(createKategori, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createProvTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM PROVINSI;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var provinsi = JSON.parse(string)

            if (provinsi.rowCount === 0) {
                let createProvinsi = `INSERT INTO PROVINSI(nama_prov) VALUES ('colorado');`
                pool.query(createProvinsi, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createUserTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM USERS;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var users = JSON.parse(string)

            if (users.rowCount === 0) {
                var hashedPassword = bcrypt.hashSync('admin');
                var date = dateFormat(new Date())
                let createUser = `INSERT INTO USERS(username, email, handphone, password, created_date, id_prov) 
                VALUES ('admin','admin@gmail.com','081317250076','${hashedPassword}','${date}','1');`
                pool.query(createUser, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })

    pool.query(createBarangTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM BARANG;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var barang = JSON.parse(string)

            var date = dateFormat(new Date())
            if (barang.rowCount === 0) {
                let createBarang = `INSERT INTO BARANG(judul, deskripsi, harga_awal, harga_akhir,lokasi,masa_lelang, kelipatan_bid,isNew,jumlah_view,merk,created_date,status,id_penjual, id_kategori) 
                VALUES ('dummy1','lorem ipsum', 10000,100000,'Banten','${date}',ARRAY[100000],'true',0,'merk1','${date}','tersedia',1,1);`
                pool.query(createBarang, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createRiwayatBidTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM RIWAYAT_BID;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var bid = JSON.parse(string)
            var date = dateFormat(new Date())

            if (bid.rowCount === 0) {
                let createRiwayat = `INSERT INTO RIWAYAT_BID(jumlah, id_user,id_barang,created_date) VALUES (100000,1,1,'${date}');`
                pool.query(createRiwayat, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createGambarTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM IMG;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var gambar = JSON.parse(string)


            if (gambar.rowCount === 0) {
                let createGambar = `INSERT INTO IMG(id_barang, url) VALUES (1,'https://www.sciencesetavenir.fr/assets/img/2017/03/29/cover-r4x3w1000-58dbbd655242b-capture-d-e-cran-2017-03-29-a-15-55-40.png');`
                pool.query(createGambar, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createTransaksiTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM TRANSAKSI;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)
            var transaksi = JSON.parse(string)
            var date = dateFormat(new Date())
            if (transaksi.rowCount === 0) {
                let createTransaksi = `INSERT INTO TRANSAKSI(id_barang, id_user, bid, created_date) VALUES (1,1,100000,'${date}');`
                pool.query(createTransaksi, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createForumTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM FORUM;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)

            var forum = JSON.parse(string)
            var date = dateFormat(new Date())

            if (forum.rowCount === 0) {
                let createForum = `INSERT INTO FORUM(id_barang, isi,created_date, id_user,role) VALUES (1,'lorem ipsum','${date}',1,'pembeli');`
                pool.query(createForum, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
    pool.query(createThreadForumTable, function (error, results, fields) {
        if (error) {
            throw error;
        }
        var val = []
        var setValue = function (value) {
            val = value;
        }
        let query = `SELECT * FROM THREAD_FORUM;`
        pool.query(query, function (error, results) {
            if (error) {
                res.send({
                    auth: false,
                    message: error
                })
            }
            setValue(results);
            var string = JSON.stringify(val)

            var thread_forum = JSON.parse(string)
            var date = dateFormat(new Date())

            if (thread_forum.rowCount === 0) {
                let createThreadForum = `INSERT INTO THREAD_FORUM(id_forum,id_user,created_date, isi,role) VALUES (1,1,'${date}','lorem ipsum','penjual');`
                pool.query(createThreadForum, function (error, results) {
                    if (error) {
                        console.log(error);
                    }
                })
            }
        });
    })
});

module.exports = pool;
