
const helper = require('../helpers');
var connection = require('../connection')

exports.getProv = function(req,res){
    let query = `SELECT * FROM PROVINSI;`
    var val = []
    var setValue = function (value) {
        val = value;
    }
 
    connection.query(query,(err,result)=>{
        if(err){
            res.send({
                message:err,
                status:helper.status.error
            })
            res.end()
        }
        setValue(result.rows);
        var stringProv = JSON.stringify(val)
        var provDetails = JSON.parse(stringProv)
        res.send({
            status:helper.status.success,
            data:provDetails
        })
        res.end()
    })

}