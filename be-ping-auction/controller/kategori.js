var connection = require('../connection')
const helper = require('../helpers');


exports.listCategory = function(req, res){
    var query = `SELECT * FROM KATEGORI;`

    connection.query(query, (err,result) => {
        if(err){
            res.send({
                message: err,
                status: helper.status.error
            })
            res.end()
        }
        res.send({
            message: "Success Displaying",
            data: result.rows
        })
        res.end()
    })
}