
const helper = require('../../helpers');
var connection = require('../../connection')
const jwt = require('jsonwebtoken');
var config = require('../../config');

exports.listBarang = function(req,res){
	jwt.verify(req.token, config.secretkey, (err, authData) => {
		if(err){
			res.send({
				message: "invalid token!"
			})
			res.end()
		}
		else{
			let query = `SELECT id_user from USERS where username = '${authData.username}';`
			connection.query(query,(err,results)=>{
				if(err){
					res.send({
						message:err,
						status:helper.status.error
					})
					res.end()
				}
				const id_user = results.rows[0].id_user
				let queryBarang = `SELECT * FROM BARANG where id_penjual = ${id_user};`
				connection.query(queryBarang,async(err,result)=>{
					if(err){
						res.send({
							message:err,
							status:helper.status.error
						})
						res.end()
					}
					var listUserBarang= result.rows

					 await Promise.all(listUserBarang.map(async(element)=>{
                 
                    var created_date = element.created_date
                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    var today = new Date();
                    var dateCreated = new Date(created_date);
                    
                    var diffDays = Math.round(Math.abs((today - dateCreated) / oneDay));
                
                    let jumlah_peserta_query = `select count(distinct id_user) from riwayat_bid where id_barang=${element.id_barang}`
                    var jumlah_peserta =0
                    var img_url = ''
                    var setValuePeserta = function (value) {
                       jumlah_peserta= value;
                    };
                    var setValueImg = function (value){
                        img_url = value
                    }
                    await new Promise((resolve,reject)=>{
                        connection.query(jumlah_peserta_query, (err,results)=>{
                            if(err){
                                res.send({
                                    message: err,
                                    status: helper.status.error
                                })
                                res.end()
                            }
                            setValuePeserta(results.rows[0].count)
                            resolve()

                        })
               
                    })
                    await new Promise((resolve,reject)=>{
                        let query_img = `SELECT url from img where id_barang=${element.id_barang};`
                        connection.query(query_img,(err,results)=>{
                            if(err){
                                res.send({
                                    message:err,
                                    status:helper.status.error
                                })
                                res.end()
                            }
                            setValueImg(results.rows)
                            resolve()
                        })
                   
                       

                    })
                  
                    Object.assign(element, { 'jumlah_peserta': jumlah_peserta,'lama_berlangsung':diffDays,'img':img_url });
                  
         
         
                }))
           
                res.send({
                    data:listUserBarang,
                    status:helper.status.success
                })
                res.end()
                })
              
            })
         
		}
	})
}