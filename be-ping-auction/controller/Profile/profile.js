var connection = require('../../connection')
const jwt = require('jsonwebtoken');
const config = require('../../config');
const helper = require('../../helpers');
var bcrypt = require('bcryptjs');

exports.editProfile = function(req,res){
    jwt.verify(req.token, config.secretkey,(err,authData)=>{
        if(err){
            res.json({
                message:'invalid token!'
            });
        }
        else{
            var username = authData.username;
            var email = req.body.email;
            var password = req.body.password
            var handphone = req.body.handphone;
            if(handphone !== ""){
                if(handphone.charAt(0)!='0'){
                    handphone = '0'+handphone
                }

            }
           
            var hashedPassword = bcrypt.hashSync(password);
         
            var data = {
                        isEmailAvailable:true,
                        isHandphoneAvailable:true,
                  
                        }
        
            var val = []
            var setValue = function (value) {
                val = value;
            }
            let validateEmail = `SELECT * FROM USERS WHERE (email = '${email}') ;`
            connection.query(validateEmail, (err, result)=>{
                if(err){
                    res.send({
                        message:err,
                        status:helper.status.error
                    })
                    res.end()
                }

                setValue(result.rows)
                var string = JSON.stringify(val)
                var users = JSON.parse(string)
              
                if(users!= 0){
                    data = {...data,isEmailAvailable:false}
                }
               
                
                 
                    let validateHandphone =  `SELECT * FROM USERS WHERE (handphone = '${handphone}') ;`
                    connection.query(validateHandphone,(err,result)=>{
                        if(err){
                            res.send({
                                message:err,
                                status:helper.status.error
                            })
                            res.end()
                        }
                    
                        setValue(result.rows)
                        var string = JSON.stringify(val)
                        var users = JSON.parse(string)
                        if(users != 0){
                            data = {...data,isHandphoneAvailable:false}
                        }
                    
                        if(data.isEmailAvailable===false || data.isHandphoneAvailable===false ){
                            res.send({
                                ...data
                            })
                            res.end()
                            
                        }
                        else{
                            let query_updateall = `UPDATE USERS set email='${email}', password='${hashedPassword}', handphone='${handphone}' where username='${username}';`

                            if(email==='' && password==='' && handphone!==''){
                                query_updateall = `UPDATE USERS set handphone='${handphone}' where username='${username}';`
                            }
                            else if(email==='' && password !=='' && handphone===''){
                                query_updateall = `UPDATE USERS set password='${hashedPassword}' where username='${username}';`
                            }
                            else if(email!=='' && password ==='' && handphone ===''){
                                query_updateall = `UPDATE USERS set email='${email}' where username='${username}';`
                            }
                            else if(email!=='' && password !=='' && handphone ===''){
                                query_updateall = `UPDATE USERS set email='${email}', password='${hashedPassword}' where username='${username}';`
                            }
                            else if(email!=='' && password ==='' && handphone !==''){
                               query_updateall = `UPDATE USERS set email='${email}', handphone='${handphone}' where username='${username}';`
                            }
                            else if(email ==='' && password !=='' && handphone !==''){
                               query_updateall = `UPDATE USERS set password='${hashedPassword}', handphone='${handphone}' where username='${username}';`
                            }
                           
                        
                            
                            connection.query(query_updateall, (error, result) => {
                                if (error) {
                                    res.send({
                                        message: error,
                                        status:helper.status.error
            
                                        
                                    })
                                
                                    res.end()
                                }
                                
                                else{
                                 
                                    res.send({
                                      ...data,message:'success',status:helper.status.success
                                    })
                                    res.end()
                                }
                            })
                          
                            
                        }
        
                    })
                 
                })
              
        }
    })
}