
const helper = require('../../helpers');
var connection = require('../../connection')
const jwt = require('jsonwebtoken');
var config = require('../../config');

exports.getUser = function (req, res) {
  jwt.verify(req.token, config.secretkey, (err, authData) => {
    if (err) {
      res.send({
        message: "invalid token!"
      })
      res.end()
    }
    else {
      let query = `SELECT username, email, handphone, created_date FROM USERS WHERE username = '${authData.username}';`
      connection.query(query, (err, results) => {
        if (err) {
          res.send({
            message: err,
            status: helper.status.error
          })
          res.end()
        }
        var listUserDetail = results.rows
        res.send({
          status: helper.status.success,
          data: listUserDetail
        })
        res.end()
      })
     
    }
  })
}