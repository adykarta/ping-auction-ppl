var connection = require('../connection')
const helper = require('../helpers');
const jwt = require('jsonwebtoken');
const config = require('../config');

exports.listCategory = function(req, res){
    jwt.verify(req.token, config.secretkey, (err, authData)=>{
        if(err){
            res.json({
                message:'invalid token!'
            });
        }
        else{
            var query = `SELECT * FROM KATEGORI`

            connection.query(query, (err,result) => {
                if(err){
                    res.send({
                        message: err,
                        status: helper.status.error
                    })
                    res.end()
                }

                res.send(result.rows)
                console.log(result.rows)
                res.end()
            })
            
        }
    })
    
}