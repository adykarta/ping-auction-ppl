
const jwt = require('jsonwebtoken');
const config = require('../../config');
var randtoken = require('rand-token');
const helper = require('../../helpers');

var connection = require('../../connection')
var bcrypt = require('bcryptjs');

var dateFormat = require('dateformat');
const refreshTokens = {};


exports.initial = function(req,res){
    res.status(200).send
    ({ 
        message: `Hello World ${new Date()}`, 
        test: process.env.TEST,
        PG_USER: process.env.PG_USER,
        PG_HOST: process.env.PG_HOST,
        PG_DB: process.env.PG_DATABASE,
        PG_PASSWORD: process.env.PG_PASSWORD,
        PG_PORT: process.env.PG_PORT_DB
     });
}

exports.forgotPassword = function(req,res){
    res.status(200).send({ message: `Hello World ${new Date()}` }); 
}


exports.token = function (req, res, next) {
    var username = req.body.username  
    var refreshToken = req.body.refreshToken
    if ((refreshTokens[refreshToken].refreshToken === refreshToken) && (refreshTokens[refreshToken].username === username)) {
        var token = jwt.sign({
            username: username
        }, config.secretkey, {
            expiresIn: 300
        })
        refreshTokens[refreshToken].token = token
        res.json({
            token: 'Bearer ' + token
        })
    } else {
        res.sendStatus(401)
    }
}

exports.login = function (req, res) {
  
    var username = req.body.username
    var password = req.body.password

    let query = `SELECT * FROM USERS WHERE (username = '${username}' OR email='${username}') ;`

    var val = []
    var setValue = function (value) {
        val = value;
    }


    connection.query(query, (error,results)=> {
        
         
        setValue(results.rows);
        var stringUser = JSON.stringify(val)
        var userDetails = JSON.parse(stringUser)

        if (stringUser === '[]') {
            res.send({
                auth: false,
                message: "Invalid Username"
            })
            res.end()
        } else {
            var passwordIsValid = bcrypt.compareSync(password, userDetails[0].password);
            if (!passwordIsValid) {
                res.send({
                    auth: false,
                    message: "Invalid Password!"
                });
                res.end()
            } else { 
                          
                var token = jwt.sign({
                    username: userDetails[0].username
                }, config.secretkey, {
                    expiresIn: 86400 // expires in 24 hours
                });
                var refreshToken = randtoken.uid(256);
                refreshTokens[refreshToken] = {
                    username: userDetails[0].username,
                    token: token,
                    refreshToken: refreshToken
                }
                res.send({
                    auth: true,
                    token: token,
                    refreshToken: refreshToken,
                    status:helper.status.success,
                    username:userDetails[0].username
                });
                
                res.end()

                    
                

            }
        }

    });
   
}

exports.checkRole = function(req,res){
    jwt.verify(req.token, config.secretkey, (err, authData) => {
        if (err) {
            res.json({
                message: "invalid token!"
            });
        } else {
            const username = authData.username;
            const id_barang = req.body.id_barang
          
            let query = `SELECT id_user from users where username='${username}';`
            connection.query(query,(err,results)=>{
                if(err){
                    res.send({
                        error:err,
                        status:helper.status.error,
                    })
                    res.end()
                }
                else{
                    const id_user = results.rows[0].id_user  
                    let checkRole = `SELECT id_penjual from barang where id_barang = ${id_barang};`
                    connection.query(checkRole, (err,results)=>{
                        if(err){
                            res.send({
                                message:err,
                                status:helper.status.error
                            })
                            res.end()
                        }
                        else{

                            const id_penjual = results.rows[0].id_penjual
                            let role = ''
                            if(id_user===id_penjual){
                                role = 'penjual'
                            }
                            else{
                                role='pembeli'
                            }
                            
                            res.send({
                                data:{role:role},
                                status:helper.status.success
                            })
                            res.end()
                        }
                       
                       

                    }) 
                   

            
                }
            })
        }
       
    });
}

exports.logout = function (req, res) {
    jwt.verify(req.token, config.secretkey, (err, authData) => {
        if (err) {
            res.json({
                message: "invalid token!"
            });
        } else {
            res.status(200).send({
                auth: false,
                token: null
            });
        }
        res.end()
    });
}
exports.register = function(req, res){
   
    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password
    var handphone = req.body.handphone;
    
    if(handphone.charAt(0)!='0'){
        handphone = '0'+handphone
    }
    var hashedPassword = bcrypt.hashSync(password);
    var date = dateFormat(new Date());
    var id_prov = req.body.id_prov;
    var data = {isUsernameAvailable:true,
                isEmailAvailable:true,
                isHandphoneAvailable:true,
          
                }

    var val = []
    var setValue = function (value) {
        val = value;
    }
    let validateEmail = `SELECT * FROM USERS WHERE (email = '${email}') ;`
    connection.query(validateEmail, (err, result)=>{
        if(err){
            res.send({
                message:err,
                status:helper.status.error
            })
            res.end()
        }
        setValue(result.rows)
        var string = JSON.stringify(val)
        var users = JSON.parse(string)
      
        if(users!= 0){
            data = {...data,isEmailAvailable:false}
        }
       
        let validateUsername = `SELECT * FROM USERS WHERE (username = '${username}') ;`
        connection.query(validateUsername, (err, result)=>{
            if(err){
                res.send({
                    message:err,
                    status:helper.status.error
                })
                res.end()
            }
            setValue(result.rows)
            var string = JSON.stringify(val)
            var users = JSON.parse(string)
            if(users != 0){
                data = {...data,isUsernameAvailable:false}
            }
         
            let validateHandphone =  `SELECT * FROM USERS WHERE (handphone = '${handphone}') ;`
            connection.query(validateHandphone,(err,result)=>{
                if(err){
                    res.send({
                        message:err,
                        status:helper.status.error
                    })
                    res.end()
                }
                setValue(result.rows)
                var string = JSON.stringify(val)
                var users = JSON.parse(string)
                if(users != 0){
                    data = {...data,isHandphoneAvailable:false}
                }
            
                if(data.isEmailAvailable===false || data.isHandphoneAvailable===false || data.isUsernameAvailable===false){
                    res.send({
                        ...data
                    })
                    res.end()
                    
                }
                else{
                    let createUser = `INSERT INTO USERS(username, email, handphone, password, created_date, id_prov) 
                    VALUES ('${username}','${email}','${handphone}','${hashedPassword}','${date}','${id_prov}');`
    
                    
                    connection.query(createUser, (error, result) => {
                        if (error) {
                            res.send({
                                message: error,
                                status:helper.status.error
    
                                
                            })
                        
                            res.end()
                        }
                        else{
                            res.send({
                              ...data,message:'success',status:helper.status.success
                            })
                            res.end()
                        }
                    })
                 
                    
                }

            })
            // connection.end() 

        })
        // connection.end()
        
    
    })
    // connection.end()
    
}
