var connection = require('../../connection')
const helper = require('../../helpers');
var randtoken = require('rand-token');
var bcrypt = require('bcryptjs');


function sendMail(email, token){
  
    const sgMail = require('@sendgrid/mail');
    const link = `http://${process.env.URL}/#/setNewPassword?token=${token}`
    sgMail.setApiKey('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');

    const msg = {
    to: email,
    from: 'Ping!Auction@gmail.com',
    subject: 'Forgot your Password?',
    text: "please click this link " + link + " to reset your password",
    html: "<strong>please click this link " + link +  " to reset your password</strong>",
    };
    //ES6
    // sgMail
    // .send(msg)
    // .then(() => {}, console.error);
    //ES8
    (async () => {
    try {
        const result = await sgMail.send(msg);
        console.log('Succeesdddd')
        
    } catch (err) {
        console.error(err.toString());
    }
    })();
}


exports.forgotPassword = function(req, res){
    const email = req.body.email

    connection.query(`SELECT * FROM USERS WHERE email = $1`, [email], (err,result) => {
        if(err){
            console.log(err)
            res.end()

        }
        if(result.rows.length == 1){
            var newToken = randtoken.uid(50);
            connection.query(`UPDATE USERS SET token = '${newToken}' WHERE email = '${email}'`,
            function(err, rows){
                if(err){
                    console.log(err)
                    res.end()
                    
                }
                sendMail(email, newToken)
                res.send({
                    message: 'Email successfully send',
                    token: newToken
                })
                res.end()
            })
        }
        else {
            res.send({message: 'failed'})
            res.end()
        }

    })

}

exports.setNewPassword = function(req, res) {
    const newPassword = req.body.password
    const givenToken = req.body.token
    var email = "";
    connection.query(`SELECT * FROM USERS WHERE token = $1`, [givenToken], (err, result) => {
        if(err){
            
            return console.log(err)
        }
        email = result.rows[0].email
        if(result.rows.length == 1){
            connection.query(`UPDATE USERS SET token = NULL WHERE token = '${givenToken}'`, (err, result) => {
                if(err){
                   
                    return console.log(err)
                }
                var hashedPassword = bcrypt.hashSync(newPassword);
                connection.query(`UPDATE USERS SET password = '${hashedPassword}' WHERE email = '${email}'`, async (err, result) => {
                    if(err){
                       
                        return console.log(err)
                    }
                    const send = await res.send({
                        message: 'Password updated',
                        token: givenToken
                    })
                })
               
            
            })
         
        }
        else{
            res.send({message: 'failed'})
            res.end()
        }
    })
 
}

