const fs = require('fs');
const AWS = require('aws-sdk');

// Enter copied or downloaded access id and secret here

// Enter the name of the bucket that you have created here
const BUCKET_NAME = 'ping-image';
AWS.config.update({
    accessKeyId: process.env.ACCESS_ID,
    secretAccessKey:process.env.ACCESS_SECRET
})

// Initializing S3 Interface
const s3 = new AWS.S3();

exports.uploadFile= function (file){
    // read content from the file
   

    // setting up s3 upload parameters
    const params = {
        Bucket: BUCKET_NAME,
        Key: file.originalname,
        Body: file.buffer,
        ContentType:file.mimetype,
    
    };

    // Uploading files to the bucket
    
   s3.upload(params, function(err, data) {
        if (err) {
            throw err
        }
    
        console.log(`File uploaded successfully.`)
        
    })
    // console.log(url)
  
};

exports.getImageUrl = function(file){
    // setting up s3 upload parameters
    const params = {
        Bucket: BUCKET_NAME,
        Key: file.originalname, 
        // Body: fileContent
        // LocationConstraint: "ap-south-1"
    };

    s3.getSignedUrl('getObject', params, function(err, url, data){
        if(err){
            throw err;
        }
        console.log(url);
        s3.getObject(params, function(err, data){
            if(err){
                throw err
            }
            console.log(data) 
            // console.log(err); 
        }); 
        
    }); 
}
// Enter the file you want to upload here
// uploadFile('cat.jpg');
