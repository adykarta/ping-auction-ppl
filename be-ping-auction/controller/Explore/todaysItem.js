var connection = require('../../connection')
const jwt = require('jsonwebtoken');
const config = require('../../config');
const helper = require('../../helpers');


// exports.todaysItem = function(req, res){
//     jwt.verify(req.token, config.secretkey, (err, authData) => {
//         if(err){
//             res.json({
//                 message: "invalid token!"
//             })
//         }
//         else{
//             var query = `SELECT BARANG.*, img.url FROM BARANG INNER JOIN img ON BARANG.id_barang = img.id_barang WHERE barang.status='tersedia' and barang.created_date >= NOW() - '1 day'::INTERVAL;`

//             connection.query(query, async (err, result) => {
//                 if(err){
//                     res.send({
//                         message: err,
//                         status: helper.status.error
//                     })
//                     res.end()
                  
//                 }

//                 var data = result.rows
//                 await Promise.all(data.map((element) => {
//                     var created_date = element.created_date
//                     const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
//                     const today = new Date();
//                     const dateCreated = new Date(created_date);
//                     const diffDays = Math.round(Math.abs((today - dateCreated) / oneDay));
//                     Object.assign(element, {'lama_berlangsung':diffDays});
//                 }))
                

                
//                 res.send(data)
           
//                 res.end()
               
//             })
           
//         }
//     })
     
// }

exports.todaysItem = async function(req,res){
    jwt.verify(req.token, config.secretkey, (err, authData)=>{
        if(err){
            res.json({
                message:'invalid token!'
            });
        }
        else{

        
            var query = `SELECT b.*, k.nama_kategori FROM BARANG as b join kategori as k on b.id_kategori=k.id_kategori where b.status='tersedia' and b.created_date >= NOW() - '1 day'::INTERVAL;`

            connection.query(query, async(err,results) => {
                if(err){
                    res.send({
                        message: err,
                        status: helper.status.error
                    })
                    res.end()
                }
                var data = results.rows
               
                await Promise.all(data.map(async(element)=>{
                 
                    var created_date = element.created_date
                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    var today = new Date();
                    var dateCreated = new Date(created_date);
                    
                    var diffDays = Math.round(Math.abs((today - dateCreated) / oneDay));
                
                    let jumlah_peserta_query = `select count(distinct id_user) from riwayat_bid where id_barang=${element.id_barang}`
                    var jumlah_peserta =0
                    var img_url = ''
                    var setValuePeserta = function (value) {
                       jumlah_peserta= value;
                    };
                    var setValueImg = function (value){
                        img_url = value
                    }
                    await new Promise((resolve,reject)=>{
                        connection.query(jumlah_peserta_query, (err,results)=>{
                            if(err){
                                res.send({
                                    message: err,
                                    status: helper.status.error
                                })
                                res.end()
                            }
                            setValuePeserta(results.rows[0].count)
                            resolve()

                        })
                        
                    })
                    await new Promise((resolve,reject)=>{
                        let query_img = `SELECT url from img where id_barang=${element.id_barang};`
                        connection.query(query_img,(err,results)=>{
                            if(err){
                                res.send({
                                    message:err,
                                    status:helper.status.error
                                })
                                res.end()
                            }
                            setValueImg(results.rows)
                            resolve()
                        })
                       

                    })
                  
                    Object.assign(element, { 'jumlah_peserta': jumlah_peserta,'lama_berlangsung':diffDays,'img':img_url });
                  
         
         
                }))
                res.send(data)
                res.end()
       
            })
 
                
        }
    })
}