const jwt = require('jsonwebtoken');
const connection = require('../../connection');
const helper = require('../../helpers');
const config = require('../../config');
const dateformat = require('dateformat')


exports.postToForum = function (req,res){
    jwt.verify(req.token,config.secretkey,(err,authData)=>{
        if(err){
            res.json({
                message: "invalid token!"
            });
            res.end()
        }
        else{
            const username = authData.username;
            const id_barang = req.body.id_barang;
            const isi = req.body.isi;
            let query = `SELECT id_user from users where username='${username}';`
            
            connection.query(query,async(err,results)=>{
                if(err){
                    res.send({
                        message: err,
                        status:helper.status.error
                    })
                    res.end()
                }
                else{
                    const id_user = results.rows[0].id_user
                    let id_penjual = 0;
                    let role = '';
                    let checkRole = `SELECT id_penjual from barang where id_barang = ${id_barang};`
                
                    await new Promise((resolve,reject)=>{
                        connection.query(checkRole, (err,results)=>{
                            if(err){
                                res.send({
                                    message:err,
                                    status:helper.status.error
                                })
                                res.end()
                            }
                            id_penjual = results.rows[0].id_penjual

                            resolve()
    
                        })
                       
                    })
                    if(id_penjual===id_user){
                        role='penjual'
                    }else{
                        role='pembeli'
                    }
                
                    const date = dateformat(new Date())
                
                    let insertQuery = `INSERT INTO FORUM(id_barang, isi,created_date, id_user,role) VALUES (${id_barang},'${isi}','${date}',${id_user},'${role}');`
                    connection.query(insertQuery,(err,results)=>{
                        if(err){
                            res.send({
                                message:err,
                                status:helper.status.error
                            })
                            res.end()
                        }
                        else{
                            res.send({
                                message:'success',
                                status:helper.status.success
                            })
                            res.end()

                        }
                    })
                  
                }
                   
            })
         
        }
    })

   
}

exports.postToThreadForum = function (req,res){
    jwt.verify(req.token,config.secretkey,(err,authData)=>{
        if(err){
            res.json({
                message: "invalid token!"
            });
            res.end()
        }
        else{
            const username = authData.username;
            const id_forum = req.body.id_forum;
            const isi = req.body.isi;
            let query = `SELECT id_user from users where username='${username}';`
    
            connection.query(query,async(err,results)=>{
                if(err){
                    res.send({
                        message: err,
                        status:helper.status.error
                    })
                    res.end()
                }
                else{
                    let id_penjual = 0;
                    let role = '';
                    const id_user = results.rows[0].id_user
                    let checkRole = `SELECT id_penjual from barang where id_barang = (select id_barang from forum where id_forum=${id_forum});`
                    
                    await new Promise((resolve,reject)=>{
                        connection.query(checkRole, (err,results)=>{
                            if(err){
                                res.send({
                                    message:err,
                                    status:helper.status.error
                                })
                                res.end()
                            }
                            id_penjual = results.rows[0].id_penjual
                            resolve()
    
                        })
                       
                        
                    })

                    if(id_penjual===id_user){
                        role='penjual'
                    }else{
                        role='pembeli'
                    }


                    const date = dateformat(new Date())
                
                    let insertQuery = `INSERT INTO THREAD_FORUM(id_forum,id_user,isi,created_date,role) VALUES (${id_forum},${id_user},'${isi}','${date}','${role}');`
                    connection.query(insertQuery,(err,results)=>{
                        if(err){
                            res.send({
                                message:err,
                                status:helper.status.error
                            })
                            res.end()
                        }
                        else{
                            res.send({
                                message:'success',
                                status:helper.status.success
                            })
                            res.end()

                        }
                    })  
                   
                }
                   
            })
           
        }
    })

   
}

exports.getForum =  async function(req,res){
    jwt.verify(req.token,config.secretkey,(err,authData)=>{
        if(err){
            res.json({
                message: "invalid token!"
            });
            res.end()
        }
        else{
            let data = {}
            const id_barang = req.params.id_barang
      
            let query = `select f.*, u.username from forum as f join users as u on f.id_user = u.id_user and f.id_barang =${id_barang};`
           

           
            connection.query(query,async(err,results)=>{
                if(err){
                    res.send({
                        message: err,
                        status:helper.status.error
                    })
                    res.end()
                }
                else{
                    data = results.rows
                    await Promise.all(data.map(async(element)=>{
                        var threadForum = [];
                        var setValue = function (value) {
                            threadForum = value;
                        };
                     
                        const id_forum = element.id_forum;
                        
                        let thread_query = `select f.*, u.username from thread_forum as f join users as u on f.id_user = u.id_user and f.id_forum =${id_forum};`;

                        await new Promise((resolve,reject)=>{
                            connection.query(thread_query, (err, results) => {
                                if (err) {
                                    res.send({
                                        message: err,
                                        status: helper.status.error,
                                    });
                                    res.end();
                                }

                                setValue(results.rows);
                                resolve()                   
                            })
                        });

                        Object.assign(element, { thread: threadForum });  
                    }))
                    res.send({
                        data:data,
                        status:helper.status.success
                    })
                    res.end()

                }
            })
        }
    })

   
}