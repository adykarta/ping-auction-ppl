var connection = require('../../connection')
const jwt = require('jsonwebtoken');
const config = require('../../config');
const helper = require('../../helpers');
var dateFormat = require('dateformat');

const getUser=async(username)=>{
    let data = []
    function setData(param){
        data = param
    }
    await new Promise((resolve,reject)=>{
        connection.query(`SELECT * FROM USERS where username='${username}';`,(err,results)=>{
            if(err){
                return 'failed'
                
            }
            
            setData(results.rows[0])
            resolve()
        })

    })
    
    return data
}

const updateBarang = async(id,harga_now, id_pembeli)=>{
    let query = `UPDATE barang set bid_terakhir=${harga_now}, id_pembeli=${id_pembeli} where id_barang=${id};`
    await new Promise((resolve, reject)=>{
        connection.query(query, (err, results)=>{
            if(err){
                return 'failed'
            }
            resolve()

        })
    })
    return 'success'
}


exports.bidProduct= async function(req,res){
    jwt.verify(req.token, config.secretkey, async(err, authData)=>{
        if(err){
            res.json({
                message:'invalid token!'
            });
        }
        else{
            const id_barang = req.body.id_barang;
            const harga_now = req.body.harga_now;
            const jumlah_bid = req.body.jumlah_bid;
            const username = authData.username;
            let userData = await getUser(username);
            if(userData==='failed'){
                res.send({
                    message:"get user failed",
                    status:helper.status.error
                })
                res.end()
            }
            
            let update = await updateBarang(id_barang,harga_now, userData.id_user)
            if(update==='failed'){
                res.send({
                    message:"update barang failed",
                    status:helper.status.error
                })
                res.end()
            }
            var date = dateFormat(new Date())
            let queryRiwayatBid = `INSERT INTO riwayat_bid(jumlah, id_user, id_barang,created_date) values(${jumlah_bid}, ${userData.id_user}, ${id_barang},'${date}');`
            connection.query(queryRiwayatBid,(err, results)=>{
                if(err){
                    res.send({
                        message:"insert to riwayat bid failed",
                        status:helper.status.error
                    })
                    res.end()
                }
                let queryTransaksi = `INSERT INTO transaksi(id_barang, id_user, bid, created_date)values(${id_barang}, ${userData.id_user},${harga_now}, '${date}');`
                connection.query(queryTransaksi,(err,results)=>{
                    if(err){
                        res.send({
                            message:"insert to transaksi failed",
                            status:helper.status.error
                        })
                        res.end()
                    }
                    res.send({
                        message:'bid product success',
                        status:helper.status.success
                    })
                    res.end()
                })
            })
            
        }
    })
}

const getWinner = async(id)=>{
    let data = []
    function setData(param){
        data = param
    }
    await new Promise((resolve,reject)=>{
        connection.query(`SELECT * FROM USERS where id_user=${id};`,(err,results)=>{
            if(err){
                return 'failed'
               
            }
    
            setData(results.rows[0])
            resolve()
        })

    })
    
    return data
}

exports.akhirLelang = (req,res)=>{
    jwt.verify(req.token, config.secretkey, (err, authData)=>{
        if(err){
            res.json({
                message:'invalid token!'
            });
        }
        else{
            const id_barang = req.body.id_barang;
            let query = `UPDATE barang set status='terjual' where id_barang=${id_barang} returning *;`
          
            connection.query(query, async(err,results)=>{
                if(err){
                    res.send({
                        message:'failed update barang',
                        status:helper.status.error
                    })
                    res.end()
                }
                else{
                    let detailBarang = results.rows[0]
                    if(detailBarang.id_pembeli===null){
                        res.send({
                            message:'Lelang telah berakhir',
                            pemenang:'-',
                            barang:detailBarang,
                            status:helper.status.success
                        })
                        res.end()
                    }
                    else{
                        let detailUser = await getWinner(detailBarang.id_pembeli)
                        if(detailUser==='failed'){
                            res.send({
                                message:"get winner data failed",
                                status:helper.status.error
                            })
                            res.end()
                        }
                        
                        res.send({
                            message:'Lelang telah berakhir',
                            pemenang: detailUser.username,
                            barang:detailBarang,
                            status:helper.status.success
                        })
                        res.end()

                    }
                   
                }
            })
            
        }
    })

}

exports.getRiwayatBid = (req,res)=>{
    jwt.verify(req.token, config.secretkey, (err, authData)=>{
        if(err){
            res.json({
                message:'invalid token!'
            });
        }
        else{

            const id_barang = req.params.id_barang 
            var query = `SELECT r.*, u.username FROM riwayat_bid as r join users as u on r.id_user=u.id_user where r.id_barang=${id_barang};`
            connection.query(query,(err,results)=>{
                if(err){
                    res.send({
                        message:'get riwayat bid failed',
                        status:helper.status.error

                    })
                    res.end()
                }
                else{
                    res.send({
                        data:results.rows,
                        status:helper.status.success
                    })
                    res.end()
                }
            })
            
        }
    })
}