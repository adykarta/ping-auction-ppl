var connection = require('../../connection')
const jwt = require('jsonwebtoken');
const config = require('../../config');
const helper = require('../../helpers');
// const moment = require('moment')
// const momentCountdown = require('moment-countdown')



exports.transactionDetail = function(req, res) {

   
    jwt.verify(req.token, config.secretkey, (err, authData) => {
        if(err){
            res.json({
                message: 'invalid tokenn'
            })
        }
        else{
            var query = `select * from users where username = '${authData.username}'`
            connection.query(query, (err, result) => {
                if(err){
                    res.json({
                        message: err,
                        status: helper.status.error
                    })
                    res.end()
                }
                if(result.rows.length == 1){
                    var query2 = `SELECT distinct on (id_barang)id_barang, id_transaksi,id_user, bid, judul, bid_terakhir, created_date, masa_lelang from (SELECT t.id_barang, t.id_transaksi, t.id_user, t.bid, b.judul, b.bid_terakhir, b.created_date, b.masa_lelang from transaksi as t inner join barang as b on t.id_barang = b.id_barang where t.id_user=${result.rows[0].id_user} order by t.id_barang, t.id_transaksi desc) as unique_id;`
                
                        connection.query(query2, async(err, result) => {
                        if(err){
                            res.json({
                                message: err,
                                status: helper.status.error
                            })
                            res.end()
                        }
                   
                        var data = result.rows
                       
                        await Promise.all(data.map(async(element) => {
                            let queryImage = `SELECT url FROM IMG WHERE id_barang = ${element.id_barang}`
                            img_url = ''
                            var setValueImg = function (value){
                                img_url = value
                            }
                            
                            await new Promise((resolve,reject)=>{
                                
                                connection.query(queryImage,(err,results)=>{
                                    if(err){
                                        res.send({
                                            message:err,
                                            status:helper.status.error
                                        })
                                        res.end()
                                    }
                                    setValueImg(results.rows)
                                    resolve()
                                })
                            

                            })
                            Object.assign(element, {'url': img_url})
                        }))
                        res.send({
                            message: 'Success!',
                            data: result.rows
                        })
                        // console.log("Sekarang: " + moment().format('D MMMM YYYY, h:mm:ss a'));
                        // console.log("Countdown: " + moment('2020-04-20').countdown().toString())
                        // console.log(Date.parse('2020-04-18T17:00:00.000Z') - Date.parse('2020-04-18T17:00:00.000Z'))
                        res.end()
                    })
                }
                else{
                    res.send({message: 'failed'})
                    res.end()
                }
            })
        }
    })
    
    // jwt.verify(req.token, config.secretkey, (err, authData)=>{
    //     if(err){
    //         res.json({
    //             message:'invalid token!'
    //         });
    //     }
    //     else{
    //         var checkUsername = `select id_user, username where username = '${authData.username}'`
    //         connection.query(checkUsername, (err, result) => {
    //             if(err){
    //                 res.json({
    //                     message: err,
    //                     status: helper.status.error
    //                 })
    //                 res.end()
    //             }
    //             else{
    //                 var query = `SELECT T.id_barang, B.judul, T.bid, B.bid_terakhir, B.created_date from TRANSAKSI AS T
    //                 INNER JOIN BARANG AS B ON T.id_barang = B.id_barang
    //                 WHERE T.id_transaksi = (SELECT MAX(T.id_transaksi) FROM TRANSAKSI AS T)`

    //                 connection.query(query, async(err, result) => {
    //                     if(err){
    //                         res.json({
    //                             message: err,
    //                             status: helper.status.error
    //                         })
    //                         res.end()
    //                     }
    //                     else{
    //                         var data = result.rows
    //                         await Promise.all(data.map(async(element) => {
    //                             let queryImage = `SELECT url FROM IMG WHERE id_barang = ${element.id_barang}`
    //                             img_url = ''
    //                             var setValueImg = function (value){
    //                                 img_url = value
    //                             }
                                
    //                             await new Promise((resolve,reject)=>{
                                    
    //                                 connection.query(queryImage,(err,results)=>{
    //                                     if(err){
    //                                         res.send({
    //                                             message:err,
    //                                             status:helper.status.error
    //                                         })
    //                                         res.end()
    //                                     }
    //                                     setValueImg(results.rows)
    //                                     resolve()
    //                                 })
                                

    //                             })
    //                             Object.assign(element, {'url': img_url})
    //                         }))
    //                         res.send(data)
    //                         console.log(data)
    //                         res.end()
    //                     }
    //                 })
    //             }

    //         })
            
    //     }
    // })
}
