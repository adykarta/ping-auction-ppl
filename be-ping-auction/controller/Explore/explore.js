const jwt = require('jsonwebtoken');
const connection = require('../../connection');
const helper = require('../../helpers');
const config = require('../../config');

exports.updateJumlahView = function (req,res){
    jwt.verify(req.token,config.secretkey,(err,authData)=>{
        if(err){
            res.json({
                message: "invalid token!"
            });
            res.end()
        }
        else{
            const id_barang = req.body.id_barang
            let query = `SELECT jumlah_view from BARANG where id_barang=${id_barang};`
            connection.query(query,(err,results)=>{
                if(err){
                    res.send({
                        message: err,
                        status:helper.status.error
                    })
                    res.end()
                }
                else{
                    let counter = results.rows[0].jumlah_view
                    counter = counter + 1
                    let updateViewQuery = `UPDATE BARANG SET jumlah_view = ${counter} where id_barang=${id_barang};`
                    connection.query(updateViewQuery,(err,results)=>{
                        if(err){
                            res.send({
                                message:err,
                                status:helper.status.error
                            })
                            res.end()
                        }
                        else{
                            res.send({
                                message:'success',
                                status:helper.status.success
                            })
                            res.end()
                        }

                    })
                   
                }
            })
          
        }
    })
}