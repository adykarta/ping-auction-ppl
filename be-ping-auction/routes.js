const multer = require("multer");

var storage = multer.memoryStorage();
var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});

module.exports = function (app) {
    var all = require('./controller/Auth/all_privilages');
    var user = require('./controller/Profile/detailUser');
    var prov = require('./controller/provinsi');
    var forgot = require('./controller/Auth/forgotPassword')
    var detail = require('./controller/itemDetail')
    var display = require('./controller/itemDisplay')
    var category = require('./controller/listCategory')
    var auth = require('./auth')
    var listing = require('./controller/Listing/listing')
    var barang = require('./controller/Profile/barang')
    var profile = require('./controller/Profile/profile')
    var user = require('./controller/Profile/detailUser')
    var explore = require('./controller/Explore/explore')
    var forum = require('./controller/Explore/forum')
    var today = require('./controller/Explore/todaysItem')
    var detail = require('./controller/Explore/itemDetail')
    var kategori = require('./controller/kategori')
    var today = require('./controller/Explore/todaysItem')
    var item = require('./controller/Explore/all_Item')
    var bidding = require('./controller/Explore/bidding')
    var transaction = require('./controller/Explore/transactionDetail')
    app.route('/api/') 
        .get(all.initial)
    app.route('/api/token')
        .post(all.token)
    app.route('/api/logout')
        .get(auth.verifyToken, all.logout)
    app.route('/api/login')
        .post(all.login)
    app.route('/api/check-role')
        .post(auth.verifyToken,all.checkRole)
    app.route('/api/get-prov')
        .get(prov.getProv)
    app.route('/api/get-kategori')
        .get(kategori.listCategory)
    app.route('/api/forgot-password')
        .post(forgot.forgotPassword)
    app.route('/api/set-new-password/:token')
        .post(forgot.setNewPassword)
    app.route('/api/register')
        .post(all.register)
    
    app.route('/api/item-detail')
        .get(detail.itemDetail)
    app.route('/api/list-category')
        .get(auth.verifyToken,category.listCategory)
    app.route('/api/item-display')
        .post(display.itemDisplay)
    app.route('/api/explore/today-item')
        .get(auth.verifyToken,today.todaysItem)
    //listing
    app.route('/api/listing/create-item')
        .post(auth.verifyToken,upload.array('file', 6),listing.createBarang)
    //profile
    app.route('/api/profile/list-barang')
        .get(auth.verifyToken,barang.listBarang)
    app.route('/api/profile/get-user')
        .get(auth.verifyToken,user.getUser)
    app.route('/api/profile/edit-profile')
        .post(auth.verifyToken,profile.editProfile)

    //explore
    app.route('/api/explore/get-item/jumlah-bidder')
        .get(auth.verifyToken,detail.getItemByJumlahBidder)
    app.route('/api/explore/update-jumlah-view')
        .post(auth.verifyToken,explore.updateJumlahView)
    app.route('/api/transaction/detail-transaction')
        .get(auth.verifyToken, transaction.transactionDetail)


    //forum
    app.route('/api/explore/forum/get-forum/:id_barang')
        .get(auth.verifyToken, forum.getForum)
    app.route('/api/explore/forum/post-forum')
        .post(auth.verifyToken, forum.postToForum)
    app.route('/api/explore/forum/thread/post-forum')
        .post(auth.verifyToken, forum.postToThreadForum)
    app.route('/api/item-detail/:id_barang')
        .get(auth.verifyToken, detail.itemDetail)

    //homepage
    app.route('/api/todays-item')
        .get(auth.verifyToken, today.todaysItem)
    app.route('/api/all-item')
        .get(auth.verifyToken, item.all_item)

    //bidding
    app.route('/api/bidding/bid-product')
        .post(auth.verifyToken,bidding.bidProduct)
    app.route('/api/bidding/akhir-lelang')
        .post(auth.verifyToken,bidding.akhirLelang)
    app.route('/api/bidding/riwayat-bid/:id_barang')
        .get(auth.verifyToken,bidding.getRiwayatBid)

};