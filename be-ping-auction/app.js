var express = require('express'),
	app = express(),
	// port = 5432 || 8081,
	bodyParser = require('body-parser'),
    path = require('path');

var dotenv = require('dotenv-defaults');
dotenv.config();

app.use(
	bodyParser.urlencoded({
		extended: true,
	}),
);
app.use(bodyParser.json());
app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	next();
});
app.use(express.static(path.join(__dirname, 'public')));

var routes = require('./routes');
routes(app);

// if ('dev' !== 'test') {
//     app.listen(port);
// }
// console.log('Learn Node JS With Kiddy, RESTful API server started on: ' + port);


module.exports=app;

