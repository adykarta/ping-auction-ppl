# Ikan Lele-Ping Auction
**Pipeline and Coverage Status**
- [![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ikan-lele-ping-auction/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ikan-lele-ping-auction/commits/staging)

- [![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ikan-lele-ping-auction/badges/staging/coverage.svg?job=test-fe)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ikan-lele-ping-auction/commits/staging) Front End
- [![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ikan-lele-ping-auction/badges/staging/coverage.svg?job=test-be)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ikan-lele-ping-auction/commits/staging) Back End
